package com.josholadele.streetlaw.modelBridge;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.database.Database;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.josholadele.streetlaw.model.DaoMaster;
import com.josholadele.streetlaw.model.JournalDao;
import com.josholadele.streetlaw.model.UserDao;
import com.josholadele.streetlaw.model.UserMessage;
import com.josholadele.streetlaw.model.UserMessageDao;
//import ng.apmis.agpmpn.model.JournalDao;
//import ng.apmis.agpmpn.model.OperatingProcedureDao;

/**
 * Created by Josh on 12/28/2017.
 */

public class MyOpenHelper extends DaoMaster.OpenHelper {
    public static final int SCHEMA_VERSION = 2;
    public MyOpenHelper(Context context, String name) {
        super(context, name);
    }

    public MyOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory) {
        super(context, name, factory);
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
        Log.i("greenDAO", "Upgrading schema from version " + oldVersion + " to " + newVersion + " by dropping all tables");
        super.onUpgrade(db, oldVersion, newVersion);
        MigrationHelper.getInstance().migrate(db, JournalDao.class, UserDao.class); // UserMessageDao.class,
    }
}
