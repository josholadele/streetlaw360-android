package com.josholadele.streetlaw.modelBridge;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.josholadele.streetlaw.StreetLaw360App;
import com.josholadele.streetlaw.model.User;
import com.josholadele.streetlaw.model.UserMessage;

import java.util.List;

/**
 * Created by josh on 01/05/2018.
 */

public class ConversationBridge {
    Context context;

    public ConversationBridge() {
    }

    public ConversationBridge(Context context) {
        this.context = context;
    }

    public void save(User user) {

        StreetLaw360App.getInstance().getDaoSession().getUserDao().insert(user);
        sendChangesBroadcast(user);
    }

    public void saveChat(User user, UserMessage message) {

        StreetLaw360App.getInstance().getDaoSession().getUserMessageDao().insert(message);
        sendChangesBroadcast(user);
    }


    public List<User> getAllContacts() {

        return StreetLaw360App.getInstance().getDaoSession().getUserDao().loadAll();
    }

    private synchronized void sendChangesBroadcast(User user) {
        Intent intent = new Intent("ACTION_CONVERSATION_NOTIFICATION");
        intent.putExtra("CONVERSATION_ID", user.userId);
        intent.putExtra("EXTRA_CONVERSATION", user);

        LocalBroadcastManager.getInstance(this.context).sendBroadcast(intent);
    }
}
