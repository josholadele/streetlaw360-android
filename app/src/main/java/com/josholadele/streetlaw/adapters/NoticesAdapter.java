package com.josholadele.streetlaw.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.model.Notice;
import com.josholadele.streetlaw.utils.TimeUtils;

import java.util.HashSet;
import java.util.List;

/**
 * Created by josh on 20/03/2018.
 */

public class NoticesAdapter extends RecyclerView.Adapter<NoticesAdapter.NoticesViewHolder> {

    HashSet<Integer> selectedPositions = new HashSet<>();
    private List<Notice> theList;
    private Context context;
    private NoticeClickListener caseClickListener;

    public NoticesAdapter(NoticeClickListener clickListener) {
        this.caseClickListener = clickListener;
    }


    @Override
    public NoticesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notice_card_view, parent, false);
        return new NoticesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NoticesViewHolder holder, int position) {

        Notice notice = theList.get(position);
//        holder.noteDate.setText(notice.caseStatus.statusText);
        holder.notificationTitle.setText(notice.Title);
        holder.notificationMessage.setText(notice.message);
//        holder.notificationDate.setText(notice);
//        holder.newsImage.setImageResource(R.drawable.ic_hammer);
        holder.notificationDate.setText(TimeUtils.getFormattedTime(notice.dateSent));
        if (notice.Users != null && notice.Users.size() > 0) {

        }
        holder.itemView.setActivated(selectedPositions.contains(position));

    }

    public void setNoticeData(List<Notice> notices) {
        this.theList = notices;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (theList == null) return 0;
        return theList.size();
    }


    public interface NoticeClickListener {
        void onNoticeClick(Notice caseFile, boolean isLongClick);
    }

    class NoticesViewHolder extends RecyclerView.ViewHolder {

        ImageView caseStatusImage;
        TextView notificationTitle;
        TextView notificationMessage;


        TextView notificationDate;
//        TextView releaseDate;
//        TextView genreText;

        public NoticesViewHolder(final View itemView) {
            super(itemView);
            notificationTitle = itemView.findViewById(R.id.notification_title);
            notificationMessage = itemView.findViewById(R.id.notification_message);
            caseStatusImage = itemView.findViewById(R.id.img_case_status);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    caseClickListener.onNoticeClick(theList.get(getAdapterPosition()), false);
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (selectedPositions.contains(getAdapterPosition())) {
                        selectedPositions.remove(getAdapterPosition());
                        itemView.setActivated(false);
                    } else {
                        selectedPositions.add(getAdapterPosition());
                        itemView.setActivated(true);
                    }
                    caseClickListener.onNoticeClick(theList.get(getAdapterPosition()), true);
                    return true;
                }
            });

            notificationDate = (TextView) itemView.findViewById(R.id.notification_datesent);
//            releaseDate = (TextView) itemView.findViewById(R.id.release_date);
//            genreText = (TextView) itemView.findViewById(R.id.genre_text);

        }
    }
}




