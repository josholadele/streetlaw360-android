package com.josholadele.streetlaw.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.activities.CaseFileActivity;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.enums.CaseStatus;
import com.josholadele.streetlaw.model.CaseFile;
import com.josholadele.streetlaw.model.Judge;
import com.josholadele.streetlaw.utils.UserUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by josh on 20/03/2018.
 */

public class CaseFilesAdapter extends RecyclerView.Adapter<CaseFilesAdapter.CaseFilesViewHolder> {

    private List<CaseFile> theList;
    private Context context;
    private CaseClickListener caseClickListener;
    HashMap<Integer, CaseFile> selectionList;
    public HashSet<Integer> selectedPositions = new HashSet<>();

    CaseFileActivity activity;

    public boolean IS_MULTISELECT = false;

    public CaseFilesAdapter(CaseClickListener clickListener) {
        this.caseClickListener = clickListener;
    }


    @Override
    public CaseFilesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        activity = (CaseFileActivity) context;
        View view;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.case_file_item_view, parent, false);
        return new CaseFilesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CaseFilesViewHolder holder, int position) {

        CaseFile caseFile = theList.get(position);
        holder.caseStatusText.setText(caseFile.caseStatus.statusText);
        holder.caseTitle.setText(caseFile.Title);
        holder.caseStatusImage.setImageResource(R.drawable.ic_hammer);

        setCaseStatusColor(holder, caseFile);

        holder.itemView.setActivated(selectedPositions.contains(position));

    }

    public void setCaseFileData(List<CaseFile> caseFiles) {
        this.theList = caseFiles;
        notifyDataSetChanged();
    }

    public void setSearchResult(List<CaseFile> results) {
        theList = results;
        notifyDataSetChanged();
    }

    public void addCaseFile(CaseFile caseFile) {
        this.theList.add(caseFile);// = caseFiles;
        notifyDataSetChanged();
    }

    public List<CaseFile> getData() {
        return theList;
    }

    @Override
    public int getItemCount() {
        if (theList == null) return 0;
        return theList.size();
    }

    public void clearSelections() {
//        for (int i : selectedPositions) {
//
//        }
        selectedPositions.clear();
        activity.updateToolbar(false);
    }

    void setCaseStatusColor(CaseFilesViewHolder holder, CaseFile item) {
        switch (item.caseStatus.statusNumber) {

            //Registered
            case 1: {
                holder.caseStatusImage.setImageResource(R.drawable.ic_case_registered);
                holder.caseStatusText.setTextColor(Color.parseColor("#FF5CBB67"));
                break;
            }
            /*
             Registered(1, "Registered"),
    Assigned(2, "Assigned"),
    Transferred(3, "Transferred"),
    Adjourned(4, "Adjourned"),
    Mention(5, "Mention"),
    Hearing(6, "Hearing"),
    Continuation(7, "Continuation"),
    ReportOfService(8, "Report Of Service"),
    FurtherMention(9, "Further Mention"),
    Judgement(10, "Judgement"),
    AdoptionOfWrittenAddresses(11, "Adoption Of Written Addresses"),
    HearingOfApplication(12, "Hearing Of Application"),
    Closed(13, "Closed");
            */

            //Assigned
            case 2: {
                holder.caseStatusImage.setImageResource(R.drawable.ic_case_assigned);
                holder.caseStatusText.setTextColor(Color.parseColor("#FF000000"));
                break;
            }

            //Mention
            case 5: {
                holder.caseStatusImage.setImageResource(R.drawable.ic_case_mention);
                holder.caseStatusText.setTextColor(Color.parseColor("#BF00FF"));
                break;
            }

            //Hearing
            case 6: {
                holder.caseStatusImage.setImageResource(R.drawable.ic_case_hearing);
                holder.caseStatusText.setTextColor(Color.parseColor("#E6625845"));
                break;
            }

            //Continuation
            case 7: {
                holder.caseStatusImage.setImageResource(R.drawable.ic_case_continuation);
                holder.caseStatusText.setTextColor(Color.parseColor("#DD006CF0"));
                break;
            }
//            case CaseStatus.ReportOfService: {
//                holder.newsImage.setImageResource(R.drawable.androidicons_42);
//                holder.noteDate.setTextColor(Color.parseColor("#bdbdbd"));
//                break;
//            }
//            case CaseStatus.FurtherMention: {
//                holder.newsImage.setImageResource(R.drawable.androidicons_42);
//                holder.noteDate.setTextColor(Color.parseColor("#bdbdbd"));
//                break;
//            }

            //Judgement
            case 10: {
                holder.caseStatusImage.setImageResource(R.drawable.ic_case_judgement);
                holder.caseStatusText.setTextColor(Color.parseColor("#2C0F0F"));
                break;
            }
//            case CaseStatus.AdoptionOfWrittenAddresses: {
//                holder.newsImage.setImageResource(R.drawable.androidicons_42);
//                holder.noteDate.setTextColor(Color.parseColor("#bdbdbd"));
//                break;
//            }
//            case CaseStatus.HearingOfApplication: {
//                holder.newsImage.setImageResource(R.drawable.androidicons_42);
//                holder.noteDate.setTextColor(Color.parseColor("#bdbdbd"));
//                break;
//            }
//
            //Closed
            case 13: {
                holder.caseStatusImage.setImageResource(R.drawable.icons_49);
                holder.caseStatusText.setTextColor(Color.parseColor("#EFEF4749"));
                //holder.HammerIcon.Visibility = ViewStates.Invisible;
                break;
            }
        }

    }

    public interface CaseClickListener {
        void onCaseFileClick(CaseFile caseFile, boolean isLongClick);
    }

    class CaseFilesViewHolder extends RecyclerView.ViewHolder {

        ImageView caseStatusImage;
        TextView caseTitle;

//        View lastView;


        TextView caseStatusText;
//        TextView releaseDate;
//        TextView genreText;

        public CaseFilesViewHolder(final View itemView) {
            super(itemView);
            caseTitle = itemView.findViewById(R.id.text_case_title);
            caseStatusImage = itemView.findViewById(R.id.img_case_status);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (IS_MULTISELECT && selectedPositions.contains(getAdapterPosition())) {
                        selectedPositions.remove(getAdapterPosition());
                        itemView.setActivated(false);
                        if (selectedPositions.size() == 0) {
                            IS_MULTISELECT = false;
                        }
                        activity.updateToolbar(IS_MULTISELECT);
                    } else if (IS_MULTISELECT) {
                        selectedPositions.add(getAdapterPosition());
                        itemView.setActivated(true);
                        activity.updateToolbar(IS_MULTISELECT);

                    } else {
                        caseClickListener.onCaseFileClick(theList.get(getAdapterPosition()), false);
                    }
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (UserUtils.isJudgeOrRegistrar(activity)) {

                        if (selectedPositions.size() == 0) {
                            IS_MULTISELECT = true;
                        }
                        if (selectedPositions.contains(getAdapterPosition())) {
                            selectedPositions.remove(getAdapterPosition());
                            itemView.setActivated(false);
                            activity.updateToolbar(IS_MULTISELECT);
                        } else {
                            selectedPositions.add(getAdapterPosition());
                            itemView.setActivated(true);
                            activity.updateToolbar(IS_MULTISELECT);
                        }
//                    caseClickListener.onContactClick(theList.get(getAdapterPosition()), true);
                        return true;
                    }
                    return false;
                }
            });

            caseStatusText = (TextView) itemView.findViewById(R.id.text_case_status);
//            releaseDate = (TextView) itemView.findViewById(R.id.release_date);
//            genreText = (TextView) itemView.findViewById(R.id.genre_text);

        }
    }
}




