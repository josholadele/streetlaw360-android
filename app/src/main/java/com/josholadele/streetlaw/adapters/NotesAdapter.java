package com.josholadele.streetlaw.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.model.Note;
import com.josholadele.streetlaw.utils.TimeUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by josh on 20/03/2018.
 */

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.NoteViewHolder> {

    private List<Note> theList;
    private Context context;
    private NoteClickListener caseClickListener;
    HashSet<Integer> selectedPositions = new HashSet<>();

    public NotesAdapter(NoteClickListener clickListener) {
        this.caseClickListener = clickListener;
    }


    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.note_item_view, parent, false);
        return new NoteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {

        Note note = theList.get(position);
//        holder.noteDate.setText(note.caseStatus.statusText);
        holder.noteTopic.setText(note.topic);
//        holder.newsImage.setImageResource(R.drawable.ic_hammer);
        try {

            holder.noteDate.setText(TimeUtils.getFormattedTime(note.dateLastUpdated));
        } catch (Exception ignored) {
            holder.noteDate.setText("now");

        }

    }

    public void setNoteData(List<Note> notices) {
        this.theList = notices;
        notifyDataSetChanged();
    }

    public void addNote(Note note) {
        if (this.theList == null) {
            this.theList = new ArrayList<>();
        }
        this.theList.add(note);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (theList == null) return 0;
        return theList.size();
    }


    public interface NoteClickListener {
        void onNoteClick(Note note, boolean isLongClick);
    }

    class NoteViewHolder extends RecyclerView.ViewHolder {

        ImageView caseStatusImage;
        TextView noteTopic;
//        TextView noteTime;


        TextView noteDate;
//        TextView releaseDate;
//        TextView genreText;

        public NoteViewHolder(final View itemView) {
            super(itemView);
            noteTopic = itemView.findViewById(R.id.note_topic);
//            noteTime = itemView.findViewById(R.id.notification_message);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    caseClickListener.onNoteClick(theList.get(getAdapterPosition()), false);
                }
            });


            noteDate = (TextView) itemView.findViewById(R.id.note_date);
//            releaseDate = (TextView) itemView.findViewById(R.id.release_date);
//            genreText = (TextView) itemView.findViewById(R.id.genre_text);

        }
    }
}




