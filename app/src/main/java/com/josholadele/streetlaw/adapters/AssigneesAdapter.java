package com.josholadele.streetlaw.adapters;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.activities.ChooseJudgeActivity;
import com.josholadele.streetlaw.model.CaseFile;
import com.josholadele.streetlaw.model.Judge;
import com.josholadele.streetlaw.model.User;
import com.josholadele.streetlaw.network.UrlConstants;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by josh on 20/03/2018.
 */

public class AssigneesAdapter extends RecyclerView.Adapter<AssigneesAdapter.AssigneesViewHolder> {

    private List<User> theList;
    private Context context;

    private AssigneeClickListener assigneeClickListener;
    HashMap<Integer, CaseFile> selectionList;

    public HashSet<Integer> selectedPositions = new HashSet<>();

//    ChooseJudgeActivity activity;

    public boolean IS_MULTISELECT = false;

    public AssigneesAdapter(AssigneeClickListener assigneeClickListener) {
        this.assigneeClickListener = assigneeClickListener;
    }

    // Constructor with context
    public AssigneesAdapter(Context context, AssigneeClickListener assigneeClickListener){
        this.context = context;
        this.assigneeClickListener = assigneeClickListener;
    }


    @Override
    public AssigneesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
//        activity = (ChooseJudgeActivity) context;
        View view;

        // Inflate the item views from the XML layout file
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.assignee_item_view, parent, false);

        // Return a new AssigneeViewHolderObject.
        return new AssigneesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AssigneesViewHolder holder, int position) {

        User user = theList.get(position);
        holder.assigneeName.setText(user.displayName);

        String profilePictureString = user.displayPicture;

        // If image string is not empty,load the image
        // Else, do something else
        if (!TextUtils.isEmpty(profilePictureString)) {

            // Load the image into the ImageView with the Glide library
            Glide.with(context).load(user.getDisplayPicture()).into(holder.assigneeImage);
        }else{
            // Display a toast
            Toast.makeText(context, "Image string is empty, loading a dummy image", Toast.LENGTH_SHORT).show();
            // Load a dummy image
            Glide.with(context).load(R.drawable.case_file_selector_bg).into(holder.assigneeImage);
        }

        //holder.assigneeImage.setImageBitmap();




       // holder.assigneeImage.setImage;


        //Glide.with(holder.assigneeImage.getContext()).load(R.drawable.case_file_selector_bg).into(holder.assigneeImage);
//        holder.assigneeImage;

    }
    // Binds the data to the views
    public void bind(){

    }

    public void setData(List<User> users) {
        this.theList = users;
        notifyDataSetChanged();
    }


    public void removeLawyer(User user) {
        if (theList.contains(user)) {
            theList.remove(user);
            notifyDataSetChanged();
        }
    }

    public List<User> getData() {
        return theList;
    }

    public void addSingleData(User user) {
        this.theList.add(user);// = caseFiles;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (theList == null) return 0;
        return theList.size();
    }

    public interface AssigneeClickListener {
        void onAssigneeClick(User user, boolean isLongClick);
    }


    class AssigneesViewHolder extends RecyclerView.ViewHolder {

       // This will display the assignee's name
        TextView assigneeName;
        // This will display the assignee's picture
        ImageView assigneeImage;


        public AssigneesViewHolder(final View itemView) {
            super(itemView);

            // Instantiate the views for the name and picture
            assigneeName = itemView.findViewById(R.id.assignee_name);
            assigneeImage = itemView.findViewById(R.id.assignee_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    assigneeClickListener.onAssigneeClick(theList.get(getAdapterPosition()), false);
                }
            });
        }
    }
}




