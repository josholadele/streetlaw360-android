package com.josholadele.streetlaw.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.model.JudicialDivision;

import java.util.ArrayList;
import java.util.List;

public class JudicialDivisionAdapter extends BaseAdapter {
    private List<JudicialDivision> divisions = new ArrayList<JudicialDivision>();
    private LayoutInflater layoutInflater = null;
    private Context context;

    public JudicialDivisionAdapter(Activity context, List<JudicialDivision> divisions) {
        this.divisions = divisions;
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return divisions.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder {
        TextView textView_Division;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder();
        View view = convertView;
        if(view == null) {
            view = layoutInflater.inflate(R.layout.support_simple_spinner_dropdown_item, null);
            //viewHolder.textView_Division = (TextView) view.findViewById(R.id.)
        }
        return view;
    }
}