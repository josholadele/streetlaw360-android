package com.josholadele.streetlaw.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import de.hdodenhof.circleimageview.CircleImageView;
import android.os.AsyncTask;
import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.activities.NewChatActivity;
import com.josholadele.streetlaw.model.CaseFile;
import com.josholadele.streetlaw.model.ChatContact;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by josh on 20/03/2018.
 */

public class ChatContactsAdapter extends RecyclerView.Adapter<ChatContactsAdapter.ContactViewHolder> {

    private List<ChatContact> theList;
    private Context context;
    private JudgeClickListener judgeClickListener;
    HashMap<Integer, CaseFile> selectionList;
    public HashSet<Integer> selectedPositions = new HashSet<>();

    NewChatActivity activity;

    public boolean IS_MULTISELECT = false;

    public ChatContactsAdapter(JudgeClickListener clickListener) {
        this.judgeClickListener = clickListener;
    }


    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        activity = (NewChatActivity) context;
        View view;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_contact_item_view, parent, false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {

        ChatContact judge = theList.get(position);
        holder.judgeRole.setText(judge.email);
        holder.judgeName.setText(judge.name);
        String initial = String.valueOf(judge.name.charAt(0));
        holder.judgeInitial.setText(initial);

        String URl = judge.profileImage;
        Log.e("ProfleImageURL", "URL: " + URl);
        new ChatContactsAdapter.ImageDownload(context, holder.newsImage).execute(URl);
//        holder.newsImage.setImageResource(R.drawable.ic_hammer);


//        holder.itemView.setActivated(selectedPositions.contains(position));

    }

    public void setContactsData(List<ChatContact> judges) {
        this.theList = judges;
        notifyDataSetChanged();
    }

    public List<ChatContact> getData() {
        return theList;
    }

    public void addJudge(ChatContact judge) {
        this.theList.add(judge);// = caseFiles;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (theList == null) return 0;
        return theList.size();
    }

    public void clearSelections() {
//        for (int i : selectedPositions) {
//
//        }
        selectedPositions.clear();
        activity.updateToolbar(false);
    }

    private void setCaseStatusColor(ContactViewHolder holder, CaseFile item) {
        switch (item.caseStatus.statusNumber) {

            //Registered
            case 1: {
//                holder.newsImage.setImageResource(R.drawable.ic_case_registered);
                holder.judgeRole.setTextColor(Color.parseColor("#FF5CBB67"));
                break;
            }
            /*
             Registered(1, "Registered"),
    Assigned(2, "Assigned"),
    Transferred(3, "Transferred"),
    Adjourned(4, "Adjourned"),
    Mention(5, "Mention"),
    Hearing(6, "Hearing"),
    Continuation(7, "Continuation"),
    ReportOfService(8, "Report Of Service"),
    FurtherMention(9, "Further Mention"),
    Judgement(10, "Judgement"),
    AdoptionOfWrittenAddresses(11, "Adoption Of Written Addresses"),
    HearingOfApplication(12, "Hearing Of Application"),
    Closed(13, "Closed");
            */

            //Assigned
            case 2: {
//                holder.newsImage.setImageResource(R.drawable.ic_case_assigned);
                holder.judgeRole.setTextColor(Color.parseColor("#FF000000"));
                break;
            }

            //Mention
            case 5: {
//                holder.newsImage.setImageResource(R.drawable.ic_case_mention);
                holder.judgeRole.setTextColor(Color.parseColor("#BF00FF"));
                break;
            }

            //Hearing
            case 6: {
//                holder.newsImage.setImageResource(R.drawable.ic_case_hearing);
                holder.judgeRole.setTextColor(Color.parseColor("#E6625845"));
                break;
            }

            //Continuation
            case 7: {
//                holder.newsImage.setImageResource(R.drawable.ic_case_continuation);
                holder.judgeRole.setTextColor(Color.parseColor("#DD006CF0"));
                break;
            }
//            case CaseStatus.ReportOfService: {
//                holder.newsImage.setImageResource(R.drawable.androidicons_42);
//                holder.noteDate.setTextColor(Color.parseColor("#bdbdbd"));
//                break;
//            }
//            case CaseStatus.FurtherMention: {
//                holder.newsImage.setImageResource(R.drawable.androidicons_42);
//                holder.noteDate.setTextColor(Color.parseColor("#bdbdbd"));
//                break;
//            }

            //Judgement
            case 10: {
//                holder.newsImage.setImageResource(R.drawable.ic_case_judgement);
                holder.judgeRole.setTextColor(Color.parseColor("#2C0F0F"));
                break;
            }
//            case CaseStatus.AdoptionOfWrittenAddresses: {
//                holder.newsImage.setImageResource(R.drawable.androidicons_42);
//                holder.noteDate.setTextColor(Color.parseColor("#bdbdbd"));
//                break;
//            }
//            case CaseStatus.HearingOfApplication: {
//                holder.newsImage.setImageResource(R.drawable.androidicons_42);
//                holder.noteDate.setTextColor(Color.parseColor("#bdbdbd"));
//                break;
//            }
//
            //Closed
            case 13: {
//                holder.newsImage.setImageResource(R.drawable.icons_49);
                holder.judgeRole.setTextColor(Color.parseColor("#EFEF4749"));
                //holder.HammerIcon.Visibility = ViewStates.Invisible;
                break;
            }
        }

    }

    public void deactivate() {
//        for (int i : selectedPositions) {
//
//            theList.get(i).
//        }
    }

    public interface JudgeClickListener {
        void onJudgeClick(ChatContact judge, boolean isLongClick);
    }

    class ContactViewHolder extends RecyclerView.ViewHolder {

        CircleImageView newsImage;
        TextView judgeName;
        TextView judgeInitial;

//        View lastView;


        TextView judgeRole;
//        TextView releaseDate;
//        TextView genreText;

        public ContactViewHolder(final View itemView) {
            super(itemView);
            judgeName = itemView.findViewById(R.id.judge_name);
            judgeInitial = itemView.findViewById(R.id.judge_initial);
            newsImage = itemView.findViewById(R.id.profile_image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (IS_MULTISELECT && selectedPositions.contains(getAdapterPosition())) {
                        selectedPositions.remove(getAdapterPosition());
                        itemView.setActivated(false);
                        if (selectedPositions.size() == 0) {
                            IS_MULTISELECT = false;
                        }
                        activity.updateToolbar(IS_MULTISELECT);
                    } else if (IS_MULTISELECT) {
                        selectedPositions.add(getAdapterPosition());
                        itemView.setActivated(true);
                        activity.updateToolbar(IS_MULTISELECT);

                    } else {
                        judgeClickListener.onJudgeClick(theList.get(getAdapterPosition()), false);
                    }
                }
            });
//            itemView.setOnLongClickListener(new View.OnLongClickListener() {
//                @Override
//                public boolean onLongClick(View view) {
//                    if (selectedPositions.size() == 0) {
//                        IS_MULTISELECT = false;
//                    }
//                    if (selectedPositions.contains(getAdapterPosition())) {
//                        selectedPositions.remove(getAdapterPosition());
//                        itemView.setActivated(false);
//                        activity.updateToolbar(IS_MULTISELECT);
//                    } else {
//                        selectedPositions.add(getAdapterPosition());
//                        itemView.setActivated(true);
//                        activity.updateToolbar(IS_MULTISELECT);
//                    }
////                    judgeClickListener.onContactClick(theList.get(getAdapterPosition()), true);
//                    return true;
////                    return false;
//                }
//            });

            judgeRole = (TextView) itemView.findViewById(R.id.judge_role);
//            releaseDate = (TextView) itemView.findViewById(R.id.release_date);
//            genreText = (TextView) itemView.findViewById(R.id.genre_text);

        }
    }

    class ImageDownload extends AsyncTask<String, Integer, Bitmap> {
        Context context;
        CircleImageView imageView;
        Bitmap bitmap = null;
        InputStream in = null;
        int responseCode = -1;

        //constructor.
        public ImageDownload(Context context, CircleImageView imageView) {
            this.context = context;
            this.imageView = imageView;
        }

        private Bitmap downloadImageBitmap(String sUrl) {
            Bitmap bitmap = null;
            try {
                InputStream inputStream = new URL(sUrl).openStream();   // Download Image from URL
                bitmap = BitmapFactory.decodeStream(inputStream);       // Decode Bitmap
                inputStream.close();
            } catch (Exception e) {
                Log.d("ImageDownload", "Exception 1, Something went wrong!");
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return downloadImageBitmap(params[0]);
        }

        @Override
        protected void onPostExecute(Bitmap data) {
            if (data != null) {
                imageView.setImageBitmap(data);
            }
        }
    }
}




