package com.josholadele.streetlaw.adapters;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.model.News;
import com.josholadele.streetlaw.network.UrlConstants;

import org.w3c.dom.Text;

import java.util.HashSet;
import java.util.List;

/**
 * Created by josh on 20/03/2018.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    HashSet<Integer> selectedPositions = new HashSet<>();
    private List<News> theList;
    private Context context;
    private NewsClickListener caseClickListener;

    public NewsAdapter(NewsClickListener clickListener) {
        this.caseClickListener = clickListener;
    }


    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_item_view, parent, false);
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {

        News caseFile = theList.get(position);
        holder.newsStory.setText(caseFile.story);
        holder.newsHeadline.setText(caseFile.headline);
        if (!TextUtils.isEmpty(caseFile.pictureUrl) && !caseFile.pictureUrl.equalsIgnoreCase("null")) {

            holder.newsImageLayout.setVisibility(View.VISIBLE);
            Glide.with(context).load(UrlConstants.IMAGE_DUMP + caseFile.pictureUrl).into(holder.newsImage);

        } else {

            holder.newsImageLayout.setVisibility(View.GONE);
        }
        holder.itemView.setActivated(selectedPositions.contains(position));

    }

    public void setNewsData(List<News> notices) {
        this.theList = notices;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (theList == null) return 0;
        return theList.size();
    }


    public interface NewsClickListener {
        void onNewsClick(News caseFile, boolean isLongClick);
    }

    class NewsViewHolder extends RecyclerView.ViewHolder {

        ImageView newsImage;
        TextView newsHeadline;
        LinearLayout newsImageLayout;

        TextView newsStory;

        public NewsViewHolder(final View itemView) {
            super(itemView);
            newsHeadline = itemView.findViewById(R.id.news_headline);
            newsImage = itemView.findViewById(R.id.imgView_newspix);
            newsImageLayout = itemView.findViewById(R.id.news_image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    caseClickListener.onNewsClick(theList.get(getAdapterPosition()), false);
                }
            });

            newsStory = (TextView) itemView.findViewById(R.id.news_story);
//            releaseDate = (TextView) itemView.findViewById(R.id.release_date);
//            genreText = (TextView) itemView.findViewById(R.id.genre_text);

        }
    }
}




