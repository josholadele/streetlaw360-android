package com.josholadele.streetlaw.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.os.AsyncTask;
import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.activities.ConversationActivity;
import com.josholadele.streetlaw.activities.PeersActivity;
import com.josholadele.streetlaw.model.ChatContact;
import com.josholadele.streetlaw.model.User;
import com.josholadele.streetlaw.utils.TimeUtils;

import java.util.HashSet;
import java.util.List;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;

public class ContactsListAdapter extends RecyclerView.Adapter<ContactsListAdapter.ConversationListHolder> {

    private List<User> theList;
    private List<ChatContact> theContactList;
    private Context context;
    private ContactClickListener contactClickListener;
    public HashSet<Integer> selectedPositions = new HashSet<>();
    PeersActivity activity;

    public ContactsListAdapter(ContactClickListener clickListener) {
        this.contactClickListener = clickListener;
    }


    @Override
    public ConversationListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        activity = (PeersActivity) context;
        View view;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.conversation_contact_item_view, parent, false);
        return new ConversationListHolder(view);
    }

    @Override
    public void onBindViewHolder(ConversationListHolder holder, int position) {

        User user = theList.get(position);
        holder.chatTime.setText(TimeUtils.getFormattedTime(user.lastConversationTime));
        holder.contactName.setText(user.displayName);
        holder.chatMessage.setText(user.lastMessage);
        String initial = String.valueOf(user.displayName.charAt(0));
        holder.contactInitials.setText(initial);

        String URl = user.displayPicture;
        Log.e("ProfleImageURL", "URL: " + URl);
        new ContactsListAdapter.ImageDownload(context, holder.circleImageView).execute(user.displayPicture);
//        holder.newsImage.setImageResource(R.drawable.ic_hammer);


//        holder.itemView.setActivated(selectedPositions.contains(position));

    }

    public void setUsersData(List<User> users) {
        this.theList = users;
        notifyDataSetChanged();
    }

    public void setSearchResult(List<User> results) {
        theList = results;
        notifyDataSetChanged();
    }

    public void setContactsData(List<ChatContact> judges) {
        this.theContactList = judges;
        notifyDataSetChanged();
    }

    public List<User> getData() {
        return theList;
    }

    public void addUser(User user, boolean isUpdate) {
        if (isUpdate) {
            int index = theList.indexOf(user);
            notifyItemChanged(index);
            return;
        }
        this.theList.add(user);// = caseFiles;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (theList == null) return 0;
        return theList.size();
    }

    public interface ContactClickListener {
        void onContactClick(User judge, boolean isLongClick);
    }

    class ConversationListHolder extends RecyclerView.ViewHolder {

        //        ImageView newsImage;
        TextView contactName;
        TextView contactInitials;

        TextView chatTime;
        CircleImageView circleImageView;
        TextView chatMessage;

        public ConversationListHolder(final View itemView) {
            super(itemView);
            contactName = itemView.findViewById(R.id.name);
            contactInitials = itemView.findViewById(R.id.contactInitial);
            chatMessage = itemView.findViewById(R.id.text);
            chatTime = itemView.findViewById(R.id.time);
            circleImageView = itemView.findViewById(R.id.contactCircularImage);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    contactClickListener.onContactClick(theList.get(getAdapterPosition()), false);

                }
            });

        }
    }

    class ImageDownload extends AsyncTask<String, Integer, Bitmap> {
        Context context;
        CircleImageView imageView;
        Bitmap bitmap = null;
        InputStream in = null;
        int responseCode = -1;

        //constructor.
        public ImageDownload(Context context, CircleImageView imageView) {
            this.context = context;
            this.imageView = imageView;
        }

        private Bitmap downloadImageBitmap(String sUrl) {
            Bitmap bitmap = null;
            try {
                InputStream inputStream = new URL(sUrl).openStream();   // Download Image from URL
                bitmap = BitmapFactory.decodeStream(inputStream);       // Decode Bitmap
                inputStream.close();
            } catch (Exception e) {
                Log.d("ImageDownload", "Exception 1, Something went wrong!");
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return downloadImageBitmap(params[0]);
        }

        @Override
        protected void onPostExecute(Bitmap data) {
            if (data != null) {
                imageView.setImageBitmap(data);
            }
        }
    }
}





