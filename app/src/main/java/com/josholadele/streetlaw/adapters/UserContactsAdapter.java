package com.josholadele.streetlaw.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.activities.NewChatActivity;
import com.josholadele.streetlaw.activities.PeersActivity;
import com.josholadele.streetlaw.model.CaseFile;
import com.josholadele.streetlaw.model.ChatContact;
import com.josholadele.streetlaw.model.User;
import com.josholadele.streetlaw.network.DownloadImageTask;
import com.josholadele.streetlaw.utils.ContactsFilter;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserContactsAdapter extends RecyclerView.Adapter<UserContactsAdapter.ContactViewHolder> {

    private List<ChatContact> theList;
    public List<ChatContact> filteredList;
    private Context context;
    public ContactsFilter userFilter;
    private JudgeClickListener judgeClickListener;
    HashMap<Integer, CaseFile> selectionList;
    public HashSet<Integer> selectedPositions = new HashSet<>();
    //private onItemClickListener listener;

    PeersActivity activity;

    public boolean IS_MULTISELECT = false;

    public UserContactsAdapter(JudgeClickListener clickListener) {
        this.judgeClickListener = clickListener;
    }

    public UserContactsAdapter(Context context) {

    }

    ;

    Context mContext;

    public UserContactsAdapter(Context context, List<ChatContact> contact, Fragment fragment) {
        this.theList = contact;
        this.filteredList = contact;
        this.mContext = context;
        //this.listener = (onItemClickListener) fragment;
        userFilter = new ContactsFilter(theList, this);
    }

    public interface Listener {
        void onChatContactClicked(ChatContact model);
    }

    ;

    private Listener mListener;
    //private SortedList<ChatContact> mSortedList;

    public UserContactsAdapter(Context context, Comparator<ChatContact> comparator, Listener listener) {
        //super(context, ChatContact.class, comparator);
        mListener = listener;
    }

    public UserContactsAdapter(Context context, Comparator<ChatContact> comparator) {
        mInflater = LayoutInflater.from(context);
        mComparator = comparator;
    }

    public void setSearchResult(List<ChatContact> results) {
        theList = results;
        notifyDataSetChanged();
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        activity = (PeersActivity) context;
        View view;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_contact_item_view, parent, false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {

        ChatContact judge = theList.get(position);
        holder.judgeRole.setText(judge.email);
        holder.judgeName.setText(judge.name);
        String initial = String.valueOf(judge.name.charAt(0));
        holder.judgeInitial.setText(initial);

        String URl = judge.profileImage;
        Log.e("ProfleImageURL", "URL: " + URl);
        new ImageDownload(mContext, holder.profleImage).execute(judge.profileImage);


//        holder.newsImage.setImageResource(R.drawable.ic_hammer);


//        holder.itemView.setActivated(selectedPositions.contains(position));

    }

    public void setContactsData(List<ChatContact> judges) {
        this.theList = judges;
        notifyDataSetChanged();
    }

    public void setUsersData(List<User> users) {
        List<ChatContact> cusers = new ArrayList<ChatContact>();
        for (User usr : users) {
            if (this.theList != null) {
                ChatContact c = new ChatContact(usr.userId, usr.displayName, usr.judicialDivision, usr.userId, usr.username, usr.email, usr.isLawyer);
                c.profileImage = usr.displayPicture;
                cusers.add(c);
            }
        }
        this.theList = cusers;
        notifyDataSetChanged();
    }

    public List<ChatContact> getData() {
        return theList;
    }

    public void addJudge(ChatContact judge) {
        this.theList.add(judge);// = caseFiles;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (theList == null) return 0;
        return theList.size();
    }

    public void clearSelections() {
//        for (int i : selectedPositions) {
//
//        }
        selectedPositions.clear();
        activity.updateToolbar(false);
    }

    private void setCaseStatusColor(ContactViewHolder holder, CaseFile item) {
        switch (item.caseStatus.statusNumber) {

            //Registered
            case 1: {
//                holder.newsImage.setImageResource(R.drawable.ic_case_registered);
                holder.judgeRole.setTextColor(Color.parseColor("#FF5CBB67"));
                break;
            }
            /*
             Registered(1, "Registered"),
    Assigned(2, "Assigned"),
    Transferred(3, "Transferred"),
    Adjourned(4, "Adjourned"),
    Mention(5, "Mention"),
    Hearing(6, "Hearing"),
    Continuation(7, "Continuation"),
    ReportOfService(8, "Report Of Service"),
    FurtherMention(9, "Further Mention"),
    Judgement(10, "Judgement"),
    AdoptionOfWrittenAddresses(11, "Adoption Of Written Addresses"),
    HearingOfApplication(12, "Hearing Of Application"),
    Closed(13, "Closed");
            */

            //Assigned
            case 2: {
//                holder.newsImage.setImageResource(R.drawable.ic_case_assigned);
                holder.judgeRole.setTextColor(Color.parseColor("#FF000000"));
                break;
            }

            //Mention
            case 5: {
//                holder.newsImage.setImageResource(R.drawable.ic_case_mention);
                holder.judgeRole.setTextColor(Color.parseColor("#BF00FF"));
                break;
            }

            //Hearing
            case 6: {
//                holder.newsImage.setImageResource(R.drawable.ic_case_hearing);
                holder.judgeRole.setTextColor(Color.parseColor("#E6625845"));
                break;
            }

            //Continuation
            case 7: {
//                holder.newsImage.setImageResource(R.drawable.ic_case_continuation);
                holder.judgeRole.setTextColor(Color.parseColor("#DD006CF0"));
                break;
            }
//            case CaseStatus.ReportOfService: {
//                holder.newsImage.setImageResource(R.drawable.androidicons_42);
//                holder.noteDate.setTextColor(Color.parseColor("#bdbdbd"));
//                break;
//            }
//            case CaseStatus.FurtherMention: {
//                holder.newsImage.setImageResource(R.drawable.androidicons_42);
//                holder.noteDate.setTextColor(Color.parseColor("#bdbdbd"));
//                break;
//            }

            //Judgement
            case 10: {
//                holder.newsImage.setImageResource(R.drawable.ic_case_judgement);
                holder.judgeRole.setTextColor(Color.parseColor("#2C0F0F"));
                break;
            }
//            case CaseStatus.AdoptionOfWrittenAddresses: {
//                holder.newsImage.setImageResource(R.drawable.androidicons_42);
//                holder.noteDate.setTextColor(Color.parseColor("#bdbdbd"));
//                break;
//            }
//            case CaseStatus.HearingOfApplication: {
//                holder.newsImage.setImageResource(R.drawable.androidicons_42);
//                holder.noteDate.setTextColor(Color.parseColor("#bdbdbd"));
//                break;
//            }
//
            //Closed
            case 13: {
//                holder.newsImage.setImageResource(R.drawable.icons_49);
                holder.judgeRole.setTextColor(Color.parseColor("#EFEF4749"));
                //holder.HammerIcon.Visibility = ViewStates.Invisible;
                break;
            }
        }

    }

    public void deactivate() {
//        for (int i : selectedPositions) {
//
//            theList.get(i).
//        }
    }

    public interface JudgeClickListener {
        void onJudgeClick(ChatContact judge, boolean isLongClick);
    }

    class ContactViewHolder extends RecyclerView.ViewHolder {

        //        ImageView newsImage;
        TextView judgeName;
        TextView judgeInitial;
        CircleImageView profleImage;

//        View lastView;


        TextView judgeRole;
//        TextView releaseDate;
//        TextView genreText;

        public ContactViewHolder(final View itemView) {
            super(itemView);
            judgeName = itemView.findViewById(R.id.judge_name);
            judgeInitial = itemView.findViewById(R.id.judge_initial);
            profleImage = itemView.findViewById(R.id.profile_image);
//            newsImage = itemView.findViewById(R.id.img_case_status);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (IS_MULTISELECT && selectedPositions.contains(getAdapterPosition())) {
                        selectedPositions.remove(getAdapterPosition());
                        itemView.setActivated(false);
                        if (selectedPositions.size() == 0) {
                            IS_MULTISELECT = false;
                        }
                        activity.updateToolbar(IS_MULTISELECT);
                    } else if (IS_MULTISELECT) {
                        selectedPositions.add(getAdapterPosition());
                        itemView.setActivated(true);
                        activity.updateToolbar(IS_MULTISELECT);

                    } else {
                        judgeClickListener.onJudgeClick(theList.get(getAdapterPosition()), false);
                    }
                }
            });
//            itemView.setOnLongClickListener(new View.OnLongClickListener() {
//                @Override
//                public boolean onLongClick(View view) {
//                    if (selectedPositions.size() == 0) {
//                        IS_MULTISELECT = false;
//                    }
//                    if (selectedPositions.contains(getAdapterPosition())) {
//                        selectedPositions.remove(getAdapterPosition());
//                        itemView.setActivated(false);
//                        activity.updateToolbar(IS_MULTISELECT);
//                    } else {
//                        selectedPositions.add(getAdapterPosition());
//                        itemView.setActivated(true);
//                        activity.updateToolbar(IS_MULTISELECT);
//                    }
////                    judgeClickListener.onContactClick(theList.get(getAdapterPosition()), true);
//                    return true;
////                    return false;
//                }
//            });

            judgeRole = (TextView) itemView.findViewById(R.id.judge_role);
//            releaseDate = (TextView) itemView.findViewById(R.id.release_date);
//            genreText = (TextView) itemView.findViewById(R.id.genre_text);

        }
    }

    private final SortedList<ChatContact> mSortedList = new SortedList<>(ChatContact.class, new SortedList.Callback<ChatContact>() {
        @Override
        public int compare(ChatContact a, ChatContact b) {
            return mComparator.compare(a, b);
        }

        @Override
        public void onInserted(int position, int count) {
            notifyItemRangeInserted(position, count);
        }

        @Override
        public void onRemoved(int position, int count) {
            notifyItemRangeRemoved(position, count);
        }

        @Override
        public void onMoved(int fromPosition, int toPosition) {
            notifyItemMoved(fromPosition, toPosition);
        }

        @Override
        public void onChanged(int position, int count) {
            notifyItemRangeChanged(position, count);
        }

        @Override
        public boolean areContentsTheSame(ChatContact oldItem, ChatContact newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areItemsTheSame(ChatContact item1, ChatContact item2) {
            return item1.getId() == item2.getId();
        }
    });

    private LayoutInflater mInflater;
    private Comparator<ChatContact> mComparator;

    public void add(ChatContact model) {
        mSortedList.add(model);
    }

    public void remove(ChatContact model) {
        mSortedList.remove(model);
    }

    public void add(List<ChatContact> models) {
        mSortedList.addAll(models);
    }

    public void remove(List<ChatContact> models) {
        mSortedList.beginBatchedUpdates();
        for (ChatContact model : models) {
            mSortedList.remove(model);
        }
        mSortedList.endBatchedUpdates();
    }

    public void replaceAll(List<ChatContact> models) {
        mSortedList.beginBatchedUpdates();
        for (int i = mSortedList.size() - 1; i >= 0; i--) {
            final ChatContact model = mSortedList.get(i);
            if (!models.contains(model)) {
                mSortedList.remove(model);
            }
        }
        mSortedList.addAll(models);
        mSortedList.endBatchedUpdates();
    }

    private class DownloadImagesTask extends AsyncTask<ImageView, Void, Bitmap> {

        ImageView imageView = null;
        Bitmap bitmap = null;
        InputStream in = null;
        int responseCode = -1;

        @Override
        protected Bitmap doInBackground(ImageView... imageViews) {
            this.imageView = imageViews[0];
            return download_Image((String) imageView.getTag());
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            imageView.setImageBitmap(result);
        }


        private Bitmap download_Image(String image) {
            URL url = null;
            try {
                url = new URL(image);

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(false);
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.connect();
                responseCode = httpURLConnection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    in = httpURLConnection.getInputStream();
                    bitmap = BitmapFactory.decodeStream(in);
                    in.close();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }
    }

    class ImageDownload extends AsyncTask<String, Integer, Bitmap> {
        Context context;
        ImageView imageView;
        Bitmap bitmap = null;
        InputStream in = null;
        int responseCode = -1;

        //constructor.
        public ImageDownload(Context context, ImageView imageView) {
            this.context = context;
            this.imageView = imageView;
        }

        private Bitmap downloadImageBitmap(String sUrl) {
            Bitmap bitmap = null;
            try {
                InputStream inputStream = new URL(sUrl).openStream();   // Download Image from URL
                bitmap = BitmapFactory.decodeStream(inputStream);       // Decode Bitmap
                inputStream.close();
            } catch (Exception e) {
                Log.d("ImageDownload", "Exception 1, Something went wrong!");
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return downloadImageBitmap(params[0]);
            /*
            URL url = null;
            try {
                url = new URL(params[0]);

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(false);
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.connect();
                responseCode = httpURLConnection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    in = httpURLConnection.getInputStream();
                    bitmap = BitmapFactory.decodeStream(in);
                    in.close();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap; */
        }

        @Override
        protected void onPostExecute(Bitmap data) {
            if (data != null) {
                imageView.setImageBitmap(data);
            } else {
                //imageView.setImageDrawable(R.drawable.avatar_contact);
            }
        }
    }
}





