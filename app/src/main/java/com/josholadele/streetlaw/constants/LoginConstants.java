package com.josholadele.streetlaw.constants;

/**
 * Created by josh on 16/03/2018.
 */

public interface LoginConstants {

    String Id = "Id";
    String Category = "Category";
    String JudicialDivision = "JudicialDivision";
    String FirstName = "FirstName";//="Keji";
    String LastName = "LastName";//="Ona";
    String MiddleName = "MiddleName";//="Mercy";
    String UserName = "UserName";//="keji";
    String Gender = "Gender";//="Male";
    String Email = "Email";//="keji@yahoo.com";
    String ShortBio = "ShortBio";//="me";
    String Password = "Password";//=null;
    String Telephone = "Telephone";//="08109357930";
    String AddressLine1 = "AddressLine1";//="VI";
    String AddressLine2 = "AddressLine2";//=null;
    String ProfilePicture = "ProfilePicture";//=null;
    String City = "City";//=null;
    String State = "State";//=null;
    String Country = "Country";//=null;
    String Channel = "Channel";//=0;
    String ChannelName = "ChannelName";//="Android";
    String SupremeCourtEnrollmentNumber = "SupremeCourtEnrollmentNumber";//=null;
    String Firm = "Firm";//=null;
    String Verified = "Verified";//=false;
    String DateRegistered = "DateRegistered";//="2018-01-19T17=59=00.77";
    String YearCalled = "YearCalled";//=null;
    String SubscriptionExpiryTime = "SubscriptionExpiryTime";//="0001-01-01T00=00=00";
    String JudicialDivisionId = "JudicialDivisionId";//=1;
    String Cases = "Cases";//=null;
    String Notifications = "Notifications";//=null;
}