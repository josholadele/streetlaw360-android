package com.josholadele.streetlaw.constants;

/**
 * Created by josh on 16/03/2018.
 */

public interface RegistrationConstants {

    String Category = "Category";
    String JudicialDivision = "JudicialDivision";
    String JudicialDivisionId = "JudicialDivisionId";

    String FirstName = "FirstName";//="Keji";
    String LastName = "LastName";//="Ona";
    String UserName = "UserName";//="keji";
    String Gender = "Gender";//="Male";
    String Email = "Email";//="keji@yahoo.com";
    String ShortBio = "ShortBio";//="me";
    String Password = "Password";//=null;
    String City = "City";//=null;
    String State = "State";//=null;
    String Country = "Country";//=null;
    String Channel = "Channel";//=0;

}