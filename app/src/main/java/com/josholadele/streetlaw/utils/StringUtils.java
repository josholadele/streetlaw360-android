package com.josholadele.streetlaw.utils;

/**
 * Created by josh on 31/03/2018.
 */

public class StringUtils {
    /**
     * This function capitalises the first letter of the word
     * @param word is the word to be capitalised
     * @return
     */
    public static String capitaliseFirstLetter(String word) {

        // Convert the word to lower case
        word = word.toLowerCase();
        // Get and capitalise the first letter,
        // then concatenate it to the remaining letters in the word
        word = word.substring(0, 1).toUpperCase() + word.substring(1);

        return word;
    }
}
