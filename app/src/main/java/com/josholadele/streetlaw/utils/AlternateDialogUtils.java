package com.josholadele.streetlaw.utils;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.josholadele.streetlaw.R;

public class AlternateDialogUtils extends Dialog {

    public static String quickNoticeValue;

    public AlternateDialogUtils(@NonNull Context context) {
        super(context);
    }

    public AlternateDialogUtils(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected AlternateDialogUtils(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public interface UserResponseListeners {
        void onPositive();

        void onNegative();

        void onNeutral();
    }

    /**
     * This function creates a pop-up screen for the user to interact with
     * It includes a notification image
     * @param context
     * @param title
     * @param content
     * @param footerMessage
     * @param listeners
     */
    public static void createDialog(Context context, String title, String content, String footerMessage, final AlternateDialogUtils.UserResponseListeners listeners) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.alternate_pop_up);
        TextView okButton = dialog.findViewById(R.id.ok_button);
        TextView titleText = dialog.findViewById(R.id.alternate_dialog_title);
        TextView contentText = dialog.findViewById(R.id.alternate_dialog_content);
        TextView footerText = dialog.findViewById(R.id.alternate_dialog_footer);

        titleText.setText(title);
        contentText.setText(content);
        footerText.setText(footerMessage);

        if (TextUtils.isEmpty(footerMessage)) {
            footerText.setVisibility(View.GONE);
        }


        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listeners.onPositive();
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
    }

    /**
     * This function displays a plain dialog (i.e without images) for the user to interact with
     * @param context
     * @param title
     * @param content
     * @param listeners
     */
    public static void createPlainDialog(Context context, String title, String content, final AlternateDialogUtils.UserResponseListeners listeners){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.alternate_pop_up_plain);

        TextView okButton = dialog.findViewById(R.id.ok_button_plain);
        TextView cancelButton = dialog.findViewById(R.id.cancel_button_plain);

        TextView titleText = dialog.findViewById(R.id.alternate_dialog_title_plain);
        TextView contentText = dialog.findViewById(R.id.alternate_dialog_content_plain);

        titleText.setText(title);
        contentText.setText(content);


        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listeners.onPositive();
                dialog.dismiss();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listeners.onNegative();
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
    }

    /**
     * This function displays an editable dialog (i.e with an EditText) for the user to interact with
     * @param context
     * @param title - The pop-up header title
     * @param listeners
     */
    public static void createEditTextDialog(Context context, String title, final AlternateDialogUtils.UserResponseListeners listeners){

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.alternate_pop_up_edit_text);

        TextView addButton = dialog.findViewById(R.id.ok_button_et);
        TextView cancelButton = dialog.findViewById(R.id.cancel_button_et);

        TextView titleText = dialog.findViewById(R.id.alternate_dialog_title_et);
        final EditText quickNoticeEdit = dialog.findViewById(R.id.alternate_dialog_content_et);


        titleText.setText(title);



        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quickNoticeValue = quickNoticeEdit.getText().toString();
                listeners.onPositive();
                dialog.dismiss();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listeners.onNegative();
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
    }
}
