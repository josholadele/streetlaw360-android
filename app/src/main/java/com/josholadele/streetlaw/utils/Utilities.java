package com.josholadele.streetlaw.utils;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import java.util.concurrent.TimeUnit;

/**
 * Created by Oladele on 10/26/17.
 */

public class Utilities {

    public static String getDbName(Context context) {
        return context.getPackageName().replace(".", "_");
    }

    public static void hideKeyboard(AppCompatActivity context) {
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static String formatDateTime(long time) {
        return "14:44";
    }

    public static void displayRoundImageFromUrl(Context mContext, String s, ImageView profileImage) {
    }

    public String getFormattedTime(long timestamp) {

        long oneDayInMillis = TimeUnit.DAYS.toMillis(1); // 24 * 60 * 60 * 1000;

        long timeDifference = System.currentTimeMillis() - timestamp;

        return timeDifference < oneDayInMillis
                ? DateFormat.format("hh:mm a", timestamp).toString()
                : DateFormat.format("dd MMM - hh:mm a", timestamp).toString();
    }
}
