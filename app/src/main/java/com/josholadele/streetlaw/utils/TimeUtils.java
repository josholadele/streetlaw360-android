package com.josholadele.streetlaw.utils;

import android.text.format.DateFormat;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * Created by josh on 31/03/2018.
 */

public class TimeUtils {


    public static String getFormattedTime(String timeString) {


        String[] dateTimeSplit = timeString.split("T");
        String[] dateSplit = dateTimeSplit[0].split("-");
        String[] timeSplit = dateTimeSplit[1].split(":");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Integer.parseInt(dateSplit[0]), Integer.parseInt(dateSplit[1]) - 1, Integer.parseInt(dateSplit[2]), Integer.parseInt(timeSplit[0]), Integer.parseInt(timeSplit[1]));
        long timestamp = calendar.getTimeInMillis();

//        long oneDayInMillis = TimeUnit.DAYS.toMillis(1); // 24 * 60 * 60 * 1000;
//
//        long timeDifference = System.currentTimeMillis() - timestamp;
//
//        return timeDifference < oneDayInMillis
//                ? DateFormat.format("hh:mm a", timestamp).toString()
//                : DateFormat.format("dd MMM - hh:mm a", timestamp).toString();
        return getFormattedTime(timestamp);
    }

    public static String getFormattedTime(long timestamp) {

        long oneDayInMillis = TimeUnit.DAYS.toMillis(1); // 24 * 60 * 60 * 1000;

        long timeDifference = System.currentTimeMillis() - timestamp;

        return timeDifference < oneDayInMillis
                ? "Today - " + DateFormat.format("hh:mm a", timestamp).toString()
                : DateFormat.format("dd MMM - hh:mm a", timestamp).toString();
    }

    public static long getLongValue(String timeString) {


        String[] dateTimeSplit = timeString.split("T");
        String[] dateSplit = dateTimeSplit[0].split("-");
        String[] timeSplit = dateTimeSplit[1].split(":");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Integer.parseInt(dateSplit[0]), Integer.parseInt(dateSplit[1]), Integer.parseInt(dateSplit[2]), Integer.parseInt(timeSplit[0]), Integer.parseInt(timeSplit[1]));
        return calendar.getTimeInMillis();
    }

}
