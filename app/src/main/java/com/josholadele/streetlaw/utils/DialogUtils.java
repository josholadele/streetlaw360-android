package com.josholadele.streetlaw.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.load.engine.Resource;
import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.model.CaseFile;

/**
 * Created by josh on 31/03/2018.
 */

public class DialogUtils extends Dialog {
    public DialogUtils(@NonNull Context context) {
        super(context);
    }

    public DialogUtils(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected DialogUtils(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public interface UserResponseListeners {
        void onPositive();

        void onNegative();

        void onNeutral();
    }


    public static void createDialog(Context context, String title, String content, String footerMessage, final UserResponseListeners listeners) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.use_fingerprint_layout);
        Button okButton = dialog.findViewById(R.id.ok_button);
        TextView titleText = dialog.findViewById(R.id.dialog_title);
        TextView contentText = dialog.findViewById(R.id.dialog_content);
        TextView footerText = dialog.findViewById(R.id.dialog_footer);

        titleText.setText(title);
        contentText.setText(content);
        footerText.setText(footerMessage);

        if (TextUtils.isEmpty(footerMessage)) {
            footerText.setVisibility(View.GONE);
        }
        Button dismissButton = dialog.findViewById(R.id.dismiss_button);


        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listeners.onPositive();
                dialog.dismiss();
            }
        });

        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listeners.onNegative();
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
    }
}
