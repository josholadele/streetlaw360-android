package com.josholadele.streetlaw.enums;

/**
 * Created by josh on 20/03/2018.
 */

public enum CaseStatus {
    Registered(1, "Registered"),
    Assigned(2, "Assigned"),
    Transferred(3, "Transferred"),
    Adjourned(4, "Adjourned"),
    Ongoing (5, "Ongoing"),
    Mention(6, "Mention"),
    Hearing(7, "Hearing"),
    Continuation(8, "Continuation"),
    Reportofservice(9, "Report Of Service"),
    Furthermention(9, "Further Mention"),
    Judgement(10, "Judgement"),
    AdoptionOfWrittenAddresses(11, "Adoption Of Written Addresses"),
    HearingOfApplication(12, "Hearing Of Application"),
    Closed(13, "Closed");

    public String statusText;
    public int statusNumber;

    CaseStatus(int i, String registered) {
        statusNumber = i;
        statusText = registered;
    }
}
