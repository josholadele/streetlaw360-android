package com.josholadele.streetlaw.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by josh on 19/03/2018.
 */

public enum UserCategory {
//    ChiefJudge("Chief Judge / Chief Magistrate", "ChiefJudge", 1),
    Judge("Judge / Magistrate", "Judge", 2),
    Lawyer("Lawyer", "Lawyer", 3),
    CourtRegistrar("Court Registrar/Judicial Staff", "JudicialStaff", 4),
    LawStudent("Law Student", "LawStudent", 5),
    Litigant("Litigant", "Litigant", 6);

    public String roleName;
    public String displayName;
    public int Id;

    UserCategory(String displayName, String roleName, int Id) {
        this.displayName = displayName;
        this.roleName = roleName;
        this.Id = Id;
    }

    public static String[] RoleNames() {
        String[] roleNames = new String[]{};
        for (int i = 0; i < UserCategory.values().length; i++) {
            roleNames[i] = UserCategory.values()[i].roleName;
        }
        return roleNames;
    }

    public static String[] DisplayNames() {
        String[] displayNames = new String[]{};
        for (int i = 0; i < UserCategory.values().length; i++) {
            displayNames[i] = UserCategory.values()[i].displayName;
        }
        return displayNames;
    }

    public static List<String> Roles() {
        List<String> roleNames = new ArrayList<>();
        for (int i = 0; i < UserCategory.values().length; i++) {
            roleNames.add(UserCategory.values()[i].displayName);
        }
        return roleNames;
    }

    public static String getRoleNameByDisplay(String displayName) {
        String roleNames = "";
        for (int i = 0; i < UserCategory.values().length; i++) {
            if (displayName.equalsIgnoreCase(UserCategory.values()[i].displayName)) {
                return UserCategory.values()[i].roleName;
            }
        }
        return roleNames;
    }
}
