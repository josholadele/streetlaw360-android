package com.josholadele.streetlaw.network;

import android.content.Context;
import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.josholadele.streetlaw.StreetLaw360App;

import java.io.Serializable;

import com.josholadele.streetlaw.utils.NetworkUtils;

import org.json.JSONObject;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by josh on 10/03/2018.
 */

public class NetworkHelper {


    public void postJSON(String url, JSONObject payload) {

    }


    public NetworkHelper.VolleyResponse<String> getFutureString(String url, String keyTrack, JSONObject jsonRequest) {

        RequestFuture<String> future = RequestFuture.newFuture();

        StringRequest request = new StringRequest("https://google.com", future, future) {

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> headers = new HashMap<>();
//                headers.put("Content-Type", "application/json");
//                headers.put("Authorization", "Basic am9zaF9hcGk6am9zaG1hbg==");
//                return headers;
//            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(120000, 15, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        StreetLaw360App.getInstance().addToRequestQueue(request, keyTrack + Calendar.getInstance().getTimeInMillis());

        try {
            String response = future.get(10, TimeUnit.MINUTES);
            return new NetworkHelper.VolleyResponse<>(true, response);
        } catch (InterruptedException e) {
            // exception handling
            return buildErrorMessage(e.getMessage());
        } catch (ExecutionException e) {
            // exception handling
            return buildErrorMessage(e.getMessage());
        } catch (TimeoutException e) {
            // exception handling
            return buildErrorMessage(e.getMessage());
        } catch (Exception ex) {
            return buildErrorMessage(ex.getMessage());
        }

    }


    public void loadJsonObject(String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params, Response.Listener<JSONObject> volleyCallbackResponse, Response.ErrorListener volleyErrorResponse) {


        JsonObjectRequest request = new JsonObjectRequest(action, url, jsonRequest, volleyCallbackResponse, volleyErrorResponse) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                if (params != null)
                    return params;
                return super.getParams();
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(120000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        StreetLaw360App.getInstance().addToRequestQueue(request, keyTrack + Calendar.getInstance().getTimeInMillis());
    }

    public void loadJsonArray(String url, String keyTrack, int action, JSONArray jsonRequest, final Map<String, String> params, Response.Listener<JSONArray> volleyCallbackResponse, Response.ErrorListener volleyErrorResponse) {


        JsonArrayRequest request = new JsonArrayRequest(action, url, jsonRequest, volleyCallbackResponse, volleyErrorResponse) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                if (params != null)
                    return params;
                return super.getParams();
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(120000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        StreetLaw360App.getInstance().addToRequestQueue(request, keyTrack + Calendar.getInstance().getTimeInMillis());
    }

//    public static HttpHeaders buildAuthHeaders() {
//
//        HttpHeaders requestHeaders = new HttpHeaders();
//
//        HttpAuthentication auth = new HttpBasicAuthentication(
//                "josh_api", "joshman");
//        requestHeaders.setAuthorization(auth);
//
//        return requestHeaders;
//    }


    public NetworkHelper.VolleyResponse<JSONObject> loadFutureJsonObject(String url, String keyTrack, int action, JSONObject jsonRequest) {

        RequestFuture<JSONObject> future = RequestFuture.newFuture();

        JsonObjectRequest request = new JsonObjectRequest(action, url, jsonRequest, future, future) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> getHead = super.getHeaders();
//                getHead.put("Content-Type", "application/json");
//                getHead.put("Authorization", "Basic am9zaF9hcGk6am9zaG1hbg==");
                HashMap<String, String> params = new HashMap<String, String>();
                String creds = String.format("%s:%s","josh_api","joshman");
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
//                getHead.put("Authorization", auth);
//                params.put("Content-Type", "application/json");
//                getHead.put("content-type", "application/json");
//                Map<String, String> headers = new HashMap<>();
//                headers.put("Content-Type", "application/json");
//                headers.put("Authorization", "Basic am9zaF9hcGk6am9zaG1hbg==");
                return super.getHeaders();
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(6000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        StreetLaw360App.getInstance().addToRequestQueue(request, keyTrack + Calendar.getInstance().getTimeInMillis());

        try {
            JSONObject response = future.get(2, TimeUnit.MINUTES);
            return new NetworkHelper.VolleyResponse<>(true, response);
        } catch (InterruptedException e) {
            // exception handling
            return buildErrorMessage(e.getMessage());
        } catch (ExecutionException e) {
            // exception handling
            return buildErrorMessage(e.getMessage());
        } catch (TimeoutException e) {
            // exception handling
            return buildErrorMessage(e.getMessage());
        } catch (Exception ex) {
            return buildErrorMessage(ex.getMessage());
        }

    }

    public NetworkHelper.VolleyResponse<JSONArray> loadFutureJsonArray(String url, String keyTrack, int action, JSONArray jsonRequest, final Map<String, String> params) {

        RequestFuture<JSONArray> future = RequestFuture.newFuture();

        JsonArrayRequest request = new JsonArrayRequest(action, url, jsonRequest, future, future) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                if (params != null)
                    return params;
                return super.getParams();
            }

        };

        request.setRetryPolicy(new DefaultRetryPolicy(120000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        StreetLaw360App.getInstance().addToRequestQueue(request, keyTrack + Calendar.getInstance().getTimeInMillis());
        try {
            JSONArray response = future.get(2, TimeUnit.MINUTES);
            return new NetworkHelper.VolleyResponse<JSONArray>(true, response);
        } catch (Exception e) {
            return buildErrorMessage(e.getMessage());
        }
    }

    public NetworkHelper.VolleyResponse<String> loadFutureStringObject(String url, String keyTrack, int action, final Map<String, String> params) {
        RequestFuture<String> future = RequestFuture.newFuture();
        StringRequest request = new StringRequest(action, url, future, future) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                if (params != null)
                    return params;
                return super.getParams();
            }

            @Override
            public String getBodyContentType() {
                return body.trim().isEmpty() ?
                        super.getBodyContentType() : "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                if (!body.trim().isEmpty())
                    return body.getBytes();
                return "".getBytes();
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(120000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        StreetLaw360App.getInstance().addToRequestQueue(request, keyTrack + Calendar.getInstance().getTimeInMillis());


        String response = null;
        try {
            response = future.get(2, TimeUnit.MINUTES);
            return new NetworkHelper.VolleyResponse<>(true, response);
        } catch (InterruptedException e) {
            return buildErrorMessage(e.getMessage());
        } catch (ExecutionException e) {
            return buildErrorMessage(e.getMessage());
        } catch (TimeoutException e) {
            return buildErrorMessage(e.getMessage());
        }


    }


    public void loadStringObject(String url, String keyTrack, int action, final Map<String, String> params, final Response.Listener<String> volleyCallbackResponse, Response.ErrorListener volleyErrorResponse) {

        StringRequest request = new StringRequest(action, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                volleyCallbackResponse.onResponse(response);
            }
        }, volleyErrorResponse) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                if (params != null)
                    return params;
                return super.getParams();
            }

            @Override
            public String getBodyContentType() {
                return params == null ? "application/json; charset=utf-8" : "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> pars = new HashMap<String, String>();
                pars.put("Content-Type", params == null ? "application/json; charset=utf-8" : "application/x-www-form-urlencoded; charset=UTF-8");
                return pars;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                if (!body.trim().isEmpty())
                    return body.getBytes();
                return super.getBody();
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(120000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        StreetLaw360App.getInstance().addToRequestQueue(request, keyTrack + Calendar.getInstance().getTimeInMillis());
    }


    public void streamStringObj(Context context, String url, String keyTrack, int action, final Map<String, String> params, final Response.Listener<String> volleyCallbackResponse, Response.ErrorListener volleyErrorResponse) {
        if (NetworkUtils.isNetworkAvailable(context)) {
            StringRequest request = new StringRequest(action, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    volleyCallbackResponse.onResponse(response);
                }
            }, volleyErrorResponse) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    if (params != null)
                        return params;
                    return super.getParams();
                }

                @Override
                public String getBodyContentType() {
                    return params == null ? "application/json; charset=utf-8" : "application/x-www-form-urlencoded; charset=UTF-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> pars = new HashMap<String, String>();
                    pars.put("Content-Type", params == null ? "application/json; charset=utf-8" : "application/x-www-form-urlencoded; charset=UTF-8");
                    return pars;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    if (!body.trim().isEmpty())
                        return body.getBytes();
                    return super.getBody();
                }

            };
            StreetLaw360App.getInstance().addToRequestQueue(request, keyTrack + Calendar.getInstance().getTimeInMillis());
        }
    }

    String body = "";

    public void loadStringObject(String url, String keyTrack, int action, String body, final Map<String, String> params, Response.Listener<String> volleyCallbackResponse, Response.ErrorListener volleyErrorResponse) {
        this.body = body;
        loadStringObject(url, keyTrack, action, params, volleyCallbackResponse, volleyErrorResponse);
    }

    public void stream(Context context, String url, String keyTrack, int action, String body, final Map<String, String> params, Response.Listener<String> volleyCallbackResponse, Response.ErrorListener volleyErrorResponse) {
        this.body = body;

        streamStringObj(context, url, keyTrack, action, params, volleyCallbackResponse, volleyErrorResponse);
    }

    public NetworkHelper.VolleyResponse<String> loadFutureStringObject(String url, String keyTrack, int action, String body, final Map<String, String> params) {
        this.body = body;
        return loadFutureStringObject(url, keyTrack, action, params);
    }


    public interface VolleyResponseInterface<T> {
        void onResponse(T t);

        void onGotoError(JSONObject error);

        void onError(String error);
    }

    public NetworkHelper.VolleyResponse buildErrorMessage(String errorMessage) {
        NetworkHelper.VolleyResponse volleyResponse = new NetworkHelper.VolleyResponse(false, null);
        volleyResponse.setErrorMessage(errorMessage);
        return volleyResponse;
    }

    public class VolleyResponse<T> {
        private boolean isSuccess = false;
        private T response;
        private String errorMessage;

        public String getErrorMessage() {
            return this.errorMessage;
        }

        public NetworkHelper.VolleyResponse setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
            return this;
        }

        public VolleyResponse(boolean isSuccess, T response) {
            this.isSuccess = isSuccess;
            this.response = response;
        }

        public boolean isSuccess() {
            return this.isSuccess;
        }

        public NetworkHelper.VolleyResponse setSuccess(boolean success) {
            this.isSuccess = success;
            return this;
        }

        public T getResponse() {
            return this.response;
        }

        public NetworkHelper.VolleyResponse setResponse(T response) {
            this.response = response;
            return this;
        }
    }


}
