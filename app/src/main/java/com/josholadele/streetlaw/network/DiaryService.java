package com.josholadele.streetlaw.network;

import android.os.AsyncTask;

import com.android.volley.Request;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by josh on 10/03/2018.
 */

public class DiaryService {

    private NetworkHelper networkHelper;

    public DiaryService() {
        networkHelper = new NetworkHelper();
    }

    public void addNote(final JSONObject noteObject, final NetworkResponseCallback responseCallback) {
        int channel = 1;
        try {

            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.ADD_NOTE, "addNote", Request.Method.POST, noteObject);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }

    public void updateNote(final JSONObject noteObject, final NetworkResponseCallback responseCallback) {
        int channel = 1;
        try {

            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(String.format(UrlConstants.UPDATE_NOTE), "updateNote", Request.Method.PUT, noteObject);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }

    public void deleteNote(final int noteId, final NetworkResponseCallback responseCallback) {
        try {

            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(String.format(UrlConstants.DELETE_NOTE, noteId), "deleteNote", Request.Method.DELETE, null);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }


    public void getById(final int selectedCaseId, final NetworkResponseCallback responseCallback) {

        new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
            @Override
            protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                return networkHelper.loadFutureJsonObject(UrlConstants.GET_NOTE_BY_ID + selectedCaseId, "getNoteById", Request.Method.GET, null);
            }

            @Override
            protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                super.onPostExecute(response);
                if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                    responseCallback.onResponse(response.getResponse());
                } else if (response.isSuccess()) {
                    responseCallback.onFailed(response.getResponse().optString("Message"));
                } else {
                    responseCallback.onError(response.getErrorMessage());
                }
            }
        }.execute();
    }


    public void getNotesByUserId(final int userId, final NetworkResponseCallback responseCallback) {

        new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
            @Override
            protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                return networkHelper.loadFutureJsonObject(String.format(UrlConstants.GET_NOTES_BY_USER_ID, userId), "getNotes", Request.Method.GET, null);
            }

            @Override
            protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                super.onPostExecute(response);
                if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                    responseCallback.onResponse(response.getResponse());
                } else if (response.isSuccess()) {
                    responseCallback.onFailed(response.getResponse().optString("Message"));
                } else {
                    responseCallback.onError(response.getErrorMessage());
                }
            }
        }.execute();
    }


}
