package com.josholadele.streetlaw.network;

import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.Request;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.josholadele.streetlaw.model.ProfilePictureModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;


/**
 * Created by josh on 10/03/2018.
 */

public class UserService {

    AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>> uploadTask;
    String imageUrl;
    private NetworkHelper networkHelper;

    public UserService() {
        networkHelper = new NetworkHelper();
    }

    public void Authenticate(String email, String password, final NetworkResponseCallback responseCallback) {
        int channel = 1;
        try {
            final JSONObject loginObject = new JSONObject();
            loginObject.put("Email", email);
            loginObject.put("Password", password);
            loginObject.put("Channel", channel);

            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.LOGIN, "login", Request.Method.POST, loginObject);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }

    public void deleteAccount(final int userID, final NetworkResponseCallback responseCallback) {

        try{

            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.DELETE_USER_ACCOUNT + userID, "deleteAccount", Request.Method.DELETE, null);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }



    public void requestToken(final String email, final NetworkResponseCallback responseCallback) {
        try {

            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.SEND_TOKEN + email, "sendtoken", Request.Method.GET, null);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }

    public void resetPassword(final JSONObject resetObject, final NetworkResponseCallback responseCallback) {
        try {

            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.RESET_PASSWORD, "resetPassword", Request.Method.PUT, resetObject);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }

    public void Register(final JSONObject regObject, final NetworkResponseCallback responseCallback) {
        int channel = 1;
        try {
            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.REGISTER, "register", Request.Method.POST, regObject);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }

    public void getJudges(final NetworkResponseCallback responseCallback) {
        try {
            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.GET_JUDGES, "getJudges", Request.Method.GET, null);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }

    public void getJudgesByJudicialDivision(final int judicialDivision, final NetworkResponseCallback responseCallback) {
        try {
            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.GET_JUDGES_BY_DIVISION + judicialDivision, "getJudges", Request.Method.GET, null);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }

    public void getLawyers(final NetworkResponseCallback responseCallback) {
        try {
            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.GET_LAWYERS, "getLawyers", Request.Method.GET, null);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }

    public void getLawyersStudents(final NetworkResponseCallback responseCallback) {
        try {
            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.GET_LAWYERS_STUDENTS, "getContacts", Request.Method.GET, null);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }

    public void getUserChatList(final int uid, final NetworkResponseCallback responseCallback) {
        try {
            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.GET_USER_CHAT_LIST + uid, "getContacts", Request.Method.GET, null);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }

    public void getOnlineCounsellor(final String email, final NetworkResponseCallback responseCallback) {
        try {
            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.GET_COUNSELLOR_LIST + email, "getContacts", Request.Method.GET, null);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }

    public void updateToken(final String userId, final String token) {
        try {
            final JSONObject updateObject = new JSONObject();
            updateObject.put("UserId", userId);
            updateObject.put("FCMToken", token);
            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.UPDATE_FCM_TOKEN, "updateToken", Request.Method.PUT, updateObject);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        Log.e("UPDATETOKEN", "Successful " + token);
//                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        Log.e("UPDATETOKEN", "Failed " + token);
//                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        Log.e("UPDATETOKEN", "Error " + token);
//                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
//            responseCallback.onError(ex.getMessage());
        }
    }

    public void updateProfile(final JSONObject updateObject, final NetworkResponseCallback responseCallback) {
        try {
            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.UPDATE_PROFILE, "updateProfile", Request.Method.PUT, updateObject);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }

    public void updateProfilePicture(String path, final ProfilePictureModel pictureModel, final NetworkResponseCallback responseCallback) {
        try {
            final JSONObject profileObject = new JSONObject();
            profileObject.put("UserId", pictureModel.userId);
//            profileObject.put("FileName", pictureModel.fileName);
            profileObject.put("ProfileImageBase64", pictureModel.base64ProfilePicture);

            MediaManager.get().upload(path)
                    .option("public_id", pictureModel.fileName).callback(new UploadCallback() {
                @Override
                public void onStart(String requestId) {

                }

                @Override
                public void onProgress(String requestId, long bytes, long totalBytes) {

                }

                @Override
                public void onSuccess(String requestId, Map resultData) {
                    imageUrl = resultData.get("url").toString();
                    uploadTask.execute();
                }

                @Override
                public void onError(String requestId, ErrorInfo error) {
                    responseCallback.onError(error.getDescription());
                }

                @Override
                public void onReschedule(String requestId, ErrorInfo error) {

                }
            })
                    .dispatch();

            uploadTask = new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    try {
                        profileObject.put("FileName", imageUrl);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return networkHelper.loadFutureJsonObject(UrlConstants.UPLOAD_DP, "uploadProfilePic", Request.Method.POST, profileObject);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        try {
                            JSONObject object = new JSONObject();
                            object.put("url", imageUrl);
                            responseCallback.onResponse(object);
                        } catch (Exception ignored) {
                            responseCallback.onError(response.getErrorMessage());
                        }
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            };
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }

    public void uploadProfilePicture(String path, final ProfilePictureModel pictureModel, final NetworkResponseCallback responseCallback) {
        try {
            final JSONObject profileObject = new JSONObject();
            profileObject.put("UserId", pictureModel.userId);
//            profileObject.put("FileName", pictureModel.fileName);
            profileObject.put("ProfileImageBase64", pictureModel.base64ProfilePicture);


            MediaManager.get().upload(path)
                    .option("public_id", pictureModel.fileName).callback(new UploadCallback() {
                @Override
                public void onStart(String requestId) {

                }

                @Override
                public void onProgress(String requestId, long bytes, long totalBytes) {

                }

                @Override
                public void onSuccess(String requestId, Map resultData) {
                    imageUrl = resultData.get("url").toString();
                    Log.e("ImageURLResp1", imageUrl);
                    uploadTask.execute();
                }

                @Override
                public void onError(String requestId, ErrorInfo error) {
                    responseCallback.onError(error.getDescription());
                }

                @Override
                public void onReschedule(String requestId, ErrorInfo error) {

                }
            })
                    .dispatch();

            uploadTask = new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    JSONObject profileObject1 = new JSONObject();
                    try {
                        Log.e("ImageURLResp", imageUrl);
                        profileObject1.put("profileImageUrl", imageUrl);
                        profileObject1.put("id", pictureModel.userId);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return networkHelper.loadFutureJsonObject(UrlConstants.UPDATE_PROFILE_IMAGE, "uploadProfilePic", Request.Method.PUT, profileObject1);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        try {
                            JSONObject object = new JSONObject();
                            object.put("url", imageUrl);
                            responseCallback.onResponse(object);
                        } catch (Exception ignored) {
                            responseCallback.onError(response.getErrorMessage());
                        }
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            };
            /*
            String url, String keyTrack, int action, JSONObject jsonRequest, final Map<String, String> params
            */


        } catch (Exception ex) {
            //_logger.Error(ex.Message);
            responseCallback.onError(ex.getMessage());
        }
    }

    public void getUserByID(final int uid, final NetworkResponseCallback responseCallback) {
        try {
            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.GET_USER_BY_ID + uid, "getUser", Request.Method.GET, null);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();

        } catch (Exception ex) {
            responseCallback.onError(ex.getMessage());
        }
    }

    public void getLitigants(final NetworkResponseCallback responseCallback) {
        try {
            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.GET_LITIGANTS, "getLitigants", Request.Method.GET, null);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();

        } catch (Exception ex) {
            responseCallback.onError(ex.getMessage());
        }
    }

    public void getUsersMessageState(final int uid, final NetworkResponseCallback responseCallback) {
        try {
            new AsyncTask<Void, Void, NetworkHelper.VolleyResponse<JSONObject>>() {
                @Override
                protected NetworkHelper.VolleyResponse<JSONObject> doInBackground(Void... voids) {
                    return networkHelper.loadFutureJsonObject(UrlConstants.GET_USERS_MESSAGE_STATE + uid, "getUsers", Request.Method.GET, null);
                }

                @Override
                protected void onPostExecute(NetworkHelper.VolleyResponse<JSONObject> response) {
                    super.onPostExecute(response);
                    if (response.isSuccess() && response.getResponse().optString("StatusCode").equalsIgnoreCase("00")) {
                        responseCallback.onResponse(response.getResponse());
                    } else if (response.isSuccess()) {
                        responseCallback.onFailed(response.getResponse().optString("Message"));
                    } else {
                        responseCallback.onError(response.getErrorMessage());
                    }
                }
            }.execute();

        } catch (Exception ex) {
            responseCallback.onError(ex.getMessage());
        }
    }
}
