package com.josholadele.streetlaw.network;

import org.json.JSONObject;

/**
 * Created by josh on 10/03/2018.
 */

public interface NetworkResponseCallback {

    void onResponse(JSONObject response);

    void onFailed(String reason);

    void onError(String reason);
}
