package com.josholadele.streetlaw.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.activities.NewCaseActivity;
import com.josholadele.streetlaw.activities.NewNoteActivity;
import com.josholadele.streetlaw.activities.NoteDetailsActivity;
import com.josholadele.streetlaw.adapters.NotesAdapter;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.enums.CaseStatus;
import com.josholadele.streetlaw.model.CaseFile;
import com.josholadele.streetlaw.model.Note;
import com.josholadele.streetlaw.network.CaseService;
import com.josholadele.streetlaw.network.DiaryService;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.utils.StringUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class DiaryFragment extends Fragment implements NotesAdapter.NoteClickListener {

    public static final int ADD_NOTE_REQUEST_CODE = 1004;
    public static byte[] isoTemplate = null;
    Handler mHandler;
    SharedPreferences sPref;
    View view = null;
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    NotesAdapter mAdapter;
    AppSharedPref _sharedPref;
    ImageButton mImgBtnFooter;
    TextView mFooterTextView;
    LinearLayout mLinearLayoutScreenFooterTextIcon;
//    List<NoteTableItemViewModel> _tableItems;
    LinearLayout emptyViewLayout;
    CalendarView mCalendarView;
    ScrollView mScrollViewNotes;
    private Button btnCapture;
    private ImageView imgFP;
    private TextView lblResponse;
    private Context mContext;
    private LinearLayout mLinearLayoutEmptyState;
    private LinearLayout mLinearLayoutErrorState;
    private LinearLayout mLinearLayoutEdiaryPageProgressBar;
    private DiaryService _noteService;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_e_diary, null);
        mCalendarView = view.findViewById(R.id.calendar_ediary);
        mRecyclerView = view.findViewById(R.id.ediary_recyclerView);
        mLinearLayoutErrorState = view.findViewById(R.id.errorstate_notes);
        mLinearLayoutEdiaryPageProgressBar = view.findViewById(R.id.linearLayoutEdiaryPageProgressBar);
        mScrollViewNotes = view.findViewById(R.id.scrollView_notes);
//        mLinearLayoutEmptyState = view.findViewById(R.id.emptystate_notes);

        emptyViewLayout = view.findViewById(R.id.empty_view);

        View screenFooter = view.findViewById(R.id.footer_ediary);
        mImgBtnFooter = screenFooter.findViewById(R.id.img_footericon);
        mLinearLayoutScreenFooterTextIcon = screenFooter.findViewById(R.id.linearlayout_footer_texticon);
        mFooterTextView = screenFooter.findViewById(R.id.txt_footer);
        CircleImageView footerImage = screenFooter.findViewById(R.id.img_footer_profile_image);

        Typeface american_typewriter = Typeface.createFromAsset(getContext().getAssets(), "fonts/American_Typewriter_Regular.ttf");
        mFooterTextView.setTypeface(american_typewriter, Typeface.BOLD);
        mFooterTextView.setText("New Note");

        mImgBtnFooter.setImageResource(R.drawable.ic_new_diary);
        mLayoutManager = new LinearLayoutManager(mContext);

        mRecyclerView.setLayoutManager(mLayoutManager);


        mFooterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchAddNewNote();
            }
        });
        mImgBtnFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchAddNewNote();
            }
        });


        mRecyclerView.setAdapter(mAdapter);
//        mAdapter.ItemClick += OnItemClick;
//        mLinearLayoutScreenFooterTextIcon.Click += MLinearLayoutScreenFooterTextIcon_Click;

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchNotes(_sharedPref.getUserId());

    }

    private void launchAddNewNote() {
        Intent intent = new Intent(getContext(), NewNoteActivity.class);
        startActivityForResult(intent, ADD_NOTE_REQUEST_CODE);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new NotesAdapter(this);
        _sharedPref = new AppSharedPref(getContext());
    }


    void fetchNotes(String userId) {

        showLoading();
        DiaryService diaryService = new DiaryService();
        diaryService.getNotesByUserId(Integer.parseInt(userId), new NetworkResponseCallback() {
            @Override
            public void onResponse(JSONObject response) {
                List<Note> noteList = buildFromResult(response.optJSONArray("Data"));
                if (noteList != null && noteList.size() > 0) {
                    showDataView();
                    mAdapter.setNoteData(noteList);
                } else {
                    showErrorMessage();
                }
            }

            @Override
            public void onFailed(String reason) {
                showErrorMessage();
            }

            @Override
            public void onError(String reason) {
                showErrorMessage();
            }
        });
    }


    private List<Note> buildFromResult(JSONArray result) {

        if (result == null || result.equals("")) {
            return null;
        }

        JSONObject movieJSONObject;
        JSONArray movieJSONArray = null;
        try {
//                movieJSONObject = new JSONObject(result);
//                if (isItem) {
//                    movieJSONArray = movieJSONObject.optJSONArray("items");
//                } else {
//                    movieJSONArray = movieJSONObject.optJSONArray("results");
//                }
            movieJSONArray = result;
        } catch (Exception e) {
            e.printStackTrace();
        }


        List<Note> notes = new ArrayList<>();


        for (int i = 0; i < movieJSONArray.length(); i++) {
            JSONObject object = movieJSONArray.optJSONObject(i);

            int id = object.optInt("Id");
            int userId = Integer.parseInt(_sharedPref.getUserId());
            String topic = object.optString("Topic");
            String content = object.optString("Content");
            String dateCreated = object.optString("DateCreated");
            String dateLastUpdated = object.optString("LastDateOfUpdate");

            notes.add(new Note(id, userId, topic, content, dateCreated, dateLastUpdated));

        }
        return notes;
    }

    private void showErrorMessage() {
        emptyViewLayout.setVisibility(View.VISIBLE);
        mScrollViewNotes.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
        mLinearLayoutEdiaryPageProgressBar.setVisibility(View.GONE);
    }

    private void showDataView() {
        emptyViewLayout.setVisibility(View.GONE);
        mScrollViewNotes.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.VISIBLE);
        mLinearLayoutEdiaryPageProgressBar.setVisibility(View.GONE);
    }

    private void showLoading() {
        emptyViewLayout.setVisibility(View.GONE);
        mScrollViewNotes.setVisibility(View.INVISIBLE);
//        mRecyclerView.setVisibility(View.INVISIBLE);
        mLinearLayoutEdiaryPageProgressBar.setVisibility(View.VISIBLE);
//        addCaseLayout.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                getActivity().finish();
            }
            break;
            default:
                break;
        }
        return true;
    }


    @Override
    public void onNoteClick(Note note, boolean isLongClick) {
        Intent intent = new Intent(getContext(), NoteDetailsActivity.class);
        intent.putExtra("note", note);
        intent.putExtra("note-Id", note.Id);
        startActivity(intent);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_NOTE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                Note note = data.getParcelableExtra("note");
                showDataView();
                mAdapter.addNote(note);
                Toast.makeText(getContext(), note.topic + " Added!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}