package com.josholadele.streetlaw.fragments;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.util.SortedList;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;

import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.StreetLaw360App;
import com.josholadele.streetlaw.activities.ChatScreenActivity;
import com.josholadele.streetlaw.adapters.ContactsAdapter;
import com.josholadele.streetlaw.adapters.PeerAdapter;
import com.josholadele.streetlaw.adapters.UserContactsAdapter;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.dummy.DummyContent;
import com.josholadele.streetlaw.dummy.DummyContent.DummyItem;
import com.josholadele.streetlaw.model.CaseFile;
import com.josholadele.streetlaw.model.ChatContact;
import com.josholadele.streetlaw.model.User;
import com.josholadele.streetlaw.model.UserDao;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.network.UserService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ContactsFragment extends Fragment implements UserContactsAdapter.JudgeClickListener, SearchView.OnQueryTextListener {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private ContactsFragment.OnListFragmentInteractionListener mListener;
    RecyclerView contactsRecycler;
    UserContactsAdapter mAdapter;
    Toolbar mToolbar;

    TextView toolbarTitle;
    TextView toolbarSelectionText;

    TextView addCaseTV;
    ImageView addCaseImg;


    ProgressBar contentLoading;
    TextView emptyView;
    Fragment fragment;
    MenuItem assignMenu;
    MenuItem filterMenu;
    private List<ChatContact> mModels;
    AppSharedPref _sharedPref;
    ArrayList<Integer> selectedCaseIds;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ContactsFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ContactsFragment newInstance(int columnCount) {
        ContactsFragment fragment = new ContactsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }

        _sharedPref = new AppSharedPref(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //View view = inflater.inflate(R.layout.fragment_contacts_item_list, container, false);

        // Set the adapter
        /*if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new ContactsAdapter(DummyContent.ITEMS, mListener));
        }
        return view; */

        View view = inflater.inflate(R.layout.peer_fragment_page, container, false);
        // fragment_peer_item_list
        contentLoading = view.findViewById(R.id.progress_layout);
        emptyView = view.findViewById(R.id.empty_view);

        contactsRecycler = view.findViewById(R.id.contact_recyclerview);


        setupRecyclerView(contactsRecycler);

        fetchContacts();
        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            //recyclerView.setAdapter(new ContactsAdapter(DummyContent.ITEMS, mListener));
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        // Inflate the menu; this adds items to the action bar if it is present.
        /*inflater.inflate(R.menu.menu_peers, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);

        super.onCreateOptionsMenu(menu, inflater);*/
        // Inflate menu to add items to action bar if it is present.

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        item.setOnActionExpandListener( new MenuItem.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {

                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                mAdapter.setSearchResult(mModels);
                return true; // Return true to collapse action view

            }
        });

        /* searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.e("TypedText", newText);
                if (fragment instanceof ContactsFragment)
                    ((ContactsFragment) fragment).mAdapter.getFilter().filter(newText);
                return false;
            }
        });
        super.onPrepareOptionsMenu(menu); */
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        // Here is where we are going to implement the filter logic
        final List<ChatContact> filteredModelList = filter(mModels, query);
        if(filteredModelList.size() > 0) {
            mAdapter.setSearchResult(filteredModelList);
            contactsRecycler.scrollToPosition(0);
            showMovieDataView();
        } else {
            mAdapter.setSearchResult(filteredModelList);
            showErrorMessage();
        }
        return true;
    }

    private static List<ChatContact> filter(List<ChatContact> models, String query) {
        final String lowerCaseQuery = query.toLowerCase();

        final List<ChatContact> filteredModelList = new ArrayList<>();
        if(null != models) {
            for (ChatContact model : models) {
                final String text = model.getName().toLowerCase();
                final String email = model.getEmail().toLowerCase();
                if (text.contains(lowerCaseQuery) || email.contains(lowerCaseQuery)) {
                    filteredModelList.add(model);
                }
            }
        }
        return filteredModelList;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DummyItem item);
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        mAdapter = new UserContactsAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(mAdapter);
//        getSampleCaseFiles();
    }

    List<CaseFile> files;

    void fetchContacts() {

        showLoading();
        /*UserDao userDao = StreetLaw360App.getInstance().getDaoSession().getUserDao();
        List<User> users = userDao.queryBuilder()
                //.where(UserDao.Properties.userId.eq("Joe"))
                //.orderAsc(UserDao.Properties.LastName)
                .list();
        mAdapter.setContactsData(users); */

        UserService userService = new UserService();
        Log.e("ContactsFragment", _sharedPref.getUserId());
        userService.getUserChatList(Integer.valueOf(_sharedPref.getUserId()), new NetworkResponseCallback() {
            @Override
            public void onResponse(JSONObject response) {

                List<ChatContact> judgeList = buildFromResult(response.optJSONArray("Data"));
                //Log.e("ChatContact", judgeList.toString());
                if (judgeList != null && judgeList.size() > 0) {
                    showMovieDataView();
                    mModels = judgeList;
                    mAdapter.setContactsData(judgeList);
                } else {
                    showErrorMessage();
                }
            }

            @Override
            public void onFailed(String reason) {
                showErrorMessage();
            }

            @Override
            public void onError(String reason) {
                showErrorMessage();
            }
        });
    }

    private void showErrorMessage() {
        emptyView.setVisibility(View.VISIBLE);
        contactsRecycler.setVisibility(View.GONE);
        contentLoading.setVisibility(View.GONE);

    }

    private void showMovieDataView() {
        emptyView.setVisibility(View.GONE);
        contactsRecycler.setVisibility(View.VISIBLE);
        contentLoading.setVisibility(View.GONE);
    }

    private void showLoading() {
        emptyView.setVisibility(View.GONE);
        contactsRecycler.setVisibility(View.GONE);
        contentLoading.setVisibility(View.VISIBLE);
    }

    private List<ChatContact> buildFromResult(JSONArray result) {

        if (result == null || result.equals("")) {
            return null;
        }

        JSONArray movieJSONArray = null;
        try {
            movieJSONArray = result;
        } catch (Exception e) {
            e.printStackTrace();
        }


        List<ChatContact> judges = new ArrayList<>();


        for (int i = 0; i < movieJSONArray.length(); i++) {
            JSONObject object = movieJSONArray.optJSONObject(i);
            String name = object.optString("FirstName") + " " + object.optString("LastName");
            String category = object.optString("Category");
//            Judge judge = new Judge(object.optInt("Id"), name,
//                    object.optString("JudicialDivision"),
//                    object.optString("UserName"),
//                    object.optString("Email"));
            ChatContact chatContact = new ChatContact(0, name, "", object.optInt("Id"), object.optString("Username"), object.optString("Email"), category.equalsIgnoreCase("lawyer"));
            chatContact.profileImage = object.optString("ProfilePicture");
            judges.add(chatContact);
        }
        return judges;
    }

    @Override
    public void onJudgeClick(final ChatContact selectedContact, boolean isLongClick) {

        ChatScreenActivity.launch(getActivity(), selectedContact);
        //getActivity().finish();
    }

    public void updateToolbar(boolean is_multiselect) {
        if (is_multiselect) {
            toolbarSelectionText.setVisibility(View.VISIBLE);
            String selectedNumer = mAdapter.selectedPositions.size() == 1 ? mAdapter.selectedPositions.size() + " case selected" : mAdapter.selectedPositions.size() + " cases selected";
            toolbarTitle.setVisibility(View.GONE);
            toolbarSelectionText.setText(selectedNumer);
            assignMenu.setVisible(true);
            filterMenu.setVisible(false);
        } else {
            toolbarSelectionText.setText("");
            toolbarSelectionText.setVisibility(View.GONE);
            toolbarTitle.setVisibility(View.VISIBLE);
            assignMenu.setVisible(false);
            filterMenu.setVisible(true);
        }
    }
}
