package com.josholadele.streetlaw.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.activities.SignUpActivity;
import com.josholadele.streetlaw.constants.RegistrationConstants;

public class ShortBioFragment extends Fragment {

    View rootView = null;
    EditText mShortBio;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.welcome_slide4, null);

//        initGUI();

        mShortBio = rootView.findViewById(R.id.editShortBio);
        activity = (SignUpActivity) getActivity();

        return rootView;
    }

    SignUpActivity activity;

    public boolean onNextClicked() {
        if (TextUtils.isEmpty(mShortBio.getText().toString())) {
            Toast.makeText(activity, "Please fill the required field", Toast.LENGTH_SHORT).show();
            return false;
        }

        activity.insertProperty(RegistrationConstants.ShortBio, mShortBio.getText().toString());
        return true;
    }

}