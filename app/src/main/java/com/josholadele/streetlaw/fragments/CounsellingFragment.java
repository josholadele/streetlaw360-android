package com.josholadele.streetlaw.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.activities.ChatScreenActivity;
import com.josholadele.streetlaw.activities.CounsellingActivity;
import com.josholadele.streetlaw.adapters.OnlineCounsellingAdapter;
import com.josholadele.streetlaw.adapters.UserContactsAdapter;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.model.CaseFile;
import com.josholadele.streetlaw.model.ChatContact;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.network.UserService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CounsellingFragment extends Fragment implements OnlineCounsellingAdapter.JudgeClickListener, SearchView.OnQueryTextListener {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private ContactsFragment.OnListFragmentInteractionListener mListener;
    RecyclerView contactsRecycler;
    OnlineCounsellingAdapter mAdapter;
    Toolbar mToolbar;
    Context mContext;

    TextView toolbarTitle;
    TextView toolbarSelectionText;

    TextView addCaseTV;
    ImageView addCaseImg;


    ProgressBar contentLoading;
    TextView emptyView;
    Fragment fragment;
    MenuItem assignMenu;
    MenuItem filterMenu;
    private List<ChatContact> mModels;
    AppSharedPref _sharedPref;
    ArrayList<Integer> selectedCaseIds;
    public CounsellingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        _sharedPref = new AppSharedPref(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.peer_fragment_page, container, false);
        // fragment_peer_item_list
        contentLoading = view.findViewById(R.id.progress_layout);
        emptyView = view.findViewById(R.id.empty_view);

        contactsRecycler = view.findViewById(R.id.contact_recyclerview);

        setupRecyclerView(contactsRecycler);

        fetchContacts();
        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            //recyclerView.setAdapter(new ContactsAdapter(DummyContent.ITEMS, mListener));
        }
        return view;
    }

    /*
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_peers, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = new SearchView(((CounsellingActivity) mContext).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        searchView.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {

                                          }
                                      }
        );
    } */

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        item.setOnActionExpandListener( new MenuItem.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {

                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                mAdapter.setSearchResult(mModels);
                return true; // Return true to collapse action view

            }
        });

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        // Here is where we are going to implement the filter logic
        Log.e("Got hre", mModels.get(0).toString());
        final List<ChatContact> filteredModelList = filter(mModels, query);
        if(filteredModelList.size() > 0) {
            mAdapter.setSearchResult(filteredModelList);
            contactsRecycler.scrollToPosition(0);
            showMovieDataView();
        } else {
            mAdapter.setSearchResult(filteredModelList);
            showErrorMessage();
        }
        return true;
    }

    private static List<ChatContact> filter(List<ChatContact> models, String query) {
        final String lowerCaseQuery = query.toLowerCase();

        final List<ChatContact> filteredModelList = new ArrayList<>();
        if(null != models) {
            for (ChatContact model : models) {
                final String text = model.getName().toLowerCase();
                final String email = model.getEmail().toLowerCase();
                if (text.contains(lowerCaseQuery) || email.contains(lowerCaseQuery)) {
                    filteredModelList.add(model);
                }
            }
        }
        return filteredModelList;
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        mAdapter = new OnlineCounsellingAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(mAdapter);
//        getSampleCaseFiles();
    }

    List<CaseFile> files;

    void fetchContacts() {

        showLoading();
        /*UserDao userDao = StreetLaw360App.getInstance().getDaoSession().getUserDao();
        List<User> users = userDao.queryBuilder()
                //.where(UserDao.Properties.userId.eq("Joe"))
                //.orderAsc(UserDao.Properties.LastName)
                .list();
        mAdapter.setContactsData(users); */

        UserService userService = new UserService();
        String userEmail = _sharedPref.getEmail();
        Log.e("ContactsFragment", _sharedPref.getUserId());
        // userService.getLitigants(Integer.valueOf(_sharedPref.getUserId()), new NetworkResponseCallback()
        if(userEmail.equalsIgnoreCase("streetlaw360@gmail.com")) {
            userService.getLitigants(new NetworkResponseCallback() {
                @Override
                public void onResponse(JSONObject response) {
                    List<ChatContact> judgeList = buildFromResult(response.optJSONArray("Data"));
                    Log.e("ChatContact", judgeList.toString());
                    if (judgeList != null && judgeList.size() > 0) {
                        showMovieDataView();
                        mModels = judgeList;
                        mAdapter.setContactsData(judgeList);
                    } else {
                        showErrorMessage();
                    }
                }

                @Override
                public void onFailed(String reason) {
                    showErrorMessage();
                }

                @Override
                public void onError(String reason) {
                    showErrorMessage();
                }
            });
        } else {
            userService.getOnlineCounsellor("streetlaw360@gmail.com", new NetworkResponseCallback() {
                @Override
                public void onResponse(JSONObject response) {

                    List<ChatContact> judgeList = buildFromResult(response.optJSONArray("Data"));
                    Log.e("ChatContact", judgeList.toString());
                    if (judgeList != null && judgeList.size() > 0) {
                        showMovieDataView();
                        mModels = judgeList;
                        mAdapter.setContactsData(judgeList);
                    } else {
                        showErrorMessage();
                    }
                }

                @Override
                public void onFailed(String reason) {
                    showErrorMessage();
                }

                @Override
                public void onError(String reason) {
                    showErrorMessage();
                }
            });
        }
    }

    private void showErrorMessage() {
        emptyView.setVisibility(View.VISIBLE);
        contactsRecycler.setVisibility(View.GONE);
        contentLoading.setVisibility(View.GONE);

    }

    private void showMovieDataView() {
        emptyView.setVisibility(View.GONE);
        contactsRecycler.setVisibility(View.VISIBLE);
        contentLoading.setVisibility(View.GONE);
    }

    private void showLoading() {
        emptyView.setVisibility(View.GONE);
        contactsRecycler.setVisibility(View.GONE);
        contentLoading.setVisibility(View.VISIBLE);
    }

    private List<ChatContact> buildFromResult(JSONArray result) {

        if (result == null || result.equals("")) {
            return null;
        }

        JSONArray movieJSONArray = null;
        try {
            movieJSONArray = result;
        } catch (Exception e) {
            e.printStackTrace();
        }


        List<ChatContact> judges = new ArrayList<>();


        for (int i = 0; i < movieJSONArray.length(); i++) {
            JSONObject object = movieJSONArray.optJSONObject(i);
            String name = object.optString("FirstName") + " " + object.optString("LastName");
            String category = object.optString("Category");
//            Judge judge = new Judge(object.optInt("Id"), name,
//                    object.optString("JudicialDivision"),
//                    object.optString("UserName"),
//                    object.optString("Email"));
            ChatContact chatContact = new ChatContact(0, name, "", object.optInt("Id"), object.optString("Username"), object.optString("Email"), category.equalsIgnoreCase("lawyer"));
            chatContact.profileImage = object.optString("ProfilePicture");
            Log.e("ChatCounselling", chatContact.toString());
            judges.add(chatContact);
        }
        return judges;
    }

    @Override
    public void onJudgeClick(final ChatContact selectedContact, boolean isLongClick) {

        ChatScreenActivity.launch(getActivity(), selectedContact);
        //getActivity().finish();
    }

}
