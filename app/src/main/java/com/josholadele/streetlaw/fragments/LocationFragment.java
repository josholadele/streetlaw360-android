package com.josholadele.streetlaw.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.activities.SignUpActivity;
import com.josholadele.streetlaw.constants.RegistrationConstants;
import com.josholadele.streetlaw.enums.Role;
import com.josholadele.streetlaw.model.JudicialDivision;

public class LocationFragment extends Fragment {

    View rootView = null;

    Spinner spinnerCity;
    Spinner spinnerState;
    Spinner spinnerCountry;
    Spinner spinnerJudicialDivision;
    TextView divisionTitleText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.welcome_slide3, null);

//        initGUI();
        spinnerCity = rootView.findViewById(R.id.spinnerCity);
        spinnerState = rootView.findViewById(R.id.spinnerState);
        spinnerCountry = rootView.findViewById(R.id.spinnerCountry);
        spinnerJudicialDivision = rootView.findViewById(R.id.spinnerJudicialDivision);
        divisionTitleText = rootView.findViewById(R.id.division_title);

        activity = (SignUpActivity) getActivity();

        final String[] cities = new String[]{"Lagos"};
        final String[] countries = new String[]{"Nigeria"};
        final String[] states = new String[]{"Lagos"};
        final String[] judicialDivisions = new String[]{"Ikeja", "Lagos Island"};
        final JudicialDivision[] jDivisions = new JudicialDivision[]{
                new JudicialDivision(9, "Ikeja", "Lagos"),
                new JudicialDivision(10, "Ikorodu", "Lagos"),
                new JudicialDivision(11, "Yaba", "Lagos"),
                new JudicialDivision(12, "Lagos Island", "Lagos"),
                new JudicialDivision(13, "Epe", "Lagos"),
                new JudicialDivision(14, "Apapa", "Lagos"),
                new JudicialDivision(15, "Badagry", "Lagos")
        };


        // (3) create an adapter from the list
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(
                getActivity(),
                R.layout.spinner_item_view,
                cities
        );

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(
                getActivity(),
                R.layout.spinner_item_view,
                countries
        );

        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(
                getActivity(),
                R.layout.spinner_item_view,
                states
        );

        ArrayAdapter<JudicialDivision> adapter4 = new ArrayAdapter<JudicialDivision>(
                getActivity(),
                R.layout.spinner_item_view,
                jDivisions
        );


        spinnerCity.setAdapter(adapter1);
        spinnerCountry.setAdapter(adapter2);
        spinnerState.setAdapter(adapter3);
        spinnerJudicialDivision.setAdapter(adapter4);

        if (activity.regObject.optString(RegistrationConstants.Category).equalsIgnoreCase(Role.Judge.roleName) ||
                activity.regObject.optString(RegistrationConstants.Category).equalsIgnoreCase(Role.CourtRegistrar.roleName)) {
            spinnerJudicialDivision.setVisibility(View.VISIBLE);
            divisionTitleText.setVisibility(View.VISIBLE);
        } else {
            spinnerJudicialDivision.setVisibility(View.GONE);
            divisionTitleText.setVisibility(View.GONE);
        }


        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedItem = cities[i];
                ((SignUpActivity) getActivity()).insertProperty(RegistrationConstants.City, selectedItem);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getContext(), "Please select a city", Toast.LENGTH_SHORT).show();

            }
        });

        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedItem = states[i];
                ((SignUpActivity) getActivity()).insertProperty(RegistrationConstants.State, selectedItem);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getContext(), "Please select a state", Toast.LENGTH_SHORT).show();

            }
        });

        spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedItem = countries[i];
                ((SignUpActivity) getActivity()).insertProperty(RegistrationConstants.Country, selectedItem);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getContext(), "Please select a country", Toast.LENGTH_SHORT).show();

            }
        });

        /*spinnerJudicialDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedItem = judicialDivisions[i];
                ((SignUpActivity) getActivity()).insertProperty(RegistrationConstants.JudicialDivision, selectedItem);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getContext(), "Please select a judicial division", Toast.LENGTH_SHORT).show();

            }
        }); */

        spinnerJudicialDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                JudicialDivision selectedItem = jDivisions[i];
                ((SignUpActivity) getActivity()).insertProperty(RegistrationConstants.JudicialDivision, selectedItem.getArea());
                ((SignUpActivity) getActivity()).insertProperty(RegistrationConstants.JudicialDivisionId, selectedItem.getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getContext(), "Please select a judicial division", Toast.LENGTH_SHORT).show();

            }
        });


        return rootView;
    }

    SignUpActivity activity;

    public boolean onNextClicked() {

        return true;
    }

}