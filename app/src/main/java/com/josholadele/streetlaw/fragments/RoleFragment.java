package com.josholadele.streetlaw.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.activities.SignUpActivity;
import com.josholadele.streetlaw.constants.RegistrationConstants;
import com.josholadele.streetlaw.enums.Role;

import java.util.ArrayList;
import java.util.List;

public class RoleFragment extends Fragment {


    View rootView = null;
    Spinner spinnerRole;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.welcome_slide1, null);

        final List<String> spinnerItems = Role.Roles();

        spinnerRole = rootView.findViewById(R.id.spinnerRole);

        // (3) create an adapter from the list
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                getActivity(),
                R.layout.spinner_item_view,
                spinnerItems
        );

        spinnerRole.setAdapter(adapter);

        spinnerRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedItem = Role.getRoleNameByDisplay(spinnerItems.get(i));
                ((SignUpActivity) getActivity()).insertProperty(RegistrationConstants.Category, selectedItem);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getContext(), "Please select a role", Toast.LENGTH_SHORT).show();

            }
        });
        return rootView;
    }

}