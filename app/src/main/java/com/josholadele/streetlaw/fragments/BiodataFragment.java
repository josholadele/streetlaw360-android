package com.josholadele.streetlaw.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.activities.SignUpActivity;
import com.josholadele.streetlaw.constants.RegistrationConstants;
import com.josholadele.streetlaw.enums.Role;

import java.util.List;

public class BiodataFragment extends Fragment {


    View rootView = null;
    Spinner spinnerGender;
    TextView mFirstName;
    TextView mLastName;
    TextView mEmailAddress;
    TextView mUserName;
    TextView mPassword;

    public String getFirstName() {
        return mFirstName.getText().toString();
    }

    public String getLastName() {
        return mLastName.getText().toString();
    }


    public String getEmailAddress() {
        return mEmailAddress.getText().toString();
    }

    public String getUsername() {
        return mUserName.getText().toString();
    }

    public String getPassword() {
        return mPassword.getText().toString();
    }

//    public String firstName, lastName, gender, emailAddress, username, password;

    SignUpActivity activity;

    public boolean onNextClicked() {

        if (TextUtils.isEmpty(getFirstName()) || TextUtils.isEmpty(getLastName()) || TextUtils.isEmpty(getEmailAddress()) || TextUtils.isEmpty(getUsername()) || TextUtils.isEmpty(getPassword())) {
            Toast.makeText(activity, "Please fill all fields", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!isEmailValid(getEmailAddress())) {
            Toast.makeText(activity, "Please enter a valid email", Toast.LENGTH_SHORT).show();
            return false;
        }

        activity.insertProperty(RegistrationConstants.FirstName, getFirstName());
        activity.insertProperty(RegistrationConstants.LastName, getLastName());
        activity.insertProperty(RegistrationConstants.Email, getEmailAddress());
        activity.insertProperty(RegistrationConstants.UserName, getUsername());
        activity.insertProperty(RegistrationConstants.Password, getPassword());
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.welcome_slide2, null);

        final List<String> spinnerItems = Role.Roles();

        spinnerGender = rootView.findViewById(R.id.spinnerGender);
        mFirstName = rootView.findViewById(R.id.editFirstName);
        mLastName = rootView.findViewById(R.id.editLastName);
        mUserName = rootView.findViewById(R.id.editUserName);
        mEmailAddress = rootView.findViewById(R.id.editEmailAddress);
        mPassword = rootView.findViewById(R.id.editPassword);

        activity = (SignUpActivity) getActivity();

        final String[] genderArray = new String[]{"Male", "Female"};

        // (3) create an adapter from the list
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getActivity(),
                R.layout.spinner_item_view, new String[]{"Male", "Female"}
        );

        spinnerGender.setAdapter(adapter);

        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedItem = genderArray[i];
                ((SignUpActivity) getActivity()).insertProperty(RegistrationConstants.Gender, selectedItem);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getContext(), "Please select your gender", Toast.LENGTH_SHORT).show();

            }
        });
        return rootView;
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@") && email.contains(".");
    }
}