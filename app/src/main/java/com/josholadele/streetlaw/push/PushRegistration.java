package com.josholadele.streetlaw.push;

import android.content.Context;

import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.network.UserService;

/**
 * Created by josh on 30/03/2018.
 */

public class PushRegistration {

    Context mContext;

    AppSharedPref appSharedPref;

    public PushRegistration(Context context) {
        mContext = context;
        appSharedPref = new AppSharedPref(mContext);
    }

    public void sendTokenToServer(String token) {
        try {
            UserService userService = new UserService();
            userService.updateToken(appSharedPref.getUserId(), token);
        } catch (Exception ex) {

        }
    }
}
