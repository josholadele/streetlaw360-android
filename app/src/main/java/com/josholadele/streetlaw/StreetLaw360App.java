package com.josholadele.streetlaw;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatDelegate;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.cloudinary.android.MediaManager;
import com.crashlytics.android.Crashlytics;
import com.josholadele.streetlaw.model.DaoMaster;
import com.josholadele.streetlaw.model.DaoSession;
import com.josholadele.streetlaw.modelBridge.MyOpenHelper;
import com.josholadele.streetlaw.utils.Utilities;

import org.greenrobot.greendao.database.Database;

import io.fabric.sdk.android.Fabric;

//import com.google.firebase.database.FirebaseDatabase;
//import com.google.firebase.messaging.FirebaseMessaging;

/**
 * Created by josh on 10/03/2018.
 */

public class StreetLaw360App extends Application {
    public static final String TAG = StreetLaw360App.class.getSimpleName();
    private static StreetLaw360App mInstance;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    Database db;
    DaoMaster.OpenHelper helper;
    private DaoSession daoSession;
    private RequestQueue mRequestQueue;

    public static synchronized StreetLaw360App getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        MediaManager.init(this, CloudinaryConfig.getConfig());

        Fabric.with(this, new Crashlytics());
//        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
//        FirebaseMessaging.getInstance().subscribeToTopic("general");
//        FirebaseMessaging.getInstance().subscribeToTopic("");

        //System.setProperty("https.protocols", "TLSv1.1");
        FontsOverride.setDefaultFont(getApplicationContext(), "DEFAULT", "fonts/American_Typewriter_Regular.ttf");
        FontsOverride.setDefaultFont(getApplicationContext(), "MONOSPACE", "fonts/American_Typewriter_Regular.ttf");
        FontsOverride.setDefaultFont(getApplicationContext(), "SERIF", "fonts/American_Typewriter_Regular.ttf");
        FontsOverride.setDefaultFont(getApplicationContext(), "SANS_SERIF", "fonts/American_Typewriter_Regular.ttf");

        helper = new MyOpenHelper(this, Utilities.getDbName(this));
        db = helper.getWritableDb();
        MultiDex.install(this);
    }

    public DaoSession getDaoSession() {
        if (daoSession == null) {
            if (db == null) {
                db = helper.getWritableDb();
            }
            daoSession = new DaoMaster(db).newSession();
        }
        return daoSession;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}