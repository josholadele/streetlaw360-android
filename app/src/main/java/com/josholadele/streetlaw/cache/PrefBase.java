package com.josholadele.streetlaw.cache;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Oladele on 9/8/17.
 */

class PrefBase {

    SharedPreferences.Editor edit;
    Context mContext;
    SharedPreferences sharedPreferences;

    protected PrefBase(Context context) {
        this.mContext = context;
        sharedPreferences = mContext.getSharedPreferences(mContext.getPackageName() + "." + getClass().getSimpleName(), Context.MODE_PRIVATE);
    }

    protected void putString(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();//.commit();
    }

    protected void putBoolean(String key, boolean value) {
        sharedPreferences.edit().putBoolean(key, value).commit();
    }

    protected void putFloat(String key, float value) {
        sharedPreferences.edit().putFloat(key, value).commit();
    }

    protected void putLong(String key, long value) {
        sharedPreferences.edit().putLong(key, value).commit();
    }

    protected void putInt(String key, int value) {
        sharedPreferences.edit().putInt(key, value).commit();
    }

    protected String getString(String key, String defaultValue) {
        return sharedPreferences.getString(key, defaultValue);
    }

    protected int getInt(String key) {
        return sharedPreferences.getInt(key, 0);
    }

    protected boolean getBoolean(String key, boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }

    protected float getFloat(String key, float defaultValue) {
        return sharedPreferences.getFloat(key, defaultValue);
    }

    protected long getLong(String key, long defaultValue) {
        return sharedPreferences.getLong(key, defaultValue);
    }

}
