package com.josholadele.streetlaw.cache;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.josholadele.streetlaw.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by CrowdStar on 2/12/2015.
 */
public final class AppSharedPref extends PrefBase {

    private final String DATE_KEY = "date";
    private final String EMAIL_KEY = "email";
    private final String PIN_KEY = "pin";
    private final String PICTURE_CACHE = "pictureCache";
    private final String HAS_LOGGED_IN = "hasLoggedIn";
    private final String USER_OBJECT = "userObject";
    private final String USER_ID = "userId";
    private final String JUDICIAL_DIVISION_ID = "divisionId";
    private final String USER_CATEGORY = "userCategory";
    private final String IS_FIRST_TIME = "isFirstTime";
    private final String PROFILE_PIC = "profile_pic";
    private final String ADDRESS_LINE_ONE = "addressLine1";
    private final String ADDRESS_LINE_TWO = "addressLine2";
    private final String FCM_TOKEN = "FCMToken";
    private final String PROFILE_DISPLAY_DESCRIPTION = "PDDescription";
    public Context mContext;


    public AppSharedPref(Context context) {
        super(context);
        this.mContext = context;
    }

    public void cacheUserObject(JSONObject userObj) {
        saveUserDetails(userObj);

        String stringToSave = userObj.toString();
        putString(USER_OBJECT, stringToSave);
    }

    private void saveUserDetails(JSONObject userObj) {
        putString(USER_ID, userObj.optString("Id"));
        putString(PICTURE_CACHE, userObj.optString("ProfilePicture"));
        putInt(JUDICIAL_DIVISION_ID, userObj.optInt("JudicialDivisionId"));
        putString(USER_CATEGORY, userObj.optString("Category"));
        putString(EMAIL_KEY, userObj.optString("Email"));
        putString(PROFILE_PIC, userObj.optString("ProfilePicture"));
        putString(ADDRESS_LINE_ONE, userObj.optString("AddressLine1"));
        putString(ADDRESS_LINE_TWO, userObj.optString("AddressLine2"));
        putString(PROFILE_DISPLAY_DESCRIPTION, userObj.optString("ProfileDisplayDescription"));
        String url = userObj.optString("ProfilePicture");
    }

    public String downloadImage(Context context, String url) {
        /*URL wallpaperURL = new URL(url);
        URLConnection connection = wallpaperURL.openConnection();
        InputStream inputStream = new BufferedInputStream(wallpaperURL.openStream(), 10240);
        //File cacheDir = getActivityContext().getCacheFolder(context);
        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir
        File cacheFile = cw.getDir("imageDir", Context.MODE_PRIVATE);
        //File cacheFile = new File(cacheDir, "localFileName.jpg");
        FileOutputStream outputStream = new FileOutputStream(cacheFile);

        byte buffer[] = new byte[1024];
        int dataSize;
        int loadedSize = 0;
        while ((dataSize = inputStream.read(buffer)) != -1) {
            loadedSize += dataSize;
            //publishProgress(loadedSize);
            outputStream.write(buffer, 0, dataSize);
        }

        outputStream.close(); */
        return " ";
    }

    public void saveToInternalStorage(Context context, JSONObject userObj){
        String url = userObj.optString("ProfilePicture");
        ContextWrapper cw = new ContextWrapper(context);
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        //File mypath=new File(directory,"profile.jpg");
        new DownloadFileFromURL(context).execute(url);
        String dirt = directory.getAbsolutePath();
        putString(PICTURE_CACHE, dirt);

        /*File f = new File(url);
        Bitmap bitmap;
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);

        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,"profile.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } */

    }

    public String getUserId() {
        return getString(USER_ID, "");
    }


    public int getJudicialDivision() {
        return getInt(JUDICIAL_DIVISION_ID);
    }

    public int getUserIdInt() {
        return Integer.parseInt(getString(USER_ID, "0"));
    }

    public String getUserCategory() {
        return getString(USER_CATEGORY, "");
    }

    public String getAddressLine1() {
        return  getString(ADDRESS_LINE_ONE, "n/a");
    }

    public String getAddressLine2() {
        return  getString(ADDRESS_LINE_TWO, "n/a");
    }

    public String getFCMToken() {
        return getString(FCM_TOKEN, "");
    }

    public void setFCMToken(String token) {
        putString(FCM_TOKEN, token);
    }

    public String getProfileDisplayDescription() {
        return getString(PROFILE_DISPLAY_DESCRIPTION, "n/a");
    }

    public JSONObject getUserObject() {
        try {
            String objectString = getString(USER_OBJECT, "{}");
            return new JSONObject(objectString);
        } catch (Exception e) {
            e.printStackTrace();
            return new JSONObject();
        }
    }

    public void setDate(long dateTime) {
        putLong(DATE_KEY, dateTime);

    }

    public String getProfilePic() {
        return getString(PROFILE_PIC, "");
    }

    public void setProfilePic(String pic) {
        putString(PROFILE_PIC, pic);
    }

    public String getEmail() {
        return getString(EMAIL_KEY, "");
    }

    public void setEmail(String email) {
        putString(EMAIL_KEY, email);
    }

    public String getPin() {
        return getString(PIN_KEY, "-1");
    }

    public void setPin(String pin) {
        putString(PIN_KEY, pin);
    }

    public boolean isFirstTime() {
        return getBoolean(IS_FIRST_TIME, true);
    }

    public void setFirstTime(boolean status) {
        putBoolean(IS_FIRST_TIME, status);
    }


    public void setHasLoggedIn(boolean status) {
        putBoolean(HAS_LOGGED_IN, status);
    }

    public boolean hasLoggedIn() {
        return getBoolean(HAS_LOGGED_IN, false);
    }

    public String getPictureCache() {

        return getString(PICTURE_CACHE, "");
    }

    public void setPictureCache(String fileName) {
        putString(PICTURE_CACHE, fileName);
    }


}


class DownloadFileFromURL extends AsyncTask<String, String, String> {

    Context mContext;

    public DownloadFileFromURL(Context context) {
        mContext = context;
    }
    /**
     * Before starting background thread
     * */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        System.out.println("Starting download");
    }

    /**
     * Downloading file in background thread
     * */
    @Override
    protected String doInBackground(String... f_url) {
        int count;
        try {
            String root = Environment.getExternalStorageDirectory().toString();
            ContextWrapper cw = new ContextWrapper(mContext);
            // path to /data/data/yourapp/app_data/imageDir
            File cacheFile = cw.getDir("imageDir", Context.MODE_PRIVATE);

            System.out.println("Downloading");
            URL url = new URL(f_url[0]);
            Log.e("ImageURL", f_url[0]);
;
            URLConnection conection = url.openConnection();
            conection.connect();
            // getting file length
            int lenghtOfFile = conection.getContentLength();

            // input stream to read file - with 8k buffer
            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            // Output stream to write file

            OutputStream output = new FileOutputStream(root+"/profile.jpg");
            byte data[] = new byte[1024];

            long total = 0;
            while ((count = input.read(data)) != -1) {
                total += count;

                // writing data to file
                output.write(data, 0, count);

            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }

        return null;
    }



    /**
     * After completing background task
     * **/
    @Override
    protected void onPostExecute(String file_url) {
        System.out.println("Downloaded");
    }
}