package com.josholadele.streetlaw;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.Toast;

import com.josholadele.streetlaw.activities.BaseActivity;
import com.josholadele.streetlaw.activities.SignUpActivity;
import com.josholadele.streetlaw.activities.UpdatedSettingsActivity;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.network.UserService;
import com.josholadele.streetlaw.utils.AlternateDialogUtils;

import org.json.JSONObject;

public class Verify extends AppCompatActivity {
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        // Get a reference to the app's shared preferences
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        // Get the display name from the preference file
        boolean isDisplayName = sharedPreferences.getBoolean("delete_account", false);

        // Display an interactive pop-up screen
        showPopUp();


        if (!TextUtils.isEmpty(String.valueOf(isDisplayName))) {
            Toast.makeText(this, "This is the default display name " + String.valueOf(isDisplayName), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "The string is empty ", Toast.LENGTH_SHORT).show();
        }
    }

    /*HELPER METHODS
    * */

    /**
     * This function displays a pop-up screen for the user to interact with
     */
    public void showPopUp() {
        AlternateDialogUtils.createPlainDialog(this, null, "Are you sure you want to delete your account?", new AlternateDialogUtils.UserResponseListeners() {
            @Override
            public void onPositive() {

                Toast.makeText(Verify.this, "Deleted", Toast.LENGTH_SHORT).show();

                // Delete the user's account
                deleteUserAccount();

                Intent loginIntent = new Intent(Verify.this, LoginActivity.class);
                // Go back to settings
                startActivity(loginIntent);
            }

            @Override
            public void onNegative() {

                Toast.makeText(Verify.this, "Cancel Delete", Toast.LENGTH_SHORT).show();


                // Go back to settings
                startActivity(new Intent(Verify.this, UpdatedSettingsActivity.class));
            }

            @Override
            public void onNeutral() {
                startActivity(new Intent(Verify.this, UpdatedSettingsActivity.class));
            }
        });
    }

    /**
     * This method gets the user's ID from the app's shared preference file,
     * and using the deleteAccount API, it deletes the user's account by it's ID
     */
    public void deleteUserAccount() {

        // Get a reference to the app's shared preference file
        AppSharedPref sharedPref = new AppSharedPref(Verify.this);

        // Get user ID from shared preference
        int userID = Integer.parseInt(sharedPref.getUserId());
        // Get an instance of the UserService class
        UserService userService = new UserService();

        // Delete the user account
        userService.deleteAccount(userID, new NetworkResponseCallback() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(Verify.this, "Success: Response is - " + response, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailed(String reason) {
                Toast.makeText(Verify.this, "Failed: Reason is - " + reason, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String reason) {

                Toast.makeText(Verify.this, "Error: Reason is - " + reason, Toast.LENGTH_SHORT).show();
            }
        });

    }
}
