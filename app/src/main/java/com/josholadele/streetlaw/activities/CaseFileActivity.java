package com.josholadele.streetlaw.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.adapters.CaseFilesAdapter;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.enums.CaseStatus;
import com.josholadele.streetlaw.enums.Role;
import com.josholadele.streetlaw.model.CaseFile;
import com.josholadele.streetlaw.network.CaseService;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.utils.StringUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class CaseFileActivity extends BaseActivity implements CaseFilesAdapter.CaseClickListener, SearchView.OnQueryTextListener {

    public static final int ADD_CASE_REQUEST_CODE = 1001;
    RecyclerView caseFilesRecycler;
    CaseFilesAdapter mAdapter;
    Toolbar mToolbar;
    LinearLayout addCaseLayout;
    TextView toolbarTitle;
    TextView toolbarSelectionText;
    TextView addCaseTV;
    ImageView addCaseImg;
    MenuItem assignMenu;
    MenuItem filterMenu;
    ProgressBar contentLoading;
    TextView emptyView;
    LinearLayout mainContentLayout;
    AppSharedPref _sharedPref;
    String userCategory;
    ImageView profileImage;
    List<CaseFile> files;
    private List<CaseFile> mModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wrapper_case_files);

        mDrawerLayout = findViewById(R.id.drawer_layout);

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);

        _sharedPref = new AppSharedPref(this);

        contentLoading = findViewById(R.id.progress_layout);
        mainContentLayout = findViewById(R.id.main_content_layout);
        emptyView = findViewById(R.id.empty_view);

        toolbarTitle = mToolbar.findViewById(R.id.client_logo);
        toolbarSelectionText = mToolbar.findViewById(R.id.selection_number);

        addCaseTV = findViewById(R.id.txt_open_new_case);
        addCaseImg = findViewById(R.id.img_add_new_case);
        addCaseLayout = findViewById(R.id.add_case_layout);
        profileImage = findViewById(R.id.img_casefile_profile_image);

        userCategory = _sharedPref.getUserCategory();

        doCategoryCheck();

        addCaseTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchAddNewCase();
            }
        });
        addCaseImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchAddNewCase();
            }
        });


        toggle.syncState();

        caseFilesRecycler = findViewById(R.id.cases_recyclerview);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        setupNavView(navigationView);
        navigationView.setNavigationItemSelectedListener(this);
        setupRecyclerView(caseFilesRecycler);

        fetchCases(_sharedPref.getUserId(), "All");
        loadImageFromStorage(_sharedPref.getPictureCache());

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        // Here is where we are going to implement the filter logic
        final List<CaseFile> filteredModelList = filter(files, query);
        if(filteredModelList.size() > 0) {
            mAdapter.setSearchResult(filteredModelList);
            caseFilesRecycler.scrollToPosition(0);
            showDataView();
        } else {
            mAdapter.setSearchResult(filteredModelList);
            showErrorMessage();
        }
        return true;
    }

    private static List<CaseFile> filter(List<CaseFile> models, String query) {
        final String lowerCaseQuery = query.toLowerCase();

        final List<CaseFile> filteredModelList = new ArrayList<>();
        if(null != models) {
            for (CaseFile model : models) {
                final String text = model.Title.toLowerCase();
                final String email = model.suitNumber.toLowerCase();
                final String desc = model.description.toLowerCase();
                if (text.contains(lowerCaseQuery) || email.contains(lowerCaseQuery) || desc.contains(lowerCaseQuery)) {
                    filteredModelList.add(model);
                }
            }
        }
        return filteredModelList;
    }

    private void doCategoryCheck() {
        if (userCategory.toLowerCase().equalsIgnoreCase(Role.CourtRegistrar.roleName.toLowerCase())) {
            addCaseLayout.setVisibility(View.VISIBLE);
        } else {
            addCaseLayout.setVisibility(View.GONE);
        }
    }

    private void loadImageFromStorage(String path)
    {
        try {
            File f=new File(path, "profile.jpg");
            Bitmap bt = BitmapFactory.decodeStream(new FileInputStream(f));
            Bitmap b = BitmapFactory.decodeFile(f.getAbsolutePath());
            //ImageView img=(ImageView)findViewById(R.id.imgPicker);
            String st = f.exists()? "Exists": "FIle is fake";
            if(null != b) {
                profileImage.setImageBitmap(b);
            }
            //Log.e("FileAbsPath", st + "|" + f.getAbsolutePath());

        }
        catch (FileNotFoundException e)
        {
            Log.e("ErrorLoading", e.getMessage());
            e.printStackTrace();
        }
    }

    private void launchAddNewCase() {
        Intent intent = new Intent(this, NewCaseActivity.class);
        startActivityForResult(intent, ADD_CASE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_CASE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                CaseFile caseFile = data.getParcelableExtra("case");
                mAdapter.addCaseFile(caseFile);
                Toast.makeText(this, caseFile.Title + " Added!", Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == 12 && resultCode == Activity.RESULT_OK) {
            fetchCases(_sharedPref.getUserId(), "All");
            updateToolbar(false);
//            if (data != null) {
//                CaseFile caseFile = data.getParcelableExtra("case");
//                mAdapter.addUser(caseFile);
//                Toast.makeText(this, caseFile.title + " Added!", Toast.LENGTH_SHORT).show();
//            }
        }
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        mAdapter = new CaseFilesAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
//        getSampleCaseFiles();
    }

    @Override
    public void onBackPressed() {

        if (mAdapter.IS_MULTISELECT) {
            mAdapter.IS_MULTISELECT = false;
            for (int i : mAdapter.selectedPositions) {
                caseFilesRecycler.getChildAt(i).setActivated(false);
            }
            mAdapter.clearSelections();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_case_file, menu);
        assignMenu = menu.findItem(R.id.action_assign);
        filterMenu = menu.findItem(R.id.action_filter);
        //final MenuItem item = menu.findItem(R.id.action_search);

        /*SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName())); */
        /*getMenuInflater().inflate(R.menu.menu_main, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);*/

        //final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        //searchView.setOnQueryTextListener(this);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        item.setOnActionExpandListener( new MenuItem.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {

                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                mAdapter.setSearchResult(files);
                return true; // Return true to collapse action view

            }
        });

        return true;
//        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_assign) {
            Intent intent = new Intent(this, ChooseJudgeActivity.class);

            setIntentData(intent);

            startActivityForResult(intent, 12);
            return true;
        } else if (item.getItemId() == R.id.action_filter) {
            PopupMenu popupMenu = new PopupMenu(this, findViewById(R.id.action_filter));

            popupMenu.getMenuInflater().inflate(R.menu.case_filter, popupMenu.getMenu());

            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                fetchCases(_sharedPref.getUserId(), item.getTitle().toString());
                    //Toast.makeText(CaseFileActivity.this, "Selected: " + item.getTitle(), Toast.LENGTH_SHORT).show();
                    return true;
                }
            });

            popupMenu.show();

//            Toast.makeText(this, "Filter", Toast.LENGTH_SHORT).show();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void setIntentData(Intent intent) {
        List<CaseFile> selectedCases = new ArrayList<>();
        ArrayList<Integer> selectedCaseIds = new ArrayList<>();
        for (int i : mAdapter.selectedPositions) {
            CaseFile data = mAdapter.getData().get(i);
            selectedCases.add(data);
            selectedCaseIds.add(data.Id);
        }
        if (selectedCaseIds.size() > 0) {
            intent.putExtra("selectedIds", selectedCaseIds);
        }
        intent.putExtra("isReassign", false);
    }

    @Override
    public void onCaseFileClick(CaseFile caseFile, boolean isLongClick) {

        Intent intent = new Intent(this, CaseDetailsActivity.class);
        intent.putExtra("case", caseFile);
        intent.putExtra(CaseDetailsActivity.SELECTED_CASE_ID, caseFile.Id);

        startActivity(intent);

    }

    public void updateToolbar(boolean is_multiselect) {
        if (is_multiselect) {
            toolbarSelectionText.setVisibility(View.VISIBLE);
            String selectedNumber = mAdapter.selectedPositions.size() == 1 ? mAdapter.selectedPositions.size() + " case selected" : mAdapter.selectedPositions.size() + " cases selected";
            toolbarTitle.setVisibility(View.GONE);
            toolbarSelectionText.setText(selectedNumber);
            assignMenu.setVisible(true);
            filterMenu.setVisible(false);
        } else {
            toolbarSelectionText.setText("");
            toolbarSelectionText.setVisibility(View.GONE);
            toolbarTitle.setVisibility(View.VISIBLE);
            assignMenu.setVisible(false);
            filterMenu.setVisible(true);
        }
    }


    void fetchCases(String userId, String status) {

        showLoading();
        CaseService caseService = new CaseService();
        caseService.getByUser(Integer.parseInt(userId), status, new NetworkResponseCallback() {
            @Override
            public void onResponse(JSONObject response) {
                List<CaseFile> caseFileList = buildFromResult(response.optJSONArray("Data"));
                if (caseFileList != null && caseFileList.size() > 0) {
                    showDataView();
                    files = caseFileList;
                    mAdapter.setCaseFileData(caseFileList);
                } else {
                    showErrorMessage();
                }
            }

            @Override
            public void onFailed(String reason) {
                showErrorMessage();
            }

            @Override
            public void onError(String reason) {
                showErrorMessage();
            }
        });
    }

    private void showErrorMessage() {
        emptyView.setVisibility(View.VISIBLE);
        caseFilesRecycler.setVisibility(View.INVISIBLE);
        contentLoading.setVisibility(View.GONE);
        doCategoryCheck();

    }

    private void showDataView() {
        emptyView.setVisibility(View.GONE);
        caseFilesRecycler.setVisibility(View.VISIBLE);
        contentLoading.setVisibility(View.GONE);
        doCategoryCheck();
    }

    private void showLoading() {
        emptyView.setVisibility(View.GONE);
        caseFilesRecycler.setVisibility(View.INVISIBLE);
        contentLoading.setVisibility(View.VISIBLE);
        addCaseLayout.setVisibility(View.GONE);
    }


    private List<CaseFile> buildFromResult(JSONArray result) {

        if (result == null || result.equals("")) {
            return null;
        }

        JSONObject movieJSONObject;
        JSONArray movieJSONArray = null;
        try {
//                movieJSONObject = new JSONObject(result);
//                if (isItem) {
//                    movieJSONArray = movieJSONObject.optJSONArray("items");
//                } else {
//                    movieJSONArray = movieJSONObject.optJSONArray("results");
//                }
            movieJSONArray = result;
        } catch (Exception e) {
            e.printStackTrace();
        }


        List<CaseFile> caseFiles = new ArrayList<>();


        for (int i = 0; i < movieJSONArray.length(); i++) {
            JSONObject object = movieJSONArray.optJSONObject(i);

            // Get values from JSONObject

            String caseStatusString = StringUtils.capitaliseFirstLetter(object.optString("Status"));

            int caseID = object.optInt("Id");
            CaseStatus caseStatus = CaseStatus.valueOf(caseStatusString);
            Log.e("CaseFileActivity",caseStatusString);
            String caseTitle = object.optString("Title");
            String caseSuitNumber = object.optString("SuitNumber");
            String caseDescription = "";

            // Create a new caseFile object with the extracted values
            CaseFile caseFile = new CaseFile(caseID,
                    caseStatus,
                    caseTitle,
                    caseSuitNumber,
                    caseDescription);

            // Add CaseFile object to the caseFiles ArrayList
            caseFiles.add(caseFile);

        }
        return caseFiles;
    }

}
