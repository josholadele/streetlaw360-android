package com.josholadele.streetlaw.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.model.News;
import com.josholadele.streetlaw.model.Note;
import com.josholadele.streetlaw.network.DiaryService;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.utils.DialogUtils;
import com.josholadele.streetlaw.utils.TimeUtils;

import org.json.JSONObject;

public class NoteDetailsActivity extends AppCompatActivity {


    private DiaryService diaryService;
    private int _selectedNoteId;
    private Note note;
    private JSONObject noteObject;

    TextView noteTitle;
    TextView noteTime;
    TextView noteContent;

    public static final String SELECTED_CASE_ID = "selected-case";
    public static final int EDIT_NOTE_ID = 7643;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        News news = getIntent().getParcelableExtra("news");

        setContentView(R.layout.activity_note_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView titleText = toolbar.findViewById(R.id.toolbar_title);

        note = getIntent().getParcelableExtra("note");
        _selectedNoteId = getIntent().getIntExtra("note-Id", -1);

        titleText.setText("Note");
        setSupportActionBar(toolbar);

        noteTitle = findViewById(R.id.note_title);
        noteTime = findViewById(R.id.note_time);
        noteContent = findViewById(R.id.note_content);
        diaryService = new DiaryService();

        if (note != null) {
            noteTitle.setText(note.topic);
            noteTime.setText(TimeUtils.getFormattedTime(note.dateLastUpdated));
            noteContent.setText(note.content);
        } else {
            if (_selectedNoteId != -1) {
                getNoteOnActivity();
            }
        }


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

//        Typeface avenir = Typeface.createFromAsset(getAssets(), "fonts/AvenirLTStd_Medium.otf");
        Typeface american_typewriter = Typeface.createFromAsset(getAssets(), "fonts/American_Typewriter_Regular.ttf");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (id == R.id.action_edit) {
            if (note != null) {
                Intent intent = new Intent(this, EditNoteActivity.class);
                intent.putExtra("note", note);
                startActivityForResult(intent, EDIT_NOTE_ID);
            }
            return true;
        }
        if (id == R.id.action_delete) {
            DialogUtils.createDialog(this, "Delete Note", "Delete note: " + note.topic + "?", null, new DialogUtils.UserResponseListeners() {
                @Override
                public void onPositive() {
                    deleteNote();
                }

                @Override
                public void onNegative() {
                }

                @Override
                public void onNeutral() {

                }
            });
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteNote() {
        try {
            diaryService.deleteNote(_selectedNoteId, new NetworkResponseCallback() {
                @Override
                public void onResponse(JSONObject response) {
                    finish();
                }

                @Override
                public void onFailed(String reason) {
                    Toast.makeText(NoteDetailsActivity.this, "Unable to retrieve case: " + reason, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onError(String reason) {
                    Toast.makeText(NoteDetailsActivity.this, "Unable to retrieve case: " + reason, Toast.LENGTH_SHORT).show();

                }
            });

        } catch (Exception ex) {
            Toast.makeText(this, "Unable to retrieve case: " + ex.getMessage(), Toast.LENGTH_SHORT).show();

        }
    }

    private void bindData() {
        if (noteObject != null) {
//            noteTitle.setText(note.topic);
//            noteTime.setText(TimeUtils.getFormattedTime(note.dateLastUpdated));
//            noteContent.setText(note.content);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EDIT_NOTE_ID && resultCode == Activity.RESULT_OK) {
            Note newNote = getIntent().getParcelableExtra("note");
            if (note != null) {
                noteTitle.setText(newNote.topic);
                noteTime.setText(TimeUtils.getFormattedTime(newNote.dateLastUpdated));
                noteContent.setText(newNote.content);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_note, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void getNoteOnActivity() {
        try {
            diaryService.getById(_selectedNoteId, new NetworkResponseCallback() {
                @Override
                public void onResponse(JSONObject response) {
                    noteObject = response.optJSONObject("Data");
                    bindData();
                }

                @Override
                public void onFailed(String reason) {
                    Toast.makeText(NoteDetailsActivity.this, "Unable to retrieve case: " + reason, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onError(String reason) {
                    Toast.makeText(NoteDetailsActivity.this, "Unable to retrieve case: " + reason, Toast.LENGTH_SHORT).show();

                }
            });

        } catch (Exception ex) {
            Toast.makeText(this, "Unable to retrieve case: " + ex.getMessage(), Toast.LENGTH_SHORT).show();

        }
    }
}