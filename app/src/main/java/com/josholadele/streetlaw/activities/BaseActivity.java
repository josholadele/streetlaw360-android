package com.josholadele.streetlaw.activities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.josholadele.streetlaw.MainActivity;
import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.utils.UserUtils;

/**
 * Created by josh on 16/03/2018.
 */

public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    //    public DrawerLayout mDrawerLayout;
//    Toolbar mToolbar;
//    static {
//        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
//    }

    public DrawerLayout mDrawerLayout;
    static int current_selection = R.id.nav_home;

    AppSharedPref sharedPref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = new AppSharedPref(this);

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId  = getString(R.string.app_name);
            String channelName = getString(R.string.app_name);
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        } */

        /* FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("FCMToken", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        sharedPref.setFCMToken(token);
                        Log.e("FCMToken", token);
                    }
                }); */

    }


    protected void setupNavView(NavigationView navigationView) {
        ImageView closeImage = navigationView.getHeaderView(0).findViewById(R.id.close_image);
        if (UserUtils.isJudgeOrRegistrar(this)) {
            MenuItem item = navigationView.getMenu().findItem(R.id.nav_legal_conversations);
            if (item != null)
                item.setVisible(false);
        }
         if (UserUtils.isRegistrar(this)) {
            MenuItem item = navigationView.getMenu().findItem(R.id.nav_law_report);
            if (item != null)
                item.setVisible(false);
        }

        if (UserUtils.isStudent(this)) {
            MenuItem item = navigationView.getMenu().findItem(R.id.nav_case_files);
            if (item != null)
                item.setVisible(false);
        }
        MenuItem converseItem = navigationView.getMenu().findItem(R.id.nav_legal_conversations);
        MenuItem counsellingItem = navigationView.getMenu().findItem(R.id.nav_online_counselling);
        String ucategory = sharedPref.getUserCategory();
        String uemail = sharedPref.getEmail();
        if( ucategory.equalsIgnoreCase("Lawyer")
                || ucategory.equalsIgnoreCase("JudicialStaff")
                || ucategory.equalsIgnoreCase("ChiefJudge")
                //|| ucategory.equalsIgnoreCase("Judge")
                || ucategory.equalsIgnoreCase("LawStudent")) {
            Log.e("User Category", sharedPref.getUserCategory());
            if(null != converseItem)
                converseItem.setVisible(true);
        } else {
            if(null != converseItem)
                converseItem.setVisible(false);
        }

        if( ucategory.equalsIgnoreCase("Litigant")
                ||  uemail.equalsIgnoreCase("streetlaw360@gmail.com") ) {
            Log.e("User Category", sharedPref.getUserCategory());
            if(null != counsellingItem)
                counsellingItem.setVisible(true);
        } else {
            if(null != counsellingItem)
                counsellingItem.setVisible(false);
        }
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDrawerLayout != null) {
                    mDrawerLayout.closeDrawers();
                }
            }
        });
    }

    boolean pressedOnce = false;

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (pressedOnce) {
                super.onBackPressed();
            } else {
                pressedOnce = true;
                Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pressedOnce = false;
                    }
                }, 2000);
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        FragmentManager supportFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        if (id == R.id.nav_home) {
            // Handle the camera action
            if (current_selection != R.id.nav_home) {
                current_selection = R.id.nav_home;
                startActivity(new Intent(this, MainActivity.class));
                finish();
//            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_profile) {
            // Handle the camera action
            if (current_selection != R.id.nav_profile) {
                current_selection = R.id.nav_profile;
                startActivity(new Intent(this, ProfilePage.class));
                finish();
//            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_case_files) {
//            fragmentTransaction.replace(R.id.viewpager, new Fraggy()).commit();
//            Toast.makeText(this, "Case Files", Toast.LENGTH_SHORT).show();
            if (current_selection != R.id.nav_case_files) {
                current_selection = R.id.nav_case_files;
                startActivity(new Intent(this, CaseFileActivity.class));
                finish();
//            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
            }

        } else if (id == R.id.nav_court_structure) {

            Toast.makeText(this, "Court Structure", Toast.LENGTH_SHORT).show();
            if (current_selection != R.id.nav_case_files) {
                current_selection = R.id.nav_case_files;
                startActivity(new Intent(this, ProfilePage.class));
                finish();
//            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
            }

        } else if (id == R.id.nav_legal_conversations) {

            if (current_selection != R.id.nav_legal_conversations) {
                current_selection = R.id.nav_legal_conversations;
                //startActivity(new Intent(this, ConversationActivity.class));
                startActivity(new Intent(this, PeersActivity.class ));
                //finish();
//            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
            }

        } else if (id == R.id.nav_online_counselling) {

            if (current_selection != R.id.nav_online_counselling) {
                current_selection = R.id.nav_online_counselling;
                //startActivity(new Intent(this, ConversationActivity.class));
                startActivity(new Intent(this, CounsellingActivity.class ));
                //finish();
//            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
            }

        } else if (id == R.id.nav_contacts) {

            Toast.makeText(this, "Case Files", Toast.LENGTH_SHORT).show();
            if (current_selection != R.id.nav_case_files) {
                current_selection = R.id.nav_case_files;
                startActivity(new Intent(this, ProfilePage.class));
                finish();
//            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
            }

        } else if (id == R.id.nav_court_locations) {
            if (current_selection != R.id.nav_court_locations) {
                current_selection = R.id.nav_court_locations;
                startActivity(new Intent(this, CourtLocationsActivity.class));
                finish();
//            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
            }

        }
// else if (id == R.id.nav_settings) {
//             if(current_selection != R.id.nav_settings) {
//                 current_selection = R.id.nav_settings;
//                 startActivity(new Intent(this, SettingsActivity.class));
//                 Toast.makeText(this, "Settings", Toast.LENGTH_SHORT).show();
//             }
//
//        }
        else if (id == R.id.nav_new_settings){
            if (current_selection != R.id.nav_new_settings){
                current_selection = R.id.nav_new_settings;

                startActivity(new Intent(this, UpdatedSettingsActivity.class ));

                Toast.makeText(this, "New settings", Toast.LENGTH_SHORT).show();
            }
        }else if (id == R.id.nav_law_report) {

            Toast.makeText(this, "Law Reports", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_log_out) {
            startActivity(new Intent(this, LandingPage.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();

//            if (current_selection != R.id.nav_case_files) {
//                current_selection = R.id.nav_case_files;
//                startActivity(new Intent(this, ProfilePage.class));
//                finish();
////            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show();
//            }

        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
