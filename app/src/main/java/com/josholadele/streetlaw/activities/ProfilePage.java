package com.josholadele.streetlaw.activities;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.josholadele.streetlaw.ImageUploadHelper;
import com.josholadele.streetlaw.MainActivity;
import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.model.ProfilePictureModel;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.network.UserService;
import com.josholadele.streetlaw.utils.UserUtils;
import com.theartofdev.edmodo.cropper.CropImage;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfilePage extends BaseActivity {

    public static final int EDIT_PROFILE_REQUEST_CODE = 1201;
    Toolbar mToolbar;
    TextView profileNameTV;
    TextView professionTV;
    TextView shortBioTV;
    TextView addressTV;
    ImageUploadHelper imageUploadHelper;
    AppSharedPref sharedPref;

    ImageView editImage;
    ImageView noticesImage;
    ImageView settingsImage;

    ImageView changeProfilePic;
    UserService _userService;
    CircleImageView profilePicture;
    ImageView samplePicture;
    ImageView testImg;
    JSONObject userObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wrapper_profile);
        mDrawerLayout = findViewById(R.id.drawer_layout);

        imageUploadHelper = new ImageUploadHelper(this);
        sharedPref = new AppSharedPref(this);

        //new DownloadImage().execute("https://speedysms.com.ng/web/images/logo.png");
        //new DownloadFileFromURL(this).execute("https://speedysms.com.ng/web/images/logo.png");
        testImg = findViewById(R.id.test_chat_img);
        new ImageDownload(this, testImg).execute("https://speedysms.com.ng/web/images/logo.png");
        professionTV = findViewById(R.id.profile_profession);
        profileNameTV = findViewById(R.id.profile_name);
        Typeface american_typewriter = Typeface.createFromAsset(getAssets(), "fonts/American_Typewriter_Regular.ttf");
        Typeface avenil = Typeface.createFromAsset(getAssets(), "fonts/AvenirLTStd_Medium.otf");
        profileNameTV.setTypeface(avenil, Typeface.BOLD);
        professionTV.setTypeface(american_typewriter, Typeface.NORMAL);

        addressTV = findViewById(R.id.address);
        shortBioTV = findViewById(R.id.short_bio);
        shortBioTV.setTypeface(avenil, Typeface.NORMAL);
        addressTV.setTypeface(avenil, Typeface.NORMAL);
        profilePicture = findViewById(R.id.profile_pic);
        samplePicture = findViewById(R.id.sample_pic);

        _userService = new UserService();


        bindData();
        String profilePicString = sharedPref.getPictureCache();
        Log.e("ProfilePic", profilePicString);

        if (!TextUtils.isEmpty(profilePicString) && !profilePicString.equalsIgnoreCase("null")) {
            loadImageFromStorage(profilePicString);
            //Glide.with(this).load(profilePicString).into(profilePicture).onLoadFailed(null, getResources().getDrawable(R.drawable.avatar_contact));
            //Glide.with(this).load(profilePicString).into(samplePicture).onLoadFailed(null, getResources().getDrawable(R.drawable.avatar_contact));
            //samplePicture.setImageURI(Uri.fromFile(new File(profilePicString)));
//            Glide.with(this).load("https://cdn.pixabay.com/photo/2016/03/21/23/25/link-1271843_1280.png").error(R.drawable.androidicons_42).into(samplePicture);
        }
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);

        TextView titleText = mToolbar.findViewById(R.id.client_logo);
        titleText.setText("Profile");

        toggle.syncState();

        editImage = findViewById(R.id.edit_profile);
        noticesImage = findViewById(R.id.notices);
        settingsImage = findViewById(R.id.settings);
        changeProfilePic = findViewById(R.id.change_picture);
        editImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(ProfilePage.this, EditProfileActivity.class), EDIT_PROFILE_REQUEST_CODE);
            }
        });
        noticesImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                current_selection = R.id.nav_home;
                startActivity(new Intent(ProfilePage.this, MainActivity.class));
                finish();
            }
        });
        settingsImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ProfilePage.this, "Settings", Toast.LENGTH_SHORT).show();
            }
        });
        changeProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageUploadHelper.selectImage();
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        setupNavView(navigationView);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindData();
    }

    void bindData() {
        userObject = sharedPref.getUserObject();
        String name = userObject.optString("FirstName") + " " + userObject.optString("LastName");
        String cname = WordUtils.capitalize(name);
        profileNameTV.setText(cname);
        String cat = userObject.optString("Category");
        String category = sharedPref.getProfileDisplayDescription();
        //UserUtils.getUserDesc(cat);
        professionTV.setText(category);
        shortBioTV.setText(userObject.optString("ShortBio", "n/a"));
        String address = userObject.optString("AddressLine1", "n/a"); // + "\n" + userObject.optString("AddressLine2", "n/a");
        addressTV.setText(address);
    }

    protected void uploadPicture(Bitmap bitmap, String path) {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);

        final ProfilePictureModel profilePixRequest = new ProfilePictureModel();
        profilePixRequest.userId = sharedPref.getUserIdInt();
        profilePixRequest.profileImageByteArray(byteArrayOutputStream.toByteArray());
        String guid = String.valueOf(Calendar.getInstance().getTimeInMillis());
        profilePixRequest.fileName = guid;// + ".jpg";

        _userService.uploadProfilePicture(path, profilePixRequest, new NetworkResponseCallback() {
            @Override
            public void onResponse(JSONObject response) {
                String imageUrl = response.optString("url");

                Toast.makeText(ProfilePage.this, "Profile picture uploaded successfully", Toast.LENGTH_SHORT).show();
                Bitmap imageBitmap = imageUploadHelper.getBitmapFromUri(Uri.parse(imageUrl));
                //String profilePic = saveToInternalStorage(imageBitmap);
                //sharedPref.setPictureCache(profilePic);
                //loadImageFromStorage(imageUrl);
            }

            @Override
            public void onFailed(String reason) {
//                profilePicture.setImageDrawable(ProfilePage.this.getResources().getDrawable(R.drawable.avatar_contact));
                samplePicture.setImageDrawable(ProfilePage.this.getResources().getDrawable(R.drawable.avatar_contact));

                Toast.makeText(ProfilePage.this, reason, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String reason) {
//                profilePicture.setImageDrawable(ProfilePage.this.getResources().getDrawable(R.drawable.avatar_contact));
                samplePicture.setImageDrawable(ProfilePage.this.getResources().getDrawable(R.drawable.avatar_contact));
                Toast.makeText(ProfilePage.this, reason, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadImageFromStorage(String path)
    {

        try {
            File f=new File(path, "profile.jpg");
            Bitmap bt = BitmapFactory.decodeStream(new FileInputStream(f));
            Bitmap b = BitmapFactory.decodeFile(f.getAbsolutePath());
            //ImageView img=(ImageView)findViewById(R.id.imgPicker);
            String st = f.exists()? "Exists": "FIle is fake";
            if(null == b) {
               st  = st + "Bitmap is NULL";
            } else {
               st = st + b.toString();
            }
            Log.e("FileAbsPath", st + "|" + f.getAbsolutePath());
            profilePicture.setImageBitmap(b);
            samplePicture.setImageBitmap(b);
        }
        catch (FileNotFoundException e)
        {
            Log.e("ErrorLoading", e.getMessage());
            e.printStackTrace();
        }

    }

    private String saveToInternalStorage(Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,"profile.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    void uploadImage(Uri imageUri) {
        String sourceFilename = imageUri.getPath();
//        String destinationFilename = android.os.Environment.getExternalStorageDirectory().getPath()+ File.separatorChar+"abc.mp3";

        Bitmap bitmap = imageUploadHelper.getBitmapFromUri(imageUri);
        String profilePic = saveToInternalStorage(bitmap);
        sharedPref.setPictureCache(profilePic);
        emptyImageView(samplePicture);
        loadImageFromStorage(profilePic);
//        profilePicture.setImageBitmap(bitmap);

        //samplePicture.setImageURI(imageUri);
        uploadPicture(bitmap, sourceFilename);
    }

    private void emptyImageView(ImageView ourView) {
        ourView.invalidate();
        ourView.setImageResource(0);
//        ourView.invalidate();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("ResponseFromUpload", requestCode + "|" + resultCode + "|");
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                Uri resultUri = result.getUri();
                uploadImage(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        } else {
            if (requestCode == EDIT_PROFILE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                bindData();
            } else if (resultCode == Activity.RESULT_OK) {
                switch (requestCode) {
                    case ImageUploadHelper.CAMERA_CAPTURE:
                        Uri cameraUri = ImageUploadHelper.getImageURIFromCameraUpload();
                        CropImage.activity(cameraUri).setAspectRatio(1, 1)
                                .start(this);
                        break;

                    case ImageUploadHelper.IMAGE_GALLERY:

                        Uri galleryUri = data.getData();
                        CropImage.activity(galleryUri).setAspectRatio(1, 1)
                                .start(this);

                        break;
                }
            }
        }
    }

    public void saveImage(Context context, Bitmap b, String imageName) {
        FileOutputStream foStream;
        try {
            foStream = context.openFileOutput(imageName, Context.MODE_PRIVATE);
            b.compress(Bitmap.CompressFormat.PNG, 100, foStream);
            foStream.close();
        } catch (Exception e) {
            Log.d("saveImage", "Exception 2, Something went wrong!");
            e.printStackTrace();
        }
    }

    public void showInView(Bitmap result, ImageView view) {

    }

    class ImageDownload extends AsyncTask<String, Integer, Bitmap> {
        Context context;
        ImageView imageView;
        Bitmap bitmap = null;
        InputStream in = null;
        int responseCode = -1;

        //constructor.
        public ImageDownload(Context context, ImageView imageView) {
            this.context = context;
            this.imageView = imageView;
        }

        private Bitmap downloadImageBitmap(String sUrl) {
            Bitmap bitmap = null;
            try {
                InputStream inputStream = new URL(sUrl).openStream();   // Download Image from URL
                bitmap = BitmapFactory.decodeStream(inputStream);       // Decode Bitmap
                inputStream.close();
            } catch (Exception e) {
                Log.d("ImageDownload", "Exception 1, Something went wrong!");
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return downloadImageBitmap(params[0]);
            /*
            URL url = null;
            try {
                url = new URL(params[0]);

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(false);
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.connect();
                responseCode = httpURLConnection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    in = httpURLConnection.getInputStream();
                    bitmap = BitmapFactory.decodeStream(in);
                    in.close();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap; */
        }

        @Override
        protected void onPostExecute(Bitmap data) {
            if (data != null) {
                imageView.setImageBitmap(data);
            } else {
                //imageView.setImageDrawable(R.drawable.avatar_contact);
            }
        }
    }

    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {
        private String TAG = "DownloadImage";
        private Bitmap downloadImageBitmap(String sUrl) {
            Bitmap bitmap = null;
            try {
                InputStream inputStream = new URL(sUrl).openStream();   // Download Image from URL
                bitmap = BitmapFactory.decodeStream(inputStream);       // Decode Bitmap
                inputStream.close();
            } catch (Exception e) {
                Log.d(TAG, "Exception 1, Something went wrong!");
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return downloadImageBitmap(params[0]);
        }

        protected void onPostExecute(Bitmap result) {
            saveToInternalStorage(result);
            //showInView(result, );
            //saveImage(getApplicationContext(), result, "my_image.png");
        }
    }
}



