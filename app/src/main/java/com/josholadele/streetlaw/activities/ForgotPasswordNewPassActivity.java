package com.josholadele.streetlaw.activities;

import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.josholadele.streetlaw.LoginActivity;
import com.josholadele.streetlaw.MainActivity;
import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.network.UserService;
import com.josholadele.streetlaw.push.PushRegistration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A login screen that offers login via email/password.
 */
public class ForgotPasswordNewPassActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {


    EditText mailToken;
    EditText newPassword;
    EditText confirmPassword;
    ProgressBar progressBar;
//    TextView forgotPassword;

    String resetEmail;
    AppSharedPref _sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_password_layout);
        // Set up the login form.
//        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
//        populateAutoComplete();

        mailToken = findViewById(R.id.token);
        newPassword = findViewById(R.id.password);
        confirmPassword = findViewById(R.id.confirm_password);

        progressBar = findViewById(R.id.loading_view);
        _sharedPref = new AppSharedPref(this);

        resetEmail = getIntent().getStringExtra("email");


        Button mEmailSignInButton = findViewById(R.id.btn_login_to_app);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

    }

    private void attemptLogin() {
//        if (mAuthTask != null) {
//            return;
//        }

        mailToken.setError(null);
//        loginPassword.setError(null);

        // Store values at the time of the login attempt.
        String email = mailToken.getText().toString();// mEmailView.getText().toString();

        boolean cancel = false;
        View focusView = null;


        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mailToken.setError(getString(R.string.error_field_required));
            focusView = mailToken;
            cancel = true;
        }
        if (!newPassword.getText().toString().equals(confirmPassword.getText().toString())) {
            Toast.makeText(this, "Password Mismatch", Toast.LENGTH_SHORT).show();
            return;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
//            showProgress(true);
            progressBar.setVisibility(View.VISIBLE);

            JSONObject resetObject = new JSONObject();
            try {
                resetObject.put("Token", mailToken.getText().toString());
                resetObject.put("Email", resetEmail);
                resetObject.put("NewPassword", newPassword.getText().toString());
                resetObject.put("ConfirmNewPassword", confirmPassword.getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            new UserService().resetPassword(resetObject, new NetworkResponseCallback() {
                @Override
                public void onResponse(JSONObject response) {
//                    showProgress(false);
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(ForgotPasswordNewPassActivity.this, "Password reset successful", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(ForgotPasswordNewPassActivity.this, LoginActivity.class));
                    finish();
                }

                @Override
                public void onFailed(String reason) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(ForgotPasswordNewPassActivity.this, "Failed " + reason, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onError(String reason) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(ForgotPasswordNewPassActivity.this, "Error " + reason, Toast.LENGTH_SHORT).show();

                }
            });
//            mAuthTask = new UserLoginTask(email, password);
//            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

//        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

//    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
//        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
//        ArrayAdapter<String> adapter =
//                new ArrayAdapter<>(LoginActivity.this,
//                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);
//
//        mEmailView.setAdapter(adapter);
//    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
}

