package com.josholadele.streetlaw.activities;

import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.RingtonePreference;
import android.preference.SwitchPreference;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.josholadele.streetlaw.R;

import java.util.Set;

public class UpdatedSettingsActivity extends AppCompatActivity {

    private SharedPreferences.OnSharedPreferenceChangeListener preferenceChangeListener;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updated_settings);
        // Get the fragment manager, and open an instance of NewSettingsFragment
        getFragmentManager()
                .beginTransaction()
                .add(R.id.preference_content, new NewSettingsFragment())
                .commit();

        // Get a reference to the default shared preference file
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        // Get a reference to the shared preference change listener
        preferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            /**
             * This method is called when a preference has changed
             * @param sharedPreferences The shared preference object
             * @param key The key of the changed preference
             */
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                // Display the key of the of the shared preference
                Toast.makeText(UpdatedSettingsActivity.this, "Preference of key " + key + " has changed", Toast.LENGTH_SHORT).show();


            }
        };
        // Register the shared preference object with the preference change listener
        sharedPreferences.registerOnSharedPreferenceChangeListener(preferenceChangeListener);

    }

    /**
     * When the activity is destroyed
     */
    @Override
    public void onDestroy() {
        super.onDestroy();

        // Unregister the shared preference object with the preference change listener
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(preferenceChangeListener);
    }

    /**
     * This inner class is where NewSettingsFragment is defined
     */
    public static class NewSettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            // Load the preference layout view from the layout resource file
            addPreferencesFromResource(R.xml.preferences);

            // Get a reference to the shared preference in the preference screen
            // and register it with the shared preference change listener
            getPreferenceScreen().getSharedPreferences()
                    .registerOnSharedPreferenceChangeListener(this);

            // Get a reference to the shared preference in the preference screen
            SharedPreferences sharedPreferences = getPreferenceScreen().getSharedPreferences();
            // Get a reference to the preference screen
            PreferenceScreen preferenceScreen = getPreferenceScreen();

            // Get the number of preferences in the preference screen
            int count = preferenceScreen.getPreferenceCount();

            // Loop through the preference indices in the preference screen
            for (int index = 0; index < count; index++) {
                // Get the preference at the specified index
                Preference preference = preferenceScreen.getPreference(index);
                // If the preference is not a checkbox preference, and is not a switch preference,
                if (!(preference instanceof CheckBoxPreference) && !(preference instanceof SwitchPreference)) {
                    // Get the string value of the preference, by using its key
                    String value = sharedPreferences.getString(preference.getKey(), "");
                    // Set the preference summary
                    setPreferenceSummary(preference, value);
                }
            }
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            // Get the preference associated with the changed key
            Preference preference = findPreference(key);

            // If the preference object is not null,
            if (null != preference) {
                // If the preference is not a checkbox preference and not a switch preference,
                if (!(preference instanceof CheckBoxPreference) && !(preference instanceof SwitchPreference)) {
                    // Get the value of the key that is stored in the shared preference file
                    String value = sharedPreferences.getString(preference.getKey(), "");
                    // Change the preference summary
                    setPreferenceSummary(preference, value);
                }
            }
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            // Get a reference to the shared preference in the preference screen
            // and unregister it with the shared preference change listener
            getPreferenceScreen().getSharedPreferences()
                    .unregisterOnSharedPreferenceChangeListener(this);
        }

        /**
         * This method sets the new preference summary
         *
         * @param newValueString The new preference value in string
         */

        public void setPreferenceSummary(Preference preference, String newValueString) {
            // Case 1: If the preference is a list preference,
            if (preference instanceof ListPreference) {
                // Cast the preference to a list preference object
                ListPreference listPreference = (ListPreference) preference;
                // In the list preference, get the index of the new value
                int indexOfValue = listPreference.findIndexOfValue(newValueString);
                // If the index is within a valid range (i.e it is greater than or less than zero)
                if (indexOfValue >= 0) {
                    // Get all the entries in the list preference, find the entry at the specified index,
                    // and change the summary of the list preference
                    listPreference.setSummary(listPreference.getEntries()[indexOfValue]);
                }
                // Case 2: If the preference is a ringtone preference,
            } else if (preference instanceof RingtonePreference) {
                // If the user made no choice,
                if (TextUtils.isEmpty(newValueString)) {
                    // Set the preference summary to "Silent"
                    preference.setSummary(R.string.pref_ringtone_silent);
                } else {
                    // Get the ringtone that the user specifies
                    Ringtone ringtone = RingtoneManager
                            .getRingtone(preference.getContext(),
                                    Uri.parse(newValueString));
                    // If the user's choice is null,
                    if (ringtone == null) {
                        // Set the summary to null
                        preference.setSummary(null);
                    } else {
                        // If the user chooses a valid ringtone,
                        // get the name of the ringtone
                        String nameOfRingtone = ringtone.getTitle(preference.getContext());
                        // Set the summary to the ringtone name
                        preference.setSummary(nameOfRingtone);
                    }
                }
            } else {
                // For preferences that are not ringtone or list preferences
                // Set the summary to the new preference value
                preference.setSummary(newValueString);
            }
        }

    }

}
