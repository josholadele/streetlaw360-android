package com.josholadele.streetlaw.activities;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.network.UserService;
import com.josholadele.streetlaw.utils.UserUtils;

import org.json.JSONException;
import org.json.JSONObject;


public class EditProfileActivity extends AppCompatActivity {

    Toolbar mToolbar;

    EditText firstNameET;
    EditText lastNameET;
    EditText middleNameET;
    EditText lawFirmET;
    EditText phoneNumberET;
    EditText emailET;
    EditText shortBioET;
    EditText addressET;

    LinearLayout lawFirmLayout;
    Button editButton;

    AppSharedPref sharedPref;
    UserService userService;

    JSONObject userObject;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        sharedPref = new AppSharedPref(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView titleText = toolbar.findViewById(R.id.toolbar_title);
        titleText.setText("Edit Profile");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        firstNameET = findViewById(R.id.editProfileFirstName);
        lastNameET = findViewById(R.id.editProfileLastName);
        middleNameET = findViewById(R.id.editProfileMiddleName);
        phoneNumberET = findViewById(R.id.editProfileTelephone);
        lawFirmET = findViewById(R.id.editProfileLawFirm);
        emailET = findViewById(R.id.editProfileEmail);
        shortBioET = findViewById(R.id.editProfileShortBio);
        addressET = findViewById(R.id.editProfileAddress);
        progressBar = findViewById(R.id.progress_layout);

        lawFirmLayout = findViewById(R.id.linearLayoutFirm);
        editButton = findViewById(R.id.btn_edit_profile);

        if (UserUtils.isLawyer(this)) {
            lawFirmLayout.setVisibility(View.VISIBLE);
        } else {
            lawFirmLayout.setVisibility(View.GONE);
        }

        userObject = sharedPref.getUserObject();
        bindData(userObject);

        userService = new UserService();

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(firstNameET.getText()) || TextUtils.isEmpty(lastNameET.getText())) {
                    // || TextUtils.isEmpty(emailET.getText())
                    Toast.makeText(EditProfileActivity.this, "Please fill required details", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        final JSONObject updateObject = new JSONObject();

                        updateObject.put("Id", sharedPref.getUserIdInt());
                        updateObject.put("FirstName", firstNameET.getText());
                        updateObject.put("LastName", lastNameET.getText());
                        updateObject.put("MiddleName", middleNameET.getText());
//                        updateObject.put("Email", emailET.getText());
                        updateObject.put("ShortBio", shortBioET.getText());
                        updateObject.put("Telephone", phoneNumberET.getText());
                        updateObject.put("AddressLine1", addressET.getText());
                        updateObject.put("Firm", lawFirmET.getText());

                        progressBar.setVisibility(View.VISIBLE);
                        userService.updateProfile(updateObject, new NetworkResponseCallback() {
                            @Override
                            public void onResponse(JSONObject response) {
                                progressBar.setVisibility(View.GONE);
                                updateUserObject(updateObject);
                                finish();
                            }

                            @Override
                            public void onFailed(String reason) {
                                progressBar.setVisibility(View.GONE);

                            }

                            @Override
                            public void onError(String reason) {
                                progressBar.setVisibility(View.GONE);

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    private void updateUserObject(JSONObject updateObject) {

        replaceField(updateObject, FIRST_NAME);
        replaceField(updateObject, LAST_NAME);
        replaceField(updateObject, MIDDLE_NAME);
        replaceField(updateObject, TELEPHONE);
        replaceField(updateObject, LAW_FIRM);
        replaceField(updateObject, SHORT_BIO);
        replaceField(updateObject, ADDRESS);
        replaceField(updateObject, EMAIL);
        sharedPref.cacheUserObject(userObject);
    }

    void replaceField(JSONObject updateObject, String field) {
        try {
            if (userObject.has(field)) {
                userObject.remove(field);
                userObject.put(field, updateObject.optString(field));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private final static String FIRST_NAME = "FirstName";
    private final static String LAST_NAME = "LastName";
    private final static String MIDDLE_NAME = "MiddleName";
    private final static String TELEPHONE = "Telephone";
    private final static String SHORT_BIO = "ShortBio";
    private final static String EMAIL = "Email";
    private final static String ADDRESS = "AddressLine1";
    private final static String LAW_FIRM = "Firm";
    /*

        "Id": 1044,
        "Category": "judicialstaff",
        "JudicialDivision": "Ikeja judicial division, Lagos",
        "FirstName": "Reggie",
        "LastName": "Oladele",
        "MiddleName": null,
        "UserName": "reggie",
        "Gender": "Female",
        "Email": "reggie@oladele.com",
        "ShortBio": "I am a registrar at Ikoyi High Court",
        "IsHeadJudge": false,
        "FCMToken": "drLZGR5zsks:APA91bH402vtUd9FMIQr0TPw-91FvWYbPM64nnq6X5cl6OcTdQTy7Sa3LW9ibEC4fGMIm-DFqHntUIR0fVGIE1j4xpwl1Buw02jxO6GmqMDrPCOtNZvDGKF9bTsPtm4bhUQlUByKngdR",
        "Password": null,
        "Telephone": null,
        "AddressLine1": null,
        "AddressLine2": null,
        "ProfilePicture": null,
        "City": "Lagos",
        "State": "Lagos",
        "Country": "Nigeria",
        "Channel": 0,
        "ChannelName": "Android",
        "SupremeCourtEnrollmentNumber": null,
        "Firm": null,
        "Verified": false,
        "DateRegistered": "2018-03-31T19:36:36.277",
        "YearCalled": null,
        "SubscriptionExpiryTime": "0001-01-01T00:00:00",
        "JudicialDivisionId": 9,
        "Cases": null,
        "Notifications": null
    * */

    private void bindData(JSONObject userObject) {
        firstNameET.setText(userObject.optString("FirstName").equalsIgnoreCase("null") ? "" : userObject.optString("FirstName"));
        lastNameET.setText(userObject.optString("LastName").equalsIgnoreCase("null") ? "" : userObject.optString("LastName"));
        middleNameET.setText(userObject.optString("MiddleName").equalsIgnoreCase("null") ? "" : userObject.optString("MiddleName"));
        emailET.setText(userObject.optString("Email").equalsIgnoreCase("null") ? "" : userObject.optString("Email"));
        shortBioET.setText(userObject.optString("ShortBio").equalsIgnoreCase("null") ? "" : userObject.optString("ShortBio"));
        addressET.setText(userObject.optString("AddressLine1").equalsIgnoreCase("null") ? "" : userObject.optString("AddressLine1"));
        phoneNumberET.setText(userObject.optString("Telephone").equalsIgnoreCase("null") ? "" : userObject.optString("Telephone"));
        lawFirmET.setText(userObject.optString("Firm").equalsIgnoreCase("null") ? "" : userObject.optString("Firm"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
