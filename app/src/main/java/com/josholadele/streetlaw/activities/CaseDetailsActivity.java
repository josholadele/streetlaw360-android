package com.josholadele.streetlaw.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListAdapter;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListItem;
import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.adapters.AssigneesAdapter;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.enums.CaseStatus;
import com.josholadele.streetlaw.model.Case;
import com.josholadele.streetlaw.model.CaseFile;
import com.josholadele.streetlaw.model.QuickNotice;
import com.josholadele.streetlaw.model.User;
import com.josholadele.streetlaw.network.CaseService;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.utils.AlternateDialogUtils;
import com.josholadele.streetlaw.utils.DialogUtils;
import com.josholadele.streetlaw.utils.UserUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CaseDetailsActivity extends AppCompatActivity implements AssigneesAdapter.AssigneeClickListener, AdapterView.OnItemSelectedListener {
    private static final String LOG_TAG = CaseDetailsActivity.class.getSimpleName();


    public static final String SELECTED_CASE_ID = "selected-case";
    private static final String CASE_STATUS_PREFERENCE = "case_status_preferences";

    private static String noticeValue;

    TextView suitNumberTV;
    TextView suitNumber;
    TextView caseTitleTV;
    Spinner caseStatusSpinner;
    TextView caseCurrentTV;
    TextView caseTypeTV;
    TextView caseIssueForDeterminationTV;
    TextView caseDescriptionTV;
    TextView caseJudicialDistrict;
    TextView caseJudgeName;
    ScrollView pageContent;
    TextView emptyView;
    ProgressBar contentLoading;
    RecyclerView lawyersRecycler;
    RecyclerView quickNoticeRecyclerView;

    CaseFile newCase;
    AppSharedPref sharedPref;
    MaterialDialog statusDialog;
    AssigneesAdapter lawyersAdapter;
    FloatingActionButton quickNoticeButton;

    private CaseService _caseService;
    private int _selectedCaseId;
    private Case _case;
    private JSONObject _caseObject;

    private JSONObject quickNoticeObject;

    public String descriptionText;

    Context mContext;
    String selectedCaseIdString;

    ArrayList<QuickNotice> quickNoticeArrayList;
    QuickNoticeAdapter quickNoticeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_case_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView titleText = toolbar.findViewById(R.id.toolbar_title);

        descriptionText = null;
        newCase = getIntent().getParcelableExtra("case");
        sharedPref = new AppSharedPref(this);

        Log.e(LOG_TAG, "This is the description " + descriptionText);


        // Bind to XML view components
        suitNumberTV = findViewById(R.id.case_details_suitnumber_value);
        suitNumber = findViewById(R.id.case_suit_number);
        caseTitleTV = findViewById(R.id.case_title);
        caseStatusSpinner = findViewById(R.id.spn_case_details_status);
        caseJudicialDistrict = findViewById(R.id.case_details_judicial_district);
        caseJudgeName = findViewById(R.id.case_details_judge_name);
        caseDescriptionTV = findViewById(R.id.case_description);
        caseTypeTV = findViewById(R.id.case_suit_type);
        caseIssueForDeterminationTV = findViewById(R.id.case_issue_determination);
        contentLoading = findViewById(R.id.progress_layout);
        emptyView = findViewById(R.id.empty_view);
        pageContent = findViewById(R.id.data_layout);
        lawyersRecycler = findViewById(R.id.lawyers_recyclerview);
        quickNoticeButton = findViewById(R.id.fab_case_details_quick_notice);

        Typeface american_typewriter = Typeface.createFromAsset(getAssets(), "fonts/American_Typewriter_Regular.ttf");
        Typeface avenil = Typeface.createFromAsset(getAssets(), "fonts/AvenirLTStd_Medium.otf");

        titleText.setText("CASE DETAILS");

        setSupportActionBar(toolbar);
//        getSupportActionBar().setHome

        // Get the ID of the selected case
        _selectedCaseId = getIntent().getIntExtra(CaseDetailsActivity.SELECTED_CASE_ID, 0);
        _caseService = new CaseService();
        _case = new Case();

        // Instantiate the quick notice recycler View
        //setupQuickNoticeRecyclerView();


        // Instantiate spinner adapter with string array and spinner layout
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.status, R.layout.spinner_item_view);
        // Set spinner dropdown layout
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set up spinner with spinnerAdapter
        caseStatusSpinner.setAdapter(spinnerAdapter);
        // Set up spinner action when clicked
        caseStatusSpinner.setOnItemSelectedListener(this);

        selectedCaseIdString = Integer.toString(_selectedCaseId);

        SharedPreferences preferences = getPreferences(MODE_PRIVATE);


        int selectedItemPosition = preferences.getInt(selectedCaseIdString, -1);
        Toast.makeText(this, "Retrieved item position at " + selectedCaseIdString + " is " + selectedItemPosition, Toast.LENGTH_SHORT).show();

        // If the case has a saved status in the shared preference file, set the spinner to that value
        if (selectedItemPosition != -1) {
            caseStatusSpinner.setSelection(selectedItemPosition);

        } else {
            Toast.makeText(this, "No value", Toast.LENGTH_SHORT).show();
        }


        caseTitleTV.setTypeface(american_typewriter, Typeface.BOLD);

        TextView suitNoTitle = findViewById(R.id.suit_no_title);

        TextView suitTypeTitle = findViewById(R.id.suit_type_title);

        TextView caseCurrentTitle = findViewById(R.id.current_status_title);

        TextView caseLawyers = findViewById(R.id.case_lawyers);

        caseLawyers.setTypeface(american_typewriter, Typeface.BOLD);

        caseTitleTV.setText(newCase.Title);

        caseDescriptionTV.setText("Test" + descriptionText);

        suitNumber.setText(newCase.suitNumber);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        getCaseDetailsOnActivity();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        quickNoticeRecyclerView = findViewById(R.id.rv_case_details_quick_notices);

        quickNoticeRecyclerView.setLayoutManager(linearLayoutManager);

        quickNoticeArrayList = new ArrayList<>();

        quickNoticeAdapter = new QuickNoticeAdapter(quickNoticeArrayList);

        quickNoticeRecyclerView.setAdapter(quickNoticeAdapter);

        // When the  quick notice button is clicked
        quickNoticeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Show a pop-up with an EditText
                AlternateDialogUtils.createEditTextDialog(CaseDetailsActivity.this, "Quick notice", new AlternateDialogUtils.UserResponseListeners() {
                    @Override
                    public void onPositive() {
                        if (AlternateDialogUtils.quickNoticeValue != null) {
                            // Get the notice value from the pop-up
                            noticeValue = AlternateDialogUtils.quickNoticeValue;

                            Toast.makeText(CaseDetailsActivity.this, "Notice value is " + noticeValue, Toast.LENGTH_SHORT).show();
                            //quickNoticeArrayList.clear();

                            // Create a JSON object
                            JSONObject quickNoticeObject = new JSONObject();

                            try {
                                // Add quick notice details to the JSON object
                                quickNoticeObject.put("CaseId", _selectedCaseId);
                                quickNoticeObject.put("NotifierUserId", 1);
                                quickNoticeObject.put("Message", noticeValue);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            // Post the JSON object to the database
                            _caseService.postQuickNotice(quickNoticeObject, new NetworkResponseCallback() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Toast.makeText(CaseDetailsActivity.this, "Notification response" + response, Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onFailed(String reason) {
                                    Toast.makeText(CaseDetailsActivity.this, "Notification failed" + reason, Toast.LENGTH_SHORT).show();
                                    Toast.makeText(CaseDetailsActivity.this, "Notification error" + reason, Toast.LENGTH_SHORT).show();

                                }

                                @Override
                                public void onError(String reason) {

                                }
                            });

                            // Get the notices from the API, and add them to the RecyclerView
                           getQuickNotices();

                        } else {
                            Toast.makeText(CaseDetailsActivity.this, "Notice value is null", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onNegative() {

                    }

                    @Override
                    public void onNeutral() {

                    }
                });
            }
        });
    }


    /**
     * Set action for when a spinner item is clicked
     *
     * @param adapterView
     * @param view
     * @param position    This is the position of the spinner item
     * @param l
     */
    @Override
    public void onItemSelected(final AdapterView<?> adapterView, View view, final int position, long l) {
        AlternateDialogUtils.createPlainDialog(this, "Status Update", "Are you sure you want to change the status?", new AlternateDialogUtils.UserResponseListeners() {
            @Override
            public void onPositive() {
                // Get the item at the clicked position
                final String item = adapterView.getItemAtPosition(position).toString();

                // Get the position of the selected item
                int savedItemPosition = caseStatusSpinner.getSelectedItemPosition();
                Toast.makeText(CaseDetailsActivity.this, "This is the saved selected case ID" + selectedCaseIdString, Toast.LENGTH_SHORT).show();
                Toast.makeText(CaseDetailsActivity.this, "This is the saved spinner value" + savedItemPosition, Toast.LENGTH_SHORT).show();

                SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);

                // Get an instance of the shared preferences editor
                SharedPreferences.Editor editor = sharedPreferences.edit();
                // Put the selected item's position to the shared preference file
                editor.putInt(selectedCaseIdString, savedItemPosition);
                // Save the selected item's position to the shared preference file
                editor.apply();


                JSONObject statusObject = new JSONObject();
                try {
                    // Add the ID to the JSON object
                    statusObject.put("id", newCase.Id);
                    // Add the status to the JSON object
                    statusObject.put("status", item);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                // Update the case status using a PUT method
                _caseService.changeStatus(Integer.parseInt(sharedPref.getUserId()), statusObject, new NetworkResponseCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Display the selected item with a toast
                        Toast.makeText(CaseDetailsActivity.this, "Response is: " + response, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailed(String reason) {
                        Toast.makeText(CaseDetailsActivity.this, "Reason for failure is: " + reason, Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onError(String reason) {
                        Toast.makeText(CaseDetailsActivity.this, "Error log: " + reason, Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @Override
            public void onNegative() {

            }

            @Override
            public void onNeutral() {

            }
        });

    }

    // If no spinner item is selected, do nothing.
    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    private void showErrorMessage() {
        emptyView.setVisibility(View.VISIBLE);
        contentLoading.setVisibility(View.GONE);
        pageContent.setVisibility(View.GONE);

    }

    private void showDataView() {
        emptyView.setVisibility(View.GONE);
        pageContent.setVisibility(View.VISIBLE);
        contentLoading.setVisibility(View.GONE);
    }

    private void showLoading() {
        emptyView.setVisibility(View.GONE);
        pageContent.setVisibility(View.GONE);
        contentLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (UserUtils.isJudgeOrRegistrar(this)) {
            getMenuInflater().inflate(R.menu.menu_case_detail, menu);

        }

        String ucategory = sharedPref.getUserCategory();
        MenuItem reassignItem = menu.findItem(R.id.action_reassign);
        if (ucategory.equalsIgnoreCase("Judge")
                || ucategory.equalsIgnoreCase("ChiefJudge")) {
            if (null != reassignItem)
                reassignItem.setVisible(true);
        } else {
            if (null != reassignItem)
                reassignItem.setVisible(false);
        }

        return true;
//        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        // If user selects the home button
        if (id == android.R.id.home) {
            // Go back to case files
            onBackPressed();
            return true;
        }
        // If user selects the re-assign menu item
        if (id == R.id.action_reassign) {

//            Toast.makeText(this, "Reassign", Toast.LENGTH_SHORT).show();

            // Go to the activity that handles reassignment of judges
            Intent intent = new Intent(this, ChooseJudgeActivity.class);
            setIntentData(intent);
            startActivityForResult(intent, 12);
            return true;
        }
        if (id == R.id.action_change_status) {
            buildDialog();
            statusDialog.show();
            return true;
        }
        if (id == R.id.action_notify) {
            Intent intent = new Intent(this, NotifyLawyerActivity.class);

            intent.putExtra("case-id", newCase.Id);
            intent.putExtra("isReassign", false);
//            setIntentData(intent);

            startActivityForResult(intent, 102);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 102 && resultCode == Activity.RESULT_OK) {
            getCaseDetailsOnActivity();
        }
    }

    private void setIntentData(Intent intent) {
        ArrayList<Integer> selectedCaseIds = new ArrayList<>();

        selectedCaseIds.add(newCase.Id);

        if (selectedCaseIds.size() > 0) {
            intent.putExtra("selectedIds", selectedCaseIds);
        }
        intent.putExtra("isReassign", true);

    }

    private void buildDialog() {
        MaterialSimpleListAdapter adapter = new MaterialSimpleListAdapter(new MaterialSimpleListAdapter.Callback() {
            @Override
            public void onMaterialListItemSelected(MaterialDialog dialog, int index, MaterialSimpleListItem item) {

                //Create a new JSONObject instance
                JSONObject statusObject = new JSONObject();
                try {
                    // Put the value of ID, and its key, into the JSONObject instance
                    statusObject.put("id", newCase.Id);

                    // Put the value of content, and its key, into the JSONObject instance
                    statusObject.put("status", item.getContent());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                _caseService.changeStatus(Integer.parseInt(sharedPref.getUserId()), statusObject, new NetworkResponseCallback() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }

                    @Override
                    public void onFailed(String reason) {

                    }

                    @Override
                    public void onError(String reason) {

                    }
                });
                statusDialog.dismiss();
            }
        });
        for (CaseStatus caseStatus : CaseStatus.values()) {
            adapter.add(new MaterialSimpleListItem.Builder(this).content(caseStatus.statusText).backgroundColor(Color.WHITE).build());

        }
        statusDialog = new MaterialDialog.Builder(this)
                .title(R.string.change_status)
                .adapter(adapter, null).build();
//        new MaterialDialog.Builder(this)
    }

    private void bindData() {
        if (_caseObject != null) {

            lawyersAdapter = new AssigneesAdapter(this);
            lawyersRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            lawyersRecycler.setAdapter(lawyersAdapter);
            JSONArray userArray = _caseObject.optJSONArray("Users");

            List<User> users = new ArrayList<>();

            for (int i = 0; i < userArray.length(); i++) {
                User user = new User();
                try {
                    JSONObject userObject = userArray.getJSONObject(i);

                    user.displayName = userObject.optString("FirstName") + " " + userObject.optString("LastName");
                    user.userId = userObject.optInt("Id");
                    user.displayPicture = userObject.optString("ProfilePicture");

                    // Log the image url
                    Log.e(LOG_TAG, "This is the image: " + user.displayPicture);

                    // Add user object to the arraylist
                    users.add(user);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            lawyersAdapter.setData(users);

        }
    }

    private void getQuickNotices() {
        try {
            _caseService.getQuickNotices(_selectedCaseId, new NetworkResponseCallback() {
                @Override
                public void onResponse(JSONObject response) {
                    quickNoticeObject = response;

                    Log.e(LOG_TAG, "The JSON object is: " + quickNoticeObject.toString());
                    bindQuickNoticeData();

                }

                @Override
                public void onFailed(String reason) {
                    Toast.makeText(CaseDetailsActivity.this, "Unable to retrieve quick notice: " + reason, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onError(String reason) {
                    Toast.makeText(CaseDetailsActivity.this, "Unable to retrieve quick notice: " + reason, Toast.LENGTH_SHORT).show();

                }
            });

        } catch (Exception ex) {
            Toast.makeText(this, "Unable to retrieve quick notice: " + ex.getMessage(), Toast.LENGTH_SHORT).show();

        }
    }

    /**
     * Extract the data from the JSON object and bind it to the quick notice RecyclerView
     */

    private void bindQuickNoticeData() {
        if (quickNoticeObject != null) {
            JSONArray quickNoticesArray = quickNoticeObject.optJSONArray("Data");


            for (int i = 0; i < quickNoticesArray.length(); i++) {
                QuickNotice quickNotice = new QuickNotice();
                try {
                    JSONObject noticeObject = quickNoticesArray.getJSONObject(i);

                    String noticeString = noticeObject.optString("Message");

                    quickNotice.setQuickNotice(noticeString);

                    // Log the notice string
                    Log.e(LOG_TAG, "This is the notice string: " + quickNotice.getQuickNotice());

                    // Add user object to the arraylist
                    quickNoticeArrayList.add(quickNotice);
                    quickNoticeAdapter.notifyDataSetChanged();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void getCaseDetailsOnActivity() {
        try {
            _caseService.getById(_selectedCaseId, new NetworkResponseCallback() {
                @Override
                public void onResponse(JSONObject response) {
                    _caseObject = response.optJSONObject("Data");
                    try {

                        descriptionText = _caseObject.getString("Description");

                        // Test to see if there's any data for description
                        // Log the data
                        Log.e(LOG_TAG, "Test for JSON object is "
                                + descriptionText
                                + "   "
                                + _caseObject.getString("Title")
                                + "   ");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    Log.e(LOG_TAG, "The JSON object is: " + _caseObject.toString());

                    Toast.makeText(CaseDetailsActivity.this, _case.toString(), Toast.LENGTH_SHORT).show();
                    bindData();
                }

                @Override
                public void onFailed(String reason) {
                    Toast.makeText(CaseDetailsActivity.this, "Unable to retrieve case: " + reason, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onError(String reason) {
                    Toast.makeText(CaseDetailsActivity.this, "Unable to retrieve case: " + reason, Toast.LENGTH_SHORT).show();

                }
            });

        } catch (Exception ex) {
            Toast.makeText(this, "Unable to retrieve case: " + ex.getMessage(), Toast.LENGTH_SHORT).show();

        }
    }

    public void onAssigneeClick(final User user, boolean isLongClick) {
        DialogUtils.createDialog(this, null, "Remove " + user.displayName + " from case", null, new DialogUtils.UserResponseListeners() {
            @Override
            public void onPositive() {
                _caseService.removeLawyer(user.userId, Integer.parseInt(sharedPref.getUserId()), newCase.Id, new NetworkResponseCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        lawyersAdapter.removeLawyer(user);
                    }

                    @Override
                    public void onFailed(String reason) {
                        Toast.makeText(CaseDetailsActivity.this, "Unable to remove lawyer: " + reason, Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onError(String reason) {
                        Toast.makeText(CaseDetailsActivity.this, "Unable to remove lawyer: " + reason, Toast.LENGTH_SHORT).show();

                    }
                });
            }

            @Override
            public void onNegative() {

            }

            @Override
            public void onNeutral() {

            }
        });
    }
}