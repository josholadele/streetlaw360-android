package com.josholadele.streetlaw.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.model.Note;
import com.josholadele.streetlaw.network.DiaryService;
import com.josholadele.streetlaw.network.NetworkResponseCallback;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by josh on 24/03/2018.
 */

public class EditNoteActivity extends AppCompatActivity {


    //    Button btnCancel;
    Button btnAddNote;


    EditText editNoteTopic;
    EditText editNoteContent;
//    EditText editCaseDescription;

    ProgressBar progressBar;
    Note note;

    AppSharedPref sharedPref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_new_note);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView titleText = toolbar.findViewById(R.id.toolbar_title);

        sharedPref = new AppSharedPref(this);

        btnAddNote = findViewById(R.id.btn_add_to_notes);
        btnAddNote.setText("Save Changes");

        progressBar = findViewById(R.id.progress_layout);
        editNoteTopic = findViewById(R.id.editNoteTopic);
        editNoteContent = findViewById(R.id.editNoteContent);


        titleText.setText("Edit Note");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        note = getIntent().getParcelableExtra("note");

        editNoteTopic.setText(note.topic);
        editNoteContent.setText(note.content);

        btnAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(editNoteTopic.getText()) && !TextUtils.isEmpty(editNoteContent.getText())
                        && (!editNoteTopic.getText().toString().equals(note.topic) || !editNoteContent.getText().toString().equals(note.content))) {
                    progressBar.setVisibility(View.VISIBLE);
                    addNote();
                }
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return onOptionsItemSelected(item);
    }

    private void addNote() {

        JSONObject object = new JSONObject();
        try {
            object.put("Topic", editNoteTopic.getText().toString().trim());
            object.put("Content", editNoteContent.getText().toString().trim());
            object.put("userId", sharedPref.getUserIdInt());
            object.put("Id", note.Id);

            DiaryService diaryService = new DiaryService();
            diaryService.updateNote(object, new NetworkResponseCallback() {
                @Override
                public void onResponse(JSONObject response) {
//                    showProgress(false);
                    progressBar.setVisibility(View.GONE);

                    Intent intent = new Intent();
                    intent.putExtra("note", new Note(response.optInt("Id"), sharedPref.getUserIdInt(), editNoteTopic.getText().toString(),
                            editNoteContent.getText().toString(), response.optString("DateCreated"), response.optString("LastDateOfUpdate")));
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }

                @Override
                public void onFailed(String reason) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(EditNoteActivity.this, "Unable to update note: " + reason, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onError(String reason) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(EditNoteActivity.this, "Error: " + reason, Toast.LENGTH_SHORT).show();

                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
