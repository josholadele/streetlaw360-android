package com.josholadele.streetlaw.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

//import com.google.firebase.auth.FirebaseAuth;
import com.josholadele.streetlaw.LoginActivity;
import com.josholadele.streetlaw.MainActivity;
import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.cache.AppSharedPref;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        final AppSharedPref appSharedPref = new AppSharedPref(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (appSharedPref.isFirstTime()) {
                    startActivity(new Intent(SplashScreen.this, OnboardingActivity.class));
                    finish();
                } else {
                    if (appSharedPref.hasLoggedIn()) {

                        startActivity(new Intent(SplashScreen.this, MainActivity.class));
                        finish();
                    } else {

                        startActivity(new Intent(SplashScreen.this, LandingPage.class));
                        finish();
                    }
                }
            }
        }, 400);
    }
}
