package com.josholadele.streetlaw.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.adapters.LawyerAdapter;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.model.CaseFile;
import com.josholadele.streetlaw.model.Lawyer;
import com.josholadele.streetlaw.network.CaseService;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.network.UserService;
import com.josholadele.streetlaw.utils.DialogUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NotifyLawyerActivity extends AppCompatActivity implements LawyerAdapter.LawyerClickListener, SearchView.OnQueryTextListener {

    RecyclerView lawyersRecycler;
    LawyerAdapter mAdapter;
    Toolbar mToolbar;

    TextView toolbarTitle;
    TextView toolbarSelectionText;

    TextView addCaseTV;
    ImageView addCaseImg;


    ProgressBar contentLoading;
    TextView emptyView;

    MenuItem assignMenu;
    MenuItem filterMenu;

    List<Lawyer> files;
    AppSharedPref _sharedPref;
    //    ArrayList<Integer> selectedCaseIds;
//    boolean isReassign;
    int caseId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_lawyer);


        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        _sharedPref = new AppSharedPref(this);

//        selectedCaseIds = getIntent().getIntegerArrayListExtra("selectedIds");

        caseId = getIntent().getIntExtra("case-id", -1);
//        isReassign = getIntent().getBooleanExtra("isReassign", false);

        toolbarTitle = mToolbar.findViewById(R.id.client_logo);
        toolbarTitle.setText("Notify..");
        toolbarSelectionText = mToolbar.findViewById(R.id.selection_number);


        contentLoading = findViewById(R.id.progress_layout);
        emptyView = findViewById(R.id.empty_view);

        lawyersRecycler = findViewById(R.id.lawyers_recyclerview);

        setupRecyclerView(lawyersRecycler);
//        User user =
        fetchLawyers();

    }


    private void setupRecyclerView(RecyclerView recyclerView) {
        mAdapter = new LawyerAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
//        getSampleCaseFiles();
    }

    @Override
    public void onBackPressed() {

        if (mAdapter.IS_MULTISELECT) {
            mAdapter.IS_MULTISELECT = false;
            for (int i : mAdapter.selectedPositions) {
                lawyersRecycler.getChildAt(i).setActivated(false);
            }
            mAdapter.clearSelections();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_select_lawyer, menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_select_lawyer, menu);

//        assignMenu = menu.findItem(R.id.action_assign);
//        filterMenu = menu.findItem(R.id.action_filter);
//        return true;
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        item.setOnActionExpandListener( new MenuItem.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {

                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                mAdapter.setSearchResult(files);
                return true; // Return true to collapse action view

            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        // Here is where we are going to implement the filter logic
        final List<Lawyer> filteredModelList = filter(files, query);
        if(filteredModelList.size() > 0) {
            mAdapter.setSearchResult(filteredModelList);
            lawyersRecycler.scrollToPosition(0);
            showDataView();
        } else {
            mAdapter.setSearchResult(filteredModelList);
            showErrorMessage();
        }
        return true;
    }

    private static List<Lawyer> filter(List<Lawyer> models, String query) {
        final String lowerCaseQuery = query.toLowerCase();

        final List<Lawyer> filteredModelList = new ArrayList<>();
        if(null != models) {
            for (Lawyer model : models) {
                final String text = model.name.toLowerCase();
                final String email = model.email.toLowerCase();
                final String username = model.username.toLowerCase();
                if (text.contains(lowerCaseQuery) || email.contains(lowerCaseQuery) || username.contains(lowerCaseQuery)) {
                    filteredModelList.add(model);
                }
            }
        }
        return filteredModelList;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        if (item.getItemId() == R.id.action_done) {
            if (mAdapter.selectedPositions.size() > 0 && caseId != -1) {

                String notifyContent = getResources().getString(R.string.notify_text, String.valueOf(mAdapter.selectedPositions.size()));
                DialogUtils.createDialog(this, "Notify", notifyContent, null, new DialogUtils.UserResponseListeners() {
                    @Override
                    public void onPositive() {
                        notifyLawyers(caseId);
                    }

                    @Override
                    public void onNegative() {
                        Toast.makeText(NotifyLawyerActivity.this, "Dismissed", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNeutral() {

                    }
                });
            }
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onLawyerClick(final Lawyer lawyer, boolean isLongClick) {


    }

    private ArrayList<Integer> setIntentData() {
        ArrayList<Integer> selectedCaseIds = new ArrayList<>();
        for (int i : mAdapter.selectedPositions) {
            Lawyer lawyer = mAdapter.getData().get(i);
            selectedCaseIds.add(lawyer.Id);
        }
        return selectedCaseIds;
    }

    private void notifyLawyers(int id) {
        showLoading(false);
        ArrayList<Integer> selectedLawyerIds = new ArrayList<>();
        for (int i : mAdapter.selectedPositions) {
            Lawyer data = mAdapter.getData().get(i);
            selectedLawyerIds.add(data.Id);
        }

        CaseService caseService = new CaseService();
        caseService.notifyLawyers(Integer.parseInt(new AppSharedPref(this).getUserId()), id, selectedLawyerIds, new NetworkResponseCallback() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(NotifyLawyerActivity.this, "Notified", Toast.LENGTH_SHORT).show();
                setResult(Activity.RESULT_OK);
                finish();
            }

            @Override
            public void onFailed(String reason) {
                Toast.makeText(NotifyLawyerActivity.this, "Unable to notify lawyers: " + reason, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String reason) {

                Toast.makeText(NotifyLawyerActivity.this, "Unable to notify lawyers: " + reason, Toast.LENGTH_SHORT).show();
            }
        });
    }

    void fetchLawyers() {

        showLoading(true);
        UserService userService = new UserService();
        userService.getLawyers(new NetworkResponseCallback() {
            @Override
            public void onResponse(JSONObject response) {
                List<Lawyer> judgeList = buildFromResult(response.optJSONArray("Data"));
                if (judgeList != null && judgeList.size() > 0) {
                    showDataView();
                    files = judgeList;
                    mAdapter.setLawyersData(judgeList);
                } else {
                    showErrorMessage();
                }
            }

            @Override
            public void onFailed(String reason) {
                showErrorMessage();
            }

            @Override
            public void onError(String reason) {
                showErrorMessage();
            }
        });
    }

    private void showErrorMessage() {
        emptyView.setVisibility(View.VISIBLE);
        lawyersRecycler.setVisibility(View.GONE);
        contentLoading.setVisibility(View.GONE);

    }

    private void showDataView() {
        emptyView.setVisibility(View.GONE);
        lawyersRecycler.setVisibility(View.VISIBLE);
        contentLoading.setVisibility(View.GONE);
    }

    private void showLoading(boolean hideRecycler) {
        emptyView.setVisibility(View.GONE);
        if (hideRecycler) {
            lawyersRecycler.setVisibility(View.GONE);
        } else {
            lawyersRecycler.setVisibility(View.VISIBLE);
        }
        contentLoading.setVisibility(View.VISIBLE);
    }

    private List<Lawyer> buildFromResult(JSONArray result) {

        if (result == null || result.equals("")) {
            return null;
        }

        JSONArray movieJSONArray = null;
        try {
            movieJSONArray = result;
        } catch (Exception e) {
            e.printStackTrace();
        }


        List<Lawyer> judges = new ArrayList<>();


        for (int i = 0; i < movieJSONArray.length(); i++) {
            JSONObject object = movieJSONArray.optJSONObject(i);
            String name = object.optString("FirstName") + " " + object.optString("LastName");
            Lawyer judge = new Lawyer(object.optInt("Id"), name,
                    object.optString("ProfilePicture"),
                    object.optString("UserName"),
                    object.optString("Email"));
            judges.add(judge);
        }
        return judges;
    }


    public void updateToolbar(boolean is_multiselect) {
        if (is_multiselect) {
            toolbarSelectionText.setVisibility(View.VISIBLE);
            String selectedNumer = mAdapter.selectedPositions.size() == 1 ? mAdapter.selectedPositions.size() + " case selected" : mAdapter.selectedPositions.size() + " cases selected";
            toolbarTitle.setVisibility(View.GONE);
            toolbarSelectionText.setText(selectedNumer);
            assignMenu.setVisible(true);
            filterMenu.setVisible(false);
        } else {
            toolbarSelectionText.setText("");
            toolbarSelectionText.setVisibility(View.GONE);
            toolbarTitle.setVisibility(View.VISIBLE);
            assignMenu.setVisible(false);
            filterMenu.setVisible(true);
        }
    }
}
