package com.josholadele.streetlaw.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.enums.CaseStatus;
import com.josholadele.streetlaw.model.Case;
import com.josholadele.streetlaw.model.CaseFile;
import com.josholadele.streetlaw.network.CaseService;
import com.josholadele.streetlaw.network.NetworkResponseCallback;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by josh on 24/03/2018.
 */

public class NewCaseActivity extends AppCompatActivity {


    Button btnCancel;
    Button btnAddCase;


    EditText editCaseTitle;
    EditText editCaseNumber;
    EditText editCaseDescription;

    ProgressBar progressBar;

    AppSharedPref sharedPref;
    Spinner spinnerType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_new_case);

        final String[] suitTypes = new String[]{"Family", "Land", "Criminal"};

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView titleText = toolbar.findViewById(R.id.toolbar_title);

        sharedPref = new AppSharedPref(this);
        spinnerType = findViewById(R.id.spinnerSuitType);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                R.layout.spinner_item_view,
                suitTypes
        );

        spinnerType.setAdapter(adapter);

        btnAddCase = findViewById(R.id.btn_add_case);
        btnCancel = findViewById(R.id.btn_cancel);
        progressBar = findViewById(R.id.progress_layout);
        editCaseTitle = findViewById(R.id.editCaseTitle);
        editCaseNumber = findViewById(R.id.editCaseSuitNumber);
        editCaseDescription = findViewById(R.id.editCaseDescription);


        CaseFile newCase = getIntent().getParcelableExtra("case");

//        titleText.setText("Case Details");
        titleText.setText("Add New Case");
        setSupportActionBar(toolbar);
//        getSupportActionBar().setHome


        btnAddCase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                addCase();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void addCase() {
//        AddCaseViewModel addCaseViewModel = new AddCaseViewModel
//        {
//            title = _caseTitle,
//                    SuitNumber = _caseSuitNumber,
//                    Description = _caseDescription,
//                    RegistrarUserId = userId
//        } ;
        JSONObject object = new JSONObject();
        try {
            object.put("title", editCaseTitle.getText().toString().trim());
            object.put("SuitNumber", editCaseNumber.getText().toString().trim());
            object.put("Description", editCaseDescription.getText().toString().trim());
            object.put("Type", spinnerType.getSelectedItem().toString());
//            object.put("RegistrarUserId", sharedPref.getUserObject().optString("Id"));
            object.put("RegistrarUserId", sharedPref.getUserId());
            object.put("JudicialDivisionId", sharedPref.getJudicialDivision());

            CaseService caseService = new CaseService();
            caseService.addCase(object, new NetworkResponseCallback() {
                @Override
                public void onResponse(JSONObject response) {
//                    showProgress(false);
                    progressBar.setVisibility(View.GONE);

                    /*
                     public int Id;
    public String SuitNumber;
    public String Title;
    public String Description;
    public long DateRegistered;
    public int RegistrarUserId;
    //public RegistrarViewModel Registrar;
    public long DateAssigned;
    public int AssignerUserId;
    //public AssignerViewModel Assigner;
    public long StatusDate;
    public String Status;
    //public CourtDivisionViewModel CourtDivision;
    public List<User> Users;
                    * */

                    Case newCase = new Case();
//                    new

                    Intent intent = new Intent();
                    intent.putExtra("case", new CaseFile(response.optInt("Id"), CaseStatus.Registered, editCaseTitle.getText().toString(), editCaseNumber.getText().toString(), editCaseDescription.getText().toString()));
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }

                @Override
                public void onFailed(String reason) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(NewCaseActivity.this, "Unable to add case: " + reason, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onError(String reason) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(NewCaseActivity.this, "Error: " + reason, Toast.LENGTH_SHORT).show();

                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
