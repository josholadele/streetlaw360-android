package com.josholadele.streetlaw.activities;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.adapters.MessageListAdapter;
import com.josholadele.streetlaw.cache.AppSharedPref;
import com.josholadele.streetlaw.model.ChatContact;
import com.josholadele.streetlaw.model.User;
import com.josholadele.streetlaw.model.UserMessage;
import com.josholadele.streetlaw.network.NetworkResponseCallback;
import com.josholadele.streetlaw.push.StreetlawMessagingService;
import com.josholadele.streetlaw.services.ChatService;
import com.josholadele.streetlaw.utils.TimeUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ChatScreenActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    AppSharedPref sharedPref;
    RecyclerView chatList;

    int recipientId;
    private final static String CHANNEL_ID = "Chat";
    private static final String COLLECTION_KEY = "Chat";
    private static final String DOCUMENT_KEY = "Messages";
    private static String ROOM_KEY;
    private static final Query firestoreChat = FirebaseFirestore.getInstance().collection(COLLECTION_KEY);

    public static void launch(Context context, ChatContact selectedContact) {
        Intent intent = new Intent(context, ChatScreenActivity.class);
        intent.putExtra("selected-contact", selectedContact);
        context.startActivity(intent);


    }

    ChatContact selectedContact;
    EditText inputEditText;
    Button sendButton;
    Query query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = new AppSharedPref(this);
        setContentView(R.layout.activity_message_list);
        selectedContact = getIntent().getParcelableExtra("selected-contact");
        recipientId = selectedContact.userId;
        ROOM_KEY = getRoomKey(Integer.parseInt(sharedPref.getUserId()), recipientId);

        query = FirebaseFirestore.getInstance()
                .collection(COLLECTION_KEY)
                .document(ROOM_KEY)
                .collection(DOCUMENT_KEY)
                .orderBy("time")
                .limit(50);

        initializeViews();
    }

    MessageListAdapter chatAdapter;
    TextView contactName;
    TextView contactType;

    private void initializeViews() {
        /* ChatService.fetchMessages(10, 0, Integer.parseInt(sharedPref.getUserId()), selectedContact.userId, new NetworkResponseCallback() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray chatsArray = response.optJSONArray("Data");
                if (chatsArray != null) {
                    List<UserMessage> userMessages = buildUserMessages(chatsArray);
                    chatAdapter.setMessageList(userMessages);
                }
            }

            @Override
            public void onFailed(String reason) {

            }

            @Override
            public void onError(String reason) {

            }
        }); */

        query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot snapshot,
                                @Nullable FirebaseFirestoreException exception) {
                if (exception != null) {
                    // Handle error
                    //...
                    return;
                }

                // Convert query snapshot to a list of chats
                List<UserMessage> userMessages = snapshot.toObjects(UserMessage.class);
                chatAdapter.setMessageList(userMessages);
            }
        });

        mToolbar = findViewById(R.id.toolbar);
        contactName = mToolbar.findViewById(R.id.contact_name);
        contactType = mToolbar.findViewById(R.id.contact_type);

        contactName.setText(selectedContact.name);
        contactType.setText(selectedContact.isLawyer ? "Lawyer" : "Student");

        chatList = findViewById(R.id.reyclerview_message_list);
        chatAdapter = new MessageListAdapter(this, null);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        // getSupportActionBar().setHome(true);

        inputEditText = findViewById(R.id.edittext_chatbox);

        sendButton = findViewById(R.id.button_chatbox_send);

        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//        manager.setStackFromEnd(true);
//        manager.setReverseLayout(true);
        manager.setStackFromEnd(true);
        chatList.setLayoutManager(manager);
        chatList.setAdapter(chatAdapter);


        inputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
//
//                       if (typingListener != null) typingListener.userStartedTyping();
//               if (!isTyping) {
////                        isTyping = true;
////                          }
//
//                    removeCallbacks(typingTimerRunnable);
//                    postDelayed(typingTimerRunnable, 1500);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (inputEditText.getText().toString().length() > 0) {
                    UserMessage userMessage = new UserMessage(inputEditText.getText().toString(), Calendar.getInstance().getTimeInMillis(), "", Integer.parseInt(sharedPref.getUserId()), recipientId);

                    ChatService.pushChat(ChatScreenActivity.this, userMessage, selectedContact, sharedPref.getFCMToken());
                    FirebaseFirestore.getInstance()
                            .collection(COLLECTION_KEY)
                            .document(ROOM_KEY)
                            .collection(DOCUMENT_KEY)
                            .add(userMessage);

                    chatAdapter.addMessage(userMessage);
                    scrollView();
                    inputEditText.setText("");

                    //StreetlawMessagingService.sendNotification(sharedPref.getEmail(), userMessage);
                }
            }
        });
    }

    private void scrollView() {
        chatList.post(new Runnable() {
            @Override
            public void run() {
                // Call smooth scroll
                chatList.smoothScrollToPosition(chatAdapter.getItemCount() - 1);
            }
        });
    }

    private List<UserMessage> buildUserMessages(JSONArray chatsArray) {
        List<UserMessage> userMessages = new ArrayList<>();
        for (int i = 0; i < chatsArray.length(); i++) {
            try {
                JSONObject jsonObject = chatsArray.getJSONObject(i);

                String message = jsonObject.optString("Message");
                int senderId = jsonObject.optInt("SenderId");
                int recipientId = jsonObject.optInt("RecipientId");
                String timeSent = jsonObject.optString("TimeSent");

                UserMessage userMessage = new UserMessage(message, TimeUtils.getLongValue(timeSent), "SENT", senderId, recipientId);
                userMessages.add(userMessage);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return userMessages;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

            super.onBackPressed();

    }

    private String getRoomKey(int a, int b) {
        //int key = ((Math.max(a, b) * (Math.max(a, b) + 1)) * 2) + Math.min(a, b);
        return Math.min(a, b) + "_" + Math.max(a, b);
    }
}
