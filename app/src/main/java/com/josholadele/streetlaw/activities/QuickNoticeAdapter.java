package com.josholadele.streetlaw.activities;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.josholadele.streetlaw.R;
import com.josholadele.streetlaw.model.QuickNotice;

import java.util.ArrayList;

public class QuickNoticeAdapter extends RecyclerView.Adapter<QuickNoticeAdapter.QuickNoticeViewHolder>{
    private ArrayList<QuickNotice> quickNoticeArrayList;

    public QuickNoticeAdapter(ArrayList<QuickNotice> quickNoticeArrayList) {
        this.quickNoticeArrayList = quickNoticeArrayList;
    }

    public class QuickNoticeViewHolder extends RecyclerView.ViewHolder{

        TextView quickNoticeTextView;
        public QuickNoticeViewHolder(View itemView) {
            super(itemView);

            quickNoticeTextView = itemView.findViewById(R.id.quick_notice_textView);

        }
    }

    @Override
    public QuickNoticeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View itemView = layoutInflater.inflate(R.layout.quick_notice_view, parent, false);


        QuickNoticeViewHolder quickNoticeViewHolder = new QuickNoticeViewHolder(itemView);


        return quickNoticeViewHolder;
    }

    @Override
    public void onBindViewHolder(QuickNoticeViewHolder holder, int position) {

        QuickNotice quickNotice = quickNoticeArrayList.get(position);

        holder.quickNoticeTextView.setText(quickNotice.getQuickNotice());

    }

    @Override
    public int getItemCount() {
        return quickNoticeArrayList.size();
    }


}
