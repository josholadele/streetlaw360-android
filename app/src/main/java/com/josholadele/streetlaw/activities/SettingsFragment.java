package com.josholadele.streetlaw.activities;

import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;

import com.josholadele.streetlaw.R;

public class SettingsFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

        addPreferencesFromResource(R.xml.preferences);
    }
}
