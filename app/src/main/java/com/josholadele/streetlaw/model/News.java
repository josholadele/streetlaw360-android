package com.josholadele.streetlaw.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by josh on 10/03/2018.
 */
public class News implements Parcelable {

    public int Id;
    public String story;
    public String headline;
    public String pictureUrl;
    public String sourceUrl;


    public News() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public News(int id,
                String story,
                String headline,
                String pictureUrl,
                String sourceUrl) {
        Id = id;
        this.story = story;
        this.headline = headline;
        this.pictureUrl = pictureUrl;
        this.sourceUrl = sourceUrl;
    }

    public News(int id, String story, String headline) {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
        this.Id = id;
        this.story = story;
        this.headline = headline;
    }


    protected News(Parcel in) {
        Id = in.readInt();
        story = in.readString();
        headline = in.readString();
        pictureUrl = in.readString();
        sourceUrl = in.readString();
    }

    public static final Creator<News> CREATOR = new Creator<News>() {
        @Override
        public News createFromParcel(Parcel in) {
            return new News(in);
        }

        @Override
        public News[] newArray(int size) {
            return new News[size];
        }
    };

    @Override
    public String toString() {
//        return "User{" +
//                "displayName='" + displayName + '\'' +
//                ", email='" + email + '\'' +
//                ", phone='" + phone + '\'' +
//                ", profileUri='" + profileUri + '\'' +
//                ", facility='" + facility + '\'' +
//                ", facilityAddress='" + facilityAddress + '\'' +
//                ", facilityLongtitude=" + facilityLongtitude +
//                ", facilityLatitude=" + facilityLatitude +
//                ", facilityCity='" + facilityCity + '\'' +
//                ", facilityState='" + facilityState + '\'' +
//                ", zone='" + zone + '\'' +
//                ", isActivated=" + isActivated +
//                ", speciality='" + speciality + '\'' +
//                ", role='" + role + '\'' +
//                '}';
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(story);
        dest.writeString(headline);
        dest.writeString(pictureUrl);
        dest.writeString(sourceUrl);
    }
}