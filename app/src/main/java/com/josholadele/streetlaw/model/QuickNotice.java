package com.josholadele.streetlaw.model;

public class QuickNotice {
    private String quickNotice;

    public QuickNotice(){

    }

    public QuickNotice(String quickNotice) {
        this.quickNotice = quickNotice;
    }

    public String getQuickNotice() {
        return quickNotice;
    }

    public void setQuickNotice(String quickNotice) {
        this.quickNotice = quickNotice;
    }


}
