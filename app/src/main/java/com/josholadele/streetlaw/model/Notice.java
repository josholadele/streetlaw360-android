package com.josholadele.streetlaw.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.josholadele.streetlaw.enums.CaseStatus;

import java.util.List;

/**
 * Created by josh on 10/03/2018.
 */

public class Notice implements Parcelable {

    public int Id;
    public String Title;
    public String message;
    public String dateSent;
    public List<User> Users;

    public Notice(int id, String title, String message, String dateSent, List<User> users) {
        Id = id;
        Title = title;
        this.message = message;
        this.dateSent = dateSent;
        Users = users;
    }

    public Notice() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Notice(int id, String message, String title) {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
        this.Id = id;
        this.message = message;
        this.Title = title;
    }


    protected Notice(Parcel in) {
//        displayName = in.readString();
//        email = in.readString();
//        phone = in.readString();
//        profileUri = in.readString();
//        facility = in.readString();
//        facilityType = in.readString();
//        facilityAddress = in.readString();
//        facilityLongtitude = in.readDouble();
//        facilityLatitude = in.readDouble();
//        facilityCity = in.readString();
//        facilityState = in.readString();
//        zone = in.readString();
//        isActivated = in.readByte() != 0;
//        speciality = in.readString();
//        role = in.readString();
    }

    public static final Creator<Notice> CREATOR = new Creator<Notice>() {
        @Override
        public Notice createFromParcel(Parcel in) {
            return new Notice(in);
        }

        @Override
        public Notice[] newArray(int size) {
            return new Notice[size];
        }
    };

    @Override
    public String toString() {
//        return "User{" +
//                "displayName='" + displayName + '\'' +
//                ", email='" + email + '\'' +
//                ", phone='" + phone + '\'' +
//                ", profileUri='" + profileUri + '\'' +
//                ", facility='" + facility + '\'' +
//                ", facilityAddress='" + facilityAddress + '\'' +
//                ", facilityLongtitude=" + facilityLongtitude +
//                ", facilityLatitude=" + facilityLatitude +
//                ", facilityCity='" + facilityCity + '\'' +
//                ", facilityState='" + facilityState + '\'' +
//                ", zone='" + zone + '\'' +
//                ", isActivated=" + isActivated +
//                ", speciality='" + speciality + '\'' +
//                ", role='" + role + '\'' +
//                '}';
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(displayName);
//        dest.writeString(email);
//        dest.writeString(phone);
//        dest.writeString(profileUri);
//        dest.writeString(facility);
//        dest.writeString(facilityAddress);
//        dest.writeDouble(facilityLongtitude);
//        dest.writeDouble(facilityLatitude);
//        dest.writeString(facilityCity);
//        dest.writeString(facilityState);
//        dest.writeString(zone);
//        dest.writeByte((byte) (isActivated ? 1 : 0));
//        dest.writeString(speciality);
//        dest.writeString(role);
    }
}