package com.josholadele.streetlaw.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.josholadele.streetlaw.enums.CaseStatus;

import java.util.List;

/**
 * Created by josh on 10/03/2018.
 */

public class Note implements Parcelable {

    /*
    
        public int Id { get; set; }
        public string Topic { get; set; }
        public int UserId { get; set; }
        public string Content { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime LastDateOfUpdate { get; set; }
    * */
    public int Id;
    public int userId;
    public String topic;
    public String content;
    public String dateCreated;
    public String dateLastUpdated;


    public Note() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Note(int id, int userId, String topic, String content, String dateCreated, String dateLastUpdated) {
        Id = id;
        this.userId = userId;
        this.topic = topic;
        this.content = content;
        this.dateCreated = dateCreated;
        this.dateLastUpdated = dateLastUpdated;
    }

    protected Note(Parcel in) {
        Id = in.readInt();
        userId = in.readInt();
        topic = in.readString();
        content = in.readString();
        dateCreated = in.readString();
        dateLastUpdated = in.readString();
//        facility = in.readString();
//        facilityType = in.readString();
//        facilityAddress = in.readString();
//        facilityLongtitude = in.readDouble();
//        facilityLatitude = in.readDouble();
//        facilityCity = in.readString();
//        facilityState = in.readString();
//        zone = in.readString();
//        isActivated = in.readByte() != 0;
//        speciality = in.readString();
//        role = in.readString();
    }

    public static final Creator<Note> CREATOR = new Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    @Override
    public String toString() {
//        return "User{" +
//                "displayName='" + displayName + '\'' +
//                ", email='" + email + '\'' +
//                ", phone='" + phone + '\'' +
//                ", profileUri='" + profileUri + '\'' +
//                ", facility='" + facility + '\'' +
//                ", facilityAddress='" + facilityAddress + '\'' +
//                ", facilityLongtitude=" + facilityLongtitude +
//                ", facilityLatitude=" + facilityLatitude +
//                ", facilityCity='" + facilityCity + '\'' +
//                ", facilityState='" + facilityState + '\'' +
//                ", zone='" + zone + '\'' +
//                ", isActivated=" + isActivated +
//                ", speciality='" + speciality + '\'' +
//                ", role='" + role + '\'' +
//                '}';
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeInt(userId);
        dest.writeString(topic);
        dest.writeString(content);
        dest.writeString(dateCreated);
        dest.writeString(dateLastUpdated);
//        dest.writeString(facility);
//        dest.writeString(facilityAddress);
//        dest.writeDouble(facilityLongtitude);
//        dest.writeDouble(facilityLatitude);
//        dest.writeString(facilityCity);
//        dest.writeString(facilityState);
//        dest.writeString(zone);
//        dest.writeByte((byte) (isActivated ? 1 : 0));
//        dest.writeString(speciality);
//        dest.writeString(role);
    }
}