package com.josholadele.streetlaw.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by josh on 10/03/2018.
 */

public class Lawyer implements Parcelable {

    public int Id;
    public String name;
    public String profilePic;
    public String username;
    public String email;

    public Lawyer(int id, String name, String profilePic, String username, String email) {
        Id = id;
        this.name = name;
        this.profilePic = profilePic;
        this.username = username;
        this.email = email;
    }

    public Lawyer() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }


    protected Lawyer(Parcel in) {
        Id = in.readInt();
        name = in.readString();
        profilePic = in.readString();
        username = in.readString();
        email = in.readString();
    }

    public static final Creator<Lawyer> CREATOR = new Creator<Lawyer>() {
        @Override
        public Lawyer createFromParcel(Parcel in) {
            return new Lawyer(in);
        }

        @Override
        public Lawyer[] newArray(int size) {
            return new Lawyer[size];
        }
    };

    @Override
    public String toString() {
//        return "User{" +
//                "displayName='" + displayName + '\'' +
//                ", email='" + email + '\'' +
//                ", phone='" + phone + '\'' +
//                ", profileUri='" + profileUri + '\'' +
//                ", facility='" + facility + '\'' +
//                ", facilityAddress='" + facilityAddress + '\'' +
//                ", facilityLongtitude=" + facilityLongtitude +
//                ", facilityLatitude=" + facilityLatitude +
//                ", facilityCity='" + facilityCity + '\'' +
//                ", facilityState='" + facilityState + '\'' +
//                ", zone='" + zone + '\'' +
//                ", isActivated=" + isActivated +
//                ", speciality='" + speciality + '\'' +
//                ", role='" + role + '\'' +
//                '}';
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(name);
        dest.writeString(profilePic);
        dest.writeString(username);
        dest.writeString(email);
    }
}