package com.josholadele.streetlaw.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Id;

/**
 * Created by josh on 10/03/2018.
 */
@org.greenrobot.greendao.annotation.Entity(indexes = {
        @Index(value = "time")
})
public class UserMessage implements Parcelable {

    public static final Creator<UserMessage> CREATOR = new Creator<UserMessage>() {
        @Override
        public UserMessage createFromParcel(Parcel in) {
            return new UserMessage(in);
        }

        @Override
        public UserMessage[] newArray(int size) {
            return new UserMessage[size];
        }
    };
    @Id(autoincrement = true)
    private Long id;
    public String text;
    public long time;
    public String status;
    public long userId;
    public long peerId;
    public int type;


    public UserMessage(String text, long time, String status, long userId, long peerId) {
        this.text = text;
        this.time = time;
        this.status = status;
        this.userId = userId;
        this.peerId = peerId;
    }

    protected UserMessage(Parcel in) {
        id = in.readLong();
        text = in.readString();
        time = in.readLong();
        status = in.readString();
        userId = in.readInt();
        peerId = in.readInt();
    }

    public UserMessage() {
    }

    @Generated(hash = 1149666355)
    public UserMessage(Long id, String text, long time, String status, long userId, long peerId,
            int type) {
        this.id = id;
        this.text = text;
        this.time = time;
        this.status = status;
        this.userId = userId;
        this.peerId = peerId;
        this.type = type;
    }

    @Override
    public String toString() {

        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(text);
        dest.writeLong(time);
        dest.writeString(status);
        dest.writeLong(userId);
        dest.writeLong(peerId);
    }

    public Long getId() {
        return this.id;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getUserId() {
        return this.userId;
    }

    public long getPeerId() {
        return this.peerId;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public void setPeerId(long peerId) {
        this.peerId = peerId;
    }
}