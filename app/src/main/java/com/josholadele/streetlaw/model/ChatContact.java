package com.josholadele.streetlaw.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.util.SortedList;

/**
 * Created by josh on 10/03/2018.
 */

public class ChatContact implements Parcelable {

    public static final Creator<ChatContact> CREATOR = new Creator<ChatContact>() {
        @Override
        public ChatContact createFromParcel(Parcel in) {
            return new ChatContact(in);
        }

        @Override
        public ChatContact[] newArray(int size) {
            return new ChatContact[size];
        }
    };
    public int Id;
    public String name;
    public String judicialDivision;
    public String username;
    public int userId;
    public String email;
    public boolean isLawyer;
    public String profileImage;

    public ChatContact(int id, String name, String judicialDivision, int userId, String username, String email, boolean isLawyer) {
        Id = id;
        this.name = name;
        this.judicialDivision = judicialDivision;
        this.userId = userId;
        this.username = username;
        this.email = email;
        this.isLawyer = isLawyer;
    }


    public ChatContact() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    protected ChatContact(Parcel in) {
        Id = in.readInt();
        name = in.readString();
        judicialDivision = in.readString();
        username = in.readString();
        userId = in.readInt();
        email = in.readString();
        isLawyer = in.readInt() == 1;
//        facilityCity = in.readString();
//        facilityState = in.readString();
//        zone = in.readString();
//        isActivated = in.readByte() != 0;
//        speciality = in.readString();
//        role = in.readString();
    }

    public int getId() {
        return Id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "User{" + "displayName='" + name +
                ", email='" + email + '\'' +
                ", phone='" + judicialDivision + '\'' +
                ", profileUri='" + profileImage;

//        return "User{" +
//                "displayName='" + displayName + '\'' +
//                ", email='" + email + '\'' +
//                ", phone='" + phone + '\'' +
//                ", profileUri='" + profileUri + '\'' +
//                ", facility='" + facility + '\'' +
//                ", facilityAddress='" + facilityAddress + '\'' +
//                ", facilityLongtitude=" + facilityLongtitude +
//                ", facilityLatitude=" + facilityLatitude +
//                ", facilityCity='" + facilityCity + '\'' +
//                ", facilityState='" + facilityState + '\'' +
//                ", zone='" + zone + '\'' +
//                ", isActivated=" + isActivated +
//                ", speciality='" + speciality + '\'' +
//                ", role='" + role + '\'' +
//                '}';
        //return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel in, int flags) {

        in.writeInt(Id);
        in.writeString(name);
        in.writeString(judicialDivision);
        in.writeString(username);
        in.writeInt(userId);
        in.writeString(email);
        in.writeInt(isLawyer ? 1 : 0);
//        dest.writeString(displayName);
//        dest.writeString(email);
//        dest.writeString(phone);
//        dest.writeString(profileUri);
//        dest.writeString(facility);
//        dest.writeString(facilityAddress);
//        dest.writeDouble(facilityLongtitude);
//        dest.writeDouble(facilityLatitude);
//        dest.writeString(facilityCity);
//        dest.writeString(facilityState);
//        dest.writeString(zone);
//        dest.writeByte((byte) (isActivated ? 1 : 0));
//        dest.writeString(speciality);
//        dest.writeString(role);
    }

    /*
    @Override
    public <T> boolean isSameModelAs(@NonNull T item) {
        if (item instanceof ChatContact) {
            final ChatContact ChatContact = (ChatContact) item;
            return ChatContact.Id == Id;
        }
        super.isSameModelAs(item);
        return false;
    }

    @Override
    public <T> boolean isContentTheSameAs(@NonNull T item) {
        if (item instanceof ChatContact) {
            final ChatContact other = (ChatContact) item;
            if (email != other.email) {
                return false;
            }
            return email != null ? email.equals(other.email) : other.email == null;
        }
        return false;
    }

    @Override
    public boolean areItemsTheSame(ChatContact item1, ChatContact item2) {
        return item1.getId() == item2.getId();
    } */
}