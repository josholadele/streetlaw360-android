package com.josholadele.streetlaw.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;

/**
 * Created by josh on 10/03/2018.
 */

public class ProfilePictureModel implements Parcelable {

    public static final Creator<ProfilePictureModel> CREATOR = new Creator<ProfilePictureModel>() {
        @Override
        public ProfilePictureModel createFromParcel(Parcel in) {
            return new ProfilePictureModel(in);
        }

        @Override
        public ProfilePictureModel[] newArray(int size) {
            return new ProfilePictureModel[size];
        }
    };
    public int userId;
    public String base64ProfilePicture;
    public String fileName;
    private String _base64;
    private byte[] _byteArray;

    public ProfilePictureModel() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    protected ProfilePictureModel(Parcel in) {
    }

    public byte[] profileImageByteArray() {
        return _byteArray;
    }

    public void profileImageByteArray(byte[] byteArray) {
        _byteArray = byteArray;
        base64ProfilePicture = Base64.encodeToString(_byteArray,
                Base64.NO_WRAP);

    }

    @Override
    public String toString() {
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel in, int flags) {
    }
}