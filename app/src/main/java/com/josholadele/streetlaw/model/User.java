package com.josholadele.streetlaw.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Unique;

/**
 * Created by josh on 10/03/2018.
 */

@org.greenrobot.greendao.annotation.Entity(indexes = {
        @Index(value = "lastConversationTime")
})
public class User implements Parcelable {

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
    public int Id;
    public String displayName;
    public String judicialDivision;
    public String username;
    @Unique
    public int userId;
    public String email;
    public String displayPicture;
    public long lastConversationTime;
    public String lastMessage;
    public boolean isLawyer;

    public User(String displayName, String judicialDivision, String username, int userId, String email, long lastConversationTime, String lastMessage, boolean isLawyer) {
        this.displayName = displayName;
        this.judicialDivision = judicialDivision;
        this.username = username;
        this.userId = userId;
        this.email = email;
        this.lastConversationTime = lastConversationTime;
        this.lastMessage = lastMessage;
        this.isLawyer = isLawyer;
    }

    public User(int id, String displayName, String username, String email, String displayPicture) {
        Id = id;
        this.displayName = displayName;
        this.username = username;
        this.email = email;
        this.displayPicture = displayPicture;
    }

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    protected User(Parcel in) {
        Id = in.readInt();
        displayName = in.readString();
        judicialDivision = in.readString();
        username = in.readString();
        userId = in.readInt();
        email = in.readString();
        displayPicture = in.readString();
        lastConversationTime = in.readLong();
        lastMessage = in.readString();
        isLawyer = in.readInt() == 1;
    }

    @Generated(hash = 268259301)
    public User(int Id,
                String displayName,
                String judicialDivision,
                String username,
                int userId,
                String email,
                String displayPicture,
                long lastConversationTime,
                String lastMessage,
                boolean isLawyer) {
        this.Id = Id;
        this.displayName = displayName;
        this.judicialDivision = judicialDivision;
        this.username = username;
        this.userId = userId;
        this.email = email;
        this.displayPicture = displayPicture;
        this.lastConversationTime = lastConversationTime;
        this.lastMessage = lastMessage;
        this.isLawyer = isLawyer;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "";
    }

    @Override
    public boolean equals(Object obj) {
        User user1 = (User) obj;
        if (userId == user1.userId || email.equals(user1.email) || username.equals(user1.username)) {
            return true;
        }
        return super.equals(obj);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel in, int flags) {

        in.writeInt(Id);
        in.writeString(displayName);
        in.writeString(judicialDivision);
        in.writeString(username);
        in.writeInt(userId);
        in.writeString(email);
        in.writeString(displayPicture);
        in.writeLong(lastConversationTime);
        in.writeString(lastMessage);
        in.writeInt(isLawyer ? 1 : 0);
    }

    public int getId() {
        return this.Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getJudicialDivision() {
        return this.judicialDivision;
    }

    public void setJudicialDivision(String judicialDivision) {
        this.judicialDivision = judicialDivision;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public long getLastConversationTime() {
        return this.lastConversationTime;
    }

    public void setLastConversationTime(long lastConversationTime) {
        this.lastConversationTime = lastConversationTime;
    }

    public String getLastMessage() {
        return this.lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public boolean getIsLawyer() {
        return this.isLawyer;
    }

    public void setIsLawyer(boolean isLawyer) {
        this.isLawyer = isLawyer;
    }

    public String getDisplayPicture() {
        return this.displayPicture;
    }

    public void setDisplayPicture(String displayPicture) {
        this.displayPicture = displayPicture;
    }
}