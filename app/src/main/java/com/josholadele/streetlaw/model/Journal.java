package com.josholadele.streetlaw.model;

import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.Unique;

/**
 * Created by Oladele on 10/26/17.
 */

@org.greenrobot.greendao.annotation.Entity(indexes = {
        @Index(value = "author")
})
public class Journal {

    @Id(autoincrement = true)
    private Long id; //title, mAbstract, mUrl, author
    private String title;
    private String author;
    private String mAbstract;
    private String mUrl;
    @Unique
    private String firebaseKey;
    private boolean isFavorite;

    @Keep
    @Generated(hash = 134102110)
    public Journal(Long id, String title, String author, String mAbstract,
                   String mUrl, String firebaseKey, boolean isFavorite) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.mAbstract = mAbstract;
        this.mUrl = mUrl;
        this.firebaseKey = firebaseKey;
        this.isFavorite = isFavorite;
    }

    @Generated(hash = 1562390721)
    public Journal() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String Title) {
        this.title = Title;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String Author) {
        this.author = Author;
    }

    public String getAbstract() {
        return this.mAbstract;
    }

    public void setAbstract(String Abstract) {
        this.mAbstract = Abstract;
    }

    public String getUrl() {
        return this.mUrl;
    }

    public void setUrl(String Url) {
        this.mUrl = Url;
    }

    public String getMTitle() {
        return this.title;
    }

    public void setMTitle(String mTitle) {
        this.title = mTitle;
    }

    public String getMAuthor() {
        return this.author;
    }

    public void setMAuthor(String mAuthor) {
        this.author = mAuthor;
    }

    public String getMAbstract() {
        return this.mAbstract;
    }

    public void setMAbstract(String mAbstract) {
        this.mAbstract = mAbstract;
    }

    public String getMUrl() {
        return this.mUrl;
    }

    public void setMUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public boolean getIsFavorite() {
        return this.isFavorite;
    }

    public void setIsFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getFirebaseKey() {
        return this.firebaseKey;
    }

    public void setFirebaseKey(String firebaseKey) {
        this.firebaseKey = firebaseKey;
    }


}
