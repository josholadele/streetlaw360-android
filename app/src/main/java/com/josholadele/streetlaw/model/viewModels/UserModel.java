package com.josholadele.streetlaw.model.viewModels;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

/**
 * Created by josh on 10/03/2018.
 */

public class UserModel {

    public static int Id;
    //public Category Category ;
    public static String Category;
    public static String FirstName;
    public static String LastName;
    public static String MiddleName;
    public static String UserName;
    public static String Email;
    public static String ShortBio;
    public static String Password;
    public static String Telephone;
    public static String AddressLine1;
    public static String AddressLine2;
    public static String City;
    public static String State;
    public static String Country;
    public static String ChannelName;
    public static String SupremeCourtEnrollmentNumber;
    public static String Firm;
    public static Boolean Verified;
    public static Date DateRegistered;
    public static Date YearCalled;
    public static Date SubscriptionExpiryTime;
    //public virtual ICollection<CaseViewModel> Cases ;
    public static List<String> Notifications;
    public static String ProfilePicture;

//    public static UserModel GetModel(JSONObject object) {
//        UserModel userModel = new UserModel();
//        for (Field field :
//                UserModel.class.getFields()) {
//            String name = field.getName();
//            try {
//                field.set(userModel.getClass(), object.opt(name));
//            } catch (IllegalAccessException e) {
//                e.printStackTrace();
//            }
//        }
//    }
}
